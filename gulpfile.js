var gulp = require( 'gulp-help' )( require('gulp'), {hideEmpty:true} );

var requireDir = require('require-dir');

// Lazy loader requires coffee.
require('coffee-script/register');
var lazyload = require('gulp-lazyload');

lazyload({
  gutil: 'gulp-util',
  sass: 'gulp-sass',
  childProcess: 'child_process',
  del: 'del',
  sequence: 'run-sequence',
  kss: 'kss',
  fs: 'fs',
  concat: 'gulp-concat',
  insert: 'gulp-insert',
  tap: 'gulp-tap',
  jsonSass: 'json-sass',
  path: 'path',
  debug: 'gulp-debug',
  newer: 'gulp-newer',
  highlight: 'highlight.js',
  ggf: 'gulp-google-fonts',
  webserver: 'gulp-webserver'
});

const yargs = require('yargs')
  .alias('t', 'theme')
  .default('theme', 'pgi')
  .alias('c', 'clean')
  .default('clean', false)
  .alias('v', 'verbose')
  .default('verbose', false)
  .argv;

gutil.log(`Using ${gutil.colors.magenta(yargs.theme)} theme`);

var nodeDir = 'node_modules';
var srcDir = 'src';
var distDir = 'dist';
var componentDir = `${srcDir}/component-lib`;
var styleGuideDir = 'style-guide'

var themeDir = `${componentDir}/themes/${yargs.theme}`;
var fallbackThemeDir = `${componentDir}/themes/pgi`;

var assetsDir = `${themeDir}/assets`;
var fallbackAssetsDir = `${fallbackThemeDir}/assets`;
var appAssetsDir = `${srcDir}/app/assets`;

global.paths = {
  verbose: yargs.verbose,
  hasFiles: function(path){
    if( fs.existsSync(path) ){
      return fs.readdirSync(path).length > 0;
    }
    else {
      return false;
    }
  },
  theme: {
    current: yargs.theme,
    fallback: 'pgi',
    getFile: function(file){
      if( !fs.existsSync(file) ){
        return file.replace(paths.theme.current, paths.theme.fallback);
      }
      else {
        return file;
      }
    }
  },
  // Dev server paths
  server: {
    port: 8090
  },
  // Node module paths
  node: {
    ng: `${nodeDir}/.bin/ng`,
    webpackServer: `${nodeDir}/.bin/webpack-dev-server`
  },
  // Anything that ends up in the dist folder.
  dist: {
    rootDir: `${distDir}`
  },
  // All assets that are dynamically generated.
  build: {
    appAssets: {
      rootDir: appAssetsDir,
      sharedVars: `${appAssetsDir}/shared-variables.json`,
      iconSet: `${appAssetsDir}/iconSet.svg`
    },
    css: {
      sharedVars: `${themeDir}/shared-variables.scss`
    },
    fonts: {
      root: `${assetsDir}/fonts`,
      googleFontsMain: `${assetsDir}/fonts/woff.css`,
      all: `${assetsDir}/fonts/*.css`
    },
    json: {
      sharedVarsDir: `${assetsDir}`,
      sharedVars: `${assetsDir}/shared-variables.json`,
      docs: `${srcDir}/app/routes.json`
    },
    sounds: {
      root: `${assetsDir}/sounds`,
      incoming: `${assetsDir}/sounds/IncomingCallRingtone-Call5.wav`,
      outgoing: `${assetsDir}/sounds/OutgoingCallRingtone_Stock.wav`
    },
    svg: {
      iconDocFilename: 'icon.example.scss',
      iconDoc: `${componentDir}/icons/icon.example.scss`,
      iconDocDir: `${componentDir}/icons`,
      iconSet: `${assetsDir}/iconSet.svg`,
      iconSetFilename: 'iconSet.svg',
      iconSetDir: `${assetsDir}`
    }
  },
  // All assets that live in (are checked into) the src folder.
  src: {
    rootDir: srcDir,
    packageJSON: 'package.json',
    assets: {
      all: `${assetsDir}/**/*`
    },
    app: {
      rootDir: `${srcDir}/app`
    },
    componentLib: {
      rootDir: `${componentDir}`,
      all: `${componentDir}/**/*`
    },
    theme: {
      root: themeDir,
      css: `${themeDir}/**/*.scss`,
      fontsConfig: `${assetsDir}/fonts/google-fonts.neon`
    },
    css: {
      all: `${srcDir}/**/*.scss`,
      components: `${componentDir}/**/*.scss`
    },
    html: {
      components: `${componentDir}/**/*.html`
    },
    svg: {
      iconsDir: `${assetsDir}/icon-src`,
      icons: `${assetsDir}/icon-src/*.svg`
    },
    json: {
      hierarchy: `${srcDir}/app/page-hierarchy.json`,
      sharedVars: `${themeDir}/shared-variables.json`
    }
  },
  // All assets and bundles that are in the Style Guide that is hosted in S3.
  styleGuide: {
    assets: {
      rootDir: `${styleGuideDir}/assets`
    }
  }
};

// UTILITIES

// Execute a command using the shell option.
global.utils = {
  shell: function(command, cb){
    gutil.log( gutil.colors.blue(command) );
    var child = childProcess.spawn(command, {shell:true});
    child.stdout.pipe(process.stdout);
    child.stderr.pipe(process.stderr);
    child.on('close', cb);
    return child;
  }
}


// TASKS

// Require all of the other gulp tasks.
requireDir( './gulp', {recurse:false} );

gulp.task('logo', function(done){
  utils.shell(`cat .logo`, done);
});

gulp.task( 'generated-files', function(done){
  sequence(
    ['icons', 'fonts'],
    ['shared-variables', 'kss'],
    'themed-app-assets',
    done
  );
});

var startupTasks = function(){
  var startup = ['logo', 'theme-exists'];

  if( yargs.clean ){
    startup.push('clean');
  }

  return startup;
}

gulp.task( 'theme-exists', function(){
  if( !paths.hasFiles( themeDir ) ){
    gutil.log( gutil.colors.magenta(`Cannot find the theme directory for ${gutil.colors.yellow(yargs.theme)}.`))
    gutil.log( themeDir );
    process.exit(0);
  }
});

gulp.task( 'sg',
  'Build and serve the styleguide website. This will also rebuild on changes.',
  function(done){
    sequence(
      startupTasks(),
      'generated-files',
      ['sg-serve', 'docs-watch', 'icons-watch'],
      done
    );
  }
);

gulp.task( 'sg-linked',
  'Package the styleguide for use with other projects using `npm link`. This will repackage the library as you make changes.',
  function(done){
    sequence(
      startupTasks(),
      'generated-files',
      'package',
      ['sg-serve', 'docs-watch', 'icons-watch', 'package-watch'],
      done
    );
  }
);

gulp.task( 'build',
  'Build a fresh version of the package (no watch tasks).',
  function(done){
    sequence(
      startupTasks(),
      'generated-files',
      done
    );
  }
);

gulp.task( 'build-hosted-sg',
  'Build the Style Guide bundle that will be shipped to S3.',
  function(done){
    sequence(
      startupTasks(),
      'generated-files',
      'package-hosted-sg',
      done
    );
  }
);

gulp.task( 'default', ['sg'] )

