import { PgiNgStyleGuidePage } from './app.po';

describe('pgi-ng-style-guide App', function() {
  let page: PgiNgStyleGuidePage;

  beforeEach(() => {
    page = new PgiNgStyleGuidePage();
  });

  it('should display the page header', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('PGi Style Guide and Component Library');
  });
});
