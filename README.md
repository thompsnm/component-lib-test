# pgi-ng-component-lib

A PGi component library for Angular 2 projects with SASS.

This project is both a component library that can be imported into other projects and a website
that documents how to use the components. For information on using the component library in other
projects, serve the website by following the directions below.

## Development

### Setup

1. `npm install`

### Serving the style guide

The following insructions describe how to serve the style guide from the src directory. This is useful during development.

1. `npm start` - This will generate and serve the styleguide website with further usage documentation.
1. Visit the URL printed to the console.

### Serving the style guide bundle that will be shipped to S3

The following insructions describe how to serve the style guide from the bundled style-guide directory. This is useful to verify that the style guide bundle will work on S3.

1. `npm start:hosted-sg` - This will generate and serve the styleguide bundle that is hosted on S3.
1. Visit the URL printed to the console.

### Linking this library for local development

During development, you can use (npm link)[https://docs.npmjs.com/cli/link] to link your locally checked
out component library to any projects in which you are using it. This will allow you to make changes
to the component library and see them immediately in your project. You will need to specifically link
the dist folder as follows:

1. `cd pgi-ng-component-lib`
1. `gulp build` to ensure the package is ready to link (i.e. dist/ folder exists)
1. `cd dist` You need to run link inside the dist folder since this is what usually gets published.
1. `npm link`
1. `cd ../my-other-project` move to the project where you are using the component library.
1. `npm link @pgi/ng-component-lib`

After linking, you can run the following to automatically repackage the component library as you make changes:

`npm run start:linked` from inside the component library.

Now making changes in the component library should refresh your project's website (provided your project watches
node_modules for changes).

### Code scaffolding

Run `ng generate component component-name` to generate a new component.
You can also use `ng generate directive/pipe/service/class/module`.

### Running unit tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `npm start`.

## Publishing the dist folder

The dist folder is published to the PGi private npm repo and contains the component library itself.

### Publishing via Jenkins

Jenkins will automatically identify when the component lib version has been incremented on the master branch and publish a new full version. Simply follow these steps to trigger Jenkins:

1. Run `git checkout master` to ensure you are on the master branch
1. Run `npm version [major | minor | patch]` to correctly increment the version. [Example](https://coderwall.com/p/9bx-iq/npm-to-bump-module-version)
1. Run `git push --follow-tags` to commit the updated package.json file and new tag.
1. Wait for Jenkins to automatically build the master branch pipeline, or go to the [Jenkins job](https://jenkins.centraldesktop-dev.com/view/Nokia/job/PGi%20Ng%20Component%20Lib%20Build/) and click "Scan Multibranch Pipeline Now".

### Publishing manually

Jenkins should handle all publishing for you (see above). However, if for some reason you need to publish a new version of the component lib by hand, you can do so by following these instructions:

1. Run `git checkout master` to ensure you are on the master branch
1. Run `./initialize-npm.sh` to set up your Artifactory credentials.
    - **NOTE:** `./initialize-npm.sh` only needs to be run once per machine.
1. Run `npm run validate` to test and package your code.
1. Run `npm version [major | minor | patch]` to correctly increment the version. [Example](https://coderwall.com/p/9bx-iq/npm-to-bump-module-version)
1. Run `git push --follow-tags` to commit the updated package.json file and new tag.
1. Run `npm run package:config` to repackage with the updated version number.
1. Run `npm publish dist` to publish the module to the PGI Artifactory git repo.

### Snapshots

Jenkins will automatically generate a snapshot version with every merge to the master branch. These snapshots are versioned `x.x.x-snapshot.BUILD_NUMBER`. The most recent snapshot is tagged in the npm repo, so you can install it by simply running:

```sh
npm install @pgi/ng-component-lib@snapshot
```

## Deploying the style guide

The style guide is hosted in S3 and available to view at [http://nokia-component-library.s3-website.us-east-2.amazonaws.com/index.html](http://nokia-component-library.s3-website.us-east-2.amazonaws.com/index.html).

### Deploying via Jenkins

Just as with the dist folder, Jenkins will automatically identify when the component lib version has been incremented on the master branch and publish an updated style guide to S3. Simply follow these steps to trigger Jenkins: 

1. Run `git checkout master` to ensure you are on the master branch
1. Run `npm version [major | minor | patch]` to correctly increment the version. [Example](https://coderwall.com/p/9bx-iq/npm-to-bump-module-version)
1. Run `git push --follow-tags` to commit the updated package.json file and new tag.
1. Wait for Jenkins to automatically build the master branch pipeline, or go to the [Jenkins job](https://jenkins.centraldesktop-dev.com/view/Nokia/job/PGi%20Ng%20Component%20Lib%20Build/) and click "Scan Multibranch Pipeline Now".

### Deploying manually

If you need to deploy to S3 manually, first [install the aws cli](http://docs.aws.amazon.com/cli/latest/userguide/installing.html). Next, set your local AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY:

```sh
export AWS_ACCESS_KEY_ID=YOUR_ACCESS_KEY
export AWS_SECRET_ACCESS_KEY=YOUR_SECRET_KEY
```

Then build the style guide by running:

```sh
npm run build:hosted-sg
```

Finally, run the following command from the root of this project:

```sh
aws s3 cp style-guide/ s3://nokia-component-library/ --region us-east-2 --acl public-read --recursive
```

## More Help
More information on developing components for the component library can be found by serving the library.

