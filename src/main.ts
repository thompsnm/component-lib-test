import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { environment } from './environments/environment';
// In order to dynamically compile components from the SASS documentation,
// we need to configure the app with the JIT compilers.
import { COMPILER_PROVIDERS } from '@angular/compiler';
import { AppModule } from './app/app.module';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic([COMPILER_PROVIDERS]).bootstrapModule(AppModule);
