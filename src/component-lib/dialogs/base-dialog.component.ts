import { Component } from '@angular/core';
import { PGiDialog } from './dialog.service';

@Component({
  selector: 'pgi-base-dialog',
  // TODO: Replace button with <pgi-navigation-button icon="pgi:close"></pgi-navigation-button>
  template: `
    <button class="pgi-close" (click)="closeDialog()">x</button>
    <ng-content></ng-content>`
})
export class PGiBaseDialog {

  constructor(private dialog: PGiDialog) { }

  closeDialog() {
    this.dialog.closeAll();
  }
}
