import { Component, Input } from '@angular/core';
import { PGiDialog } from './dialog.service';

@Component({
  selector: 'pgi-large-panel',
  template: `
    <pgi-base-dialog>
      <header>{{ title }}</header>
      <div class="body">
        <ng-content></ng-content>
      </div>
      <footer>
        <ng-content select="[footer]"></ng-content>
      </footer>
    </pgi-base-dialog>
    `
})
export class PGiLargePanel {
  private _title = '';

  constructor(private dialog: PGiDialog) { }

  @Input()
  set title(title: string) {
    this._title = title;
  }

  get title(): string {
    return this._title;
  }

  closeDialog() {
    this.dialog.closeAll();
  }

}
