import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PGiDialog } from './dialog.service';
import { PGiLargePanel } from './large-panel.component';
import { PGiBaseDialog } from './base-dialog.component';
import { MaterialModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    NoopAnimationsModule
  ],
  declarations: [
    PGiLargePanel,
    PGiBaseDialog
  ],
  providers: [
    PGiDialog,
  ],
  entryComponents: [
    PGiLargePanel,
    PGiBaseDialog
  ],
  exports: [
    PGiLargePanel,
    PGiBaseDialog,
  ]
})
export class PGiDialogsModule { }
