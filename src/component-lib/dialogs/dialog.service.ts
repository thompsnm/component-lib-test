import { Injectable, TemplateRef } from '@angular/core';
import { ComponentType, MdDialogConfig, MdDialog, MdDialogRef } from '@angular/material';

@Injectable()
export class PGiDialog extends MdDialog {

  // TODO: Refactor calls to PGiDialog open to pass in MdDialog config with
  // a specified panelClass attribute. Included as part of material beta-7
  open<T>(componentOrTemplateRef: ComponentType<T> | TemplateRef<T>, large: Boolean = false,
          config?: MdDialogConfig): MdDialogRef<T> {
    let dialogRef = super.open(componentOrTemplateRef, config);

    if ( large ) {
      let fancyStuff = dialogRef as any;
      let overlayElement = fancyStuff._overlayRef.overlayElement;
      overlayElement.classList.add('pgi-large-panel-overlay');
    }

    return dialogRef;
  }
}
