import { async, ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { PGiDialog } from './dialog.service';
import { PGiDialogsModule } from './dialogs.module';
import { MaterialModule } from '@angular/material';
import { Component, NgModule } from '@angular/core';
import { PGiDialogPage } from './dialog.service.page';
import { By } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

var modalHeader = '<header>Test title</header>';
var modalHTML = '<h1 body="">Test content</h1>';
var modalDOMElements = '<button footer="">Press me</button>';

@Component({
  template: `
    <pgi-large-panel [title]="title">
      <h1 body>Test content</h1>
      <button footer>Press me</button>
    </pgi-large-panel>
  `
})
class ModalContent {
  title = "Test title";
}

@Component({
  template: ``
})
class ModalStub {

}

// Create a local module so we can compile entryComponents
// (components which are not referenced directly in a template).
// @see https://github.com/angular/angular/issues/10760
@NgModule({
  declarations: [ ModalContent, ModalStub ],
  imports: [ PGiDialogsModule ],
  entryComponents: [ ModalContent, ModalStub ]
})
class TestModule {}

describe('PGiDialog', () => {
  let fixture;
  let dialog, service;

  beforeEach( async( () => {
    PGiDialogPage.makeTestBed(TestModule)
      .compileComponents();

    // Manually create a component to call detectChanges() on.
    fixture = TestBed.createComponent(ModalStub);
  }));

  beforeEach(inject([PGiDialog], (dialogService: PGiDialog) => {
    service = dialogService;
    dialog = new PGiDialogPage(document.body);
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('After the dialog is opened', () => {
    beforeEach( async ( () => {
      service.open(ModalContent);
      fixture.detectChanges();
    }));

    it('should be visible.', () => {
      expect( dialog.isVisible() ).toBe( true );
    });

    it('should contain expected content inside the dialog.', () => {
      expect( dialog.contentContains( modalHTML ) ).toBe(true);
    });

    it('should contain the expected nested DOM elements.', () => {
      expect( dialog.contentContains( modalDOMElements ) ).toBe(true);
    });

    it('should contain the expected header in the dialog.', () => {
      expect( dialog.contentContains( modalHeader ) ).toBe(true);
    });
  });

  // TODO Figure out why the MdModal afterClose event isn't triggering
  // which prevents the following specs from being written.
  xit('should close the dialog by clicking out of the frame', () => {});
  xit('should programatically close the dialog', () => {});
});
