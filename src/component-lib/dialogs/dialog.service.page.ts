import { TestBed, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { PGiDialogsModule } from './dialogs.module';
import { MaterialModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

export class PGiDialogPage {
  static makeTestBed(testModule) {
    return TestBed.configureTestingModule({
      imports: [
        PGiDialogsModule,
        testModule,
        NoopAnimationsModule,
      ]
    });
  }

  constructor(private root) { }

  isVisible() {
    let dialogContainer = this._getDialogContainer();
    if (dialogContainer) {
      return true;
    }
    else {
      return false;
    }
  }

  _getDialogContainer() {
    return this.root.querySelector('md-dialog-container');
  }

  contentContains(modalContent) {
    return this._getDialogContainer().innerHTML.indexOf(modalContent) >= 0;
  }

}
