import { Component, Input, ViewEncapsulation, HostBinding } from '@angular/core';
import { MdIcon } from '@angular/material';

@Component({
  selector: 'pgi-icon',
  template: `<md-icon color="{{color}}"
  [svgIcon]="icon"
  [attr.ariaLabel]="ariaLabel"><ng-content></ng-content></md-icon>`,
  encapsulation: ViewEncapsulation.None
})
export class PGiIcon {
  @HostBinding('class.pgi-icon') pgiIconClass = true;
  @Input() color;
  @Input() icon;
  @Input('aria-label') ariaLabel;
}
