import { NgModule, Inject } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MaterialModule, MdIconRegistry } from '@angular/material';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PGI_ICON_SET_URL } from './icon-registry.service';
import { PGiIcon } from './icon.component';

@NgModule({
  declarations: [
    PGiIcon
  ],
  imports: [
    MaterialModule,
    CommonModule,
    FormsModule
  ],
  providers: [
    MdIconRegistry
  ],
  exports: [
    PGiIcon
  ]
})
export class PGiIconsModule {
  constructor (@Inject(PGI_ICON_SET_URL) url: string, mdIconRegistry: MdIconRegistry,
    sanitizer: DomSanitizer ) {
    if ( url && url !== '' ) {
      mdIconRegistry.addSvgIconSetInNamespace(
        'pgi',
        sanitizer.bypassSecurityTrustResourceUrl( url )
      );
    } else {
      console.error(`You must configure the location of the PGi Icon Set
        before using the PGiIcon component. See component library documentation for more info.`);
    }
  }
};

