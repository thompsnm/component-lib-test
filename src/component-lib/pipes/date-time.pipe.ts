/*
 * (C) Copyright Premiere Global Services 2017 - All Rights Reserved
 *
 * This source code and documentation is copyrighted and owned by Premiere
 * Global Services.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for ANY purposes commercial or non-commercial must be obtained
 * via licensing arrangements with Premiere Global Services.
 */

import { ChangeDetectorRef, OnDestroy, Pipe, PipeTransform, Type } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { EndOfDayService } from './end-of-day.service';
import * as moment from 'moment';

/**
 * Custom pipe to format date and time.
 * This pipe updates the text automatically only at the end of the day.
 * Below is the format in which the formatting is done
 * If the date is:
 *       - Today: Displays time in 12Hrs format. Ex: 05:10 PM
 *       - Yesterday: Displays the value as 'Yesterday'
 *       - With in the last week: Displays the weeks day. Ex. Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
 *       - Greater than a week: Displays value in format dd/mm/yyyy.
 * If no date format is specified the default format used is 'hh:mm A D/MM/YYYY';
 * Ref to https://momentjs.com/docs/#/displaying/format/ for more info on formatting options.
 * Sample formats used here:
 *   Default  -  h:mm A D/MM/YYYY
 *   Greater Than a Week - D/MM/YYYY
 *   With in last Week - dddd
 *   Today - LT
 * This function as a generic date formatter by default
 * To enable Pgi call customization set enableCallFormat to true;
 * ```
 *     {{ dateObj | pgi-date:true }
 * ```
 * This pipe does support Internationalization but the custom values need to be passed as on an object to the pipe.
 * For more info on how to pass cutom value refer to https://momentjs.com/docs/#/customization/
 */
@Pipe({
  name: 'pgidate',
  pure: false
})
export class PGiDatePipe implements PipeTransform, OnDestroy {

  private dateFormat: string;
  private enableCallFormat: boolean;
  private currentTimer: number;
  private formatDate: string;
  private locMoment: moment.Moment;
  private prevValue: any;
  private locale: string;
  private locObject: any;
  private currDate: any;
  private subscription: Subscription = null;
  /**
   * This is here for consistency puprpose only as defined in angular DatePipe
   */
  PGiDatePipe_ALIASES = {
    medium: <any>'lll',
    short: <any>'llll',
    fullDate: <any>'LLLL',
    longDate: <any>'LL',
    mediumDate: <any>'ll',
    shortDate: <any>'L',
    mediumTime: <any>'LTS',
    shortTime: <any>'LT',
    default: <any>'hh:mm A D/MM/YYYY'
  };

  /**
   * Constructor.
   * @param  {ChangeDetectorRef} privateref
   * @param  {NgZone} privatengZone
   */
  constructor(private ref: ChangeDetectorRef, private endOfDayService: EndOfDayService) {
    this.locObject = {
      calendar: {
        sameDay: 'h:mm A',
        nextDay: '[Tomorrow]',
        nextWeek: 'dddd',
        lastDay: '[Yesterday]',
        lastWeek: 'dddd',
        sameElse: 'L'
      }
    };
    /**
     * @param  {any} value
     * @param  {boolean} enableCallFormat?
     * @param  {string} format?
     * @param  {string} locale?
     * @param  {any} locObject?
     * @returns string
     */
  }
  transform(value: any, enableCallFormat?: boolean, format?: string, locale?: string, locObject?: any): string {
    if (!(moment(value).isValid())) {
      throw new Error(`InvalidPipeArgument to PGiDatePipe: ${value}`);
    }
    if (!(moment(value).isSame(this.prevValue))) {
      this.prevValue = moment(value);
      this.dateFormat = (format != null || format !== undefined) ? format : <string><any>this.PGiDatePipe_ALIASES['default'];
      this.locale = (locale != null || locale !== undefined) ? locale : 'en';
      moment.locale(this.locale);
      this.currDate = value;
      this.enableCallFormat = (enableCallFormat != null || enableCallFormat !== undefined) ? enableCallFormat : false;
      if (this.enableCallFormat) {
        this.customizeMomentLocale(locObject);
      }
      this.createSubscription();
      this.formatDate = this.formattedDate();
    }
    return this.formatDate;
  }
  private createSubscription() {
    this.removeSubscription();
    this.subscription = this.endOfDayService.isEndofDay().subscribe(endOfDay => {
      if (endOfDay) {
        this.formatDate = this.formattedDate();
        this.ref.markForCheck();
      }
    });
  }

  private removeSubscription() {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

  ngOnDestroy(): void {
    this.removeSubscription();
  }
  private customizeMomentLocale(obj: any) {
    if (obj != null || obj !== undefined) {
      moment.updateLocale(this.locale, obj);
    } else {
      moment.updateLocale(this.locale, this.locObject);
    }
  }
  private formattedDate(): string {
    if (!this.enableCallFormat) {
      return moment(this.currDate).format(this.dateFormat);
    } else {
      return moment(this.currDate).calendar(null);
    }
  }

  getTimeToEndOfDay(): number {
    let diff: number = moment().endOf('day').diff(moment(), 'seconds');
    return diff * 1000;
  }

}



