import { ChangeDetectorRef, Type } from '@angular/core';
import { PGiDatePipe } from './date-time.pipe';
import { EndOfDayService } from './end-of-day.service';
import * as moment from 'moment';

describe('PGiDatePipeTest', () => {
  let mockDate: Date;
  let endofDayService: EndOfDayService;
  beforeEach(() => {
    jasmine.clock().install();
    mockDate = new Date();
    jasmine.clock().mockDate(mockDate);
    jasmine.clock().tick(1);
    endofDayService = new EndOfDayService();
  });
  afterEach(() => {
    jasmine.clock().uninstall();
    endofDayService = null;
  });

  it('should throw error when passed invalid value"', () => {
    let pipe = new PGiDatePipe(null, endofDayService);
    expect(() => { pipe.transform('Hi There Sudheer'); }).toThrow(new Error(`InvalidPipeArgument to PGiDatePipe: Hi There Sudheer`));
    expect(() => { pipe.transform(null); }).toThrow(new Error(`InvalidPipeArgument to PGiDatePipe: null`));
    expect(() => { pipe.transform('I am a pipe'); }).toThrow(new Error(`InvalidPipeArgument to PGiDatePipe: I am a pipe`));
  });

  it('should transform the date in format h:mm A D/MM/YYYY', () => {
    let pipe = new PGiDatePipe(null, endofDayService);
    expect(pipe.transform(mockDate)).toBe(moment().format('hh:mm A D/MM/YYYY'));
  });

  it('should transform todays date with PGi Call Customization in format hh:mm A', () => {
    let pipe = new PGiDatePipe(null, endofDayService);
    expect(pipe.transform(mockDate, true)).toBe(moment().format('h:mm A'));
  });

  it('should automatically trigger change after a day passes', () => {
    let changeDetectorMock = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);
    let pipe = new PGiDatePipe(changeDetectorMock, endofDayService);
    expect(pipe.transform(mockDate)).toBe(moment().format('hh:mm A D/MM/YYYY'));
    expect(changeDetectorMock.markForCheck).not.toHaveBeenCalled();
    jasmine.clock().tick(pipe.getTimeToEndOfDay());
    expect(changeDetectorMock.markForCheck).toHaveBeenCalled();
  });

  it('should not trigger change before a day passes', () => {
    let changeDetectorMock = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);
    let pipe = new PGiDatePipe(changeDetectorMock, endofDayService);
    expect(pipe.transform(mockDate)).toBe(moment().format('hh:mm A D/MM/YYYY'));
    jasmine.clock().tick(1000);
    expect(changeDetectorMock.markForCheck).not.toHaveBeenCalled();
  });

  it('should automatically update the text as time passes according to PGi Call Customization', () => {
    let changeDetectorMock = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);
    let pipe = new PGiDatePipe(changeDetectorMock, endofDayService);
    expect(pipe.transform(mockDate, true)).toBe(moment().format('h:mm A'));
    let date = mockDate.setDate(mockDate.getDate() - 1);
    expect(pipe.transform(date, true)).toBe('Yesterday');
    mockDate.setFullYear(2016);
    expect(pipe.transform(mockDate, true)).toMatch('(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/((19|20)\\d\\d)');
  });

  it('should remove all timer when destroyed', () => {
    let changeDetectorMock = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);
    let pipe = new PGiDatePipe(changeDetectorMock, endofDayService);
    expect(pipe.transform(mockDate, true)).toBe(moment().format('h:mm A'));
    pipe.ngOnDestroy();
    jasmine.clock().tick(60000);
    expect(changeDetectorMock.markForCheck).not.toHaveBeenCalled();
  });
});
