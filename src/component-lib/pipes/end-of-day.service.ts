import { Injectable, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as moment from 'moment';

@Injectable()
export class EndOfDayService implements OnDestroy {
  startAndEndDaySubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  subscription: Subscription;
  constructor() {
    let timer: Observable<number> = Observable.interval(1000);
    this.subscription = timer.subscribe(() => {
      this.checkForEndofDay();
    });
  }
  checkForEndofDay() {
    let diff: number = moment().endOf('day').diff(moment(), 'seconds');
    if (diff === 0) {
      this.endOfDay();
    }
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  startOfDay() {
    this.startAndEndDaySubject.next(false);
  }
  endOfDay() {
    this.startAndEndDaySubject.next(true);
  }
  isEndofDay(): Observable<boolean> {
    return this.startAndEndDaySubject.asObservable();
  }
}
