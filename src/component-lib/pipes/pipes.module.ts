import { NgModule } from '@angular/core';
import { PGiDatePipe } from './date-time.pipe';
import { EndOfDayService } from './end-of-day.service';

const PGI_PIPES = [
  PGiDatePipe
];

@NgModule({
  declarations: [
    PGI_PIPES
  ],
  exports: [
    PGI_PIPES
  ],
  providers: [
    EndOfDayService
  ]
})
export class PGiPipesModule { }
