import { Component, ElementRef, Input, Output, EventEmitter, Renderer2, ViewEncapsulation } from '@angular/core';

type ButtonType = 'light' | 'dark';
type IconType = 'pgi:back' | 'pgi:forward' | 'pgi:close';

@Component({
  selector: 'pgi-navigation-button',
  templateUrl: './navigation-button.component.html',
  styleUrls: ['./navigation-button.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class PGiNavigationButtonComponent {
  @Input() icon: IconType = 'pgi:back';
  @Input() set type(val: ButtonType) {
    this._type = val;
    this.setType(val);
  }
  @Input() disabled = false;
  @Input('aria-label') ariaLabel: String;
  /* Emits button click event */
  @Output() buttonClickEvent = new EventEmitter<any>();


  private _type: ButtonType = 'light';

  constructor(public elementRef: ElementRef, private _renderer: Renderer2) { }

  ngOnInit() {
    if (this.icon === 'pgi:close') {
      this._renderer.addClass(this.elementRef.nativeElement, 'close');
    } else if (this.icon === 'pgi:back' || this.icon === 'pgi:forward') {
      this._renderer.addClass(this.elementRef.nativeElement, 'back-forward');
    }
  }

  ngAfterViewInit() {
    this.setType(this._type);
  }

  /* Returns fab button element */
  private getFabButton() {
    return this.elementRef.nativeElement.querySelector('pgi-fab-button');
  }

  /* Adding class on button an basis of type and removing other classes */
  private setType(val: ButtonType) {
    if (val === 'dark') {
      this._renderer.removeClass(this.getFabButton(), 'light');
    } else {
      this._renderer.removeClass(this.getFabButton(), 'dark');
    }
    this._renderer.addClass(this.getFabButton(), val);
  }

  /* Event emitted when button is clicked */
  private onButtonClick(event: any) {
    let isDisabled;
    if (event && event.currentTarget) {
      isDisabled = event.currentTarget.getElementsByTagName('button')[0].getAttribute('disabled');
    }
    if (isDisabled === null) {
      this.buttonClickEvent.emit(event.currentTarget.parentElement);
    }
  }
}
