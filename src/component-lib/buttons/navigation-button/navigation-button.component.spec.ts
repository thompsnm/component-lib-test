import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Component } from '@angular/core';
import { HttpModule } from '@angular/http';

import { PGiNavigationButtonComponent } from './navigation-button.component';
import { PGiNavigationButtonPage } from './navigation-button.component.page';

@Component({
  template: `<pgi-navigation-button 
    [disabled]="isDisabled" 
    [type]="type" 
    (click)="increment($event)">
  </pgi-navigation-button>
  `
})

class TestWrapper {
  clickCount = 0;
  isDisabled = false;
  type = "light";

  increment(event: any) {
    this.clickCount++;
  }
}

describe('PGiNavigationButton', () => {
  let fixture: ComponentFixture<TestWrapper>;
  let navigationButton: PGiNavigationButtonPage;
  let component: PGiNavigationButtonComponent;

  beforeEach(async(() => {
    PGiNavigationButtonPage.makeTestBed(TestWrapper)
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(TestWrapper);
        fixture.autoDetectChanges();

        let el = fixture.debugElement.query(By.css('pgi-navigation-button'));
        navigationButton = new PGiNavigationButtonPage(el);
        component = el.componentInstance;
      });
  }));

  describe('type properties', () => {
    it('should apply class light by default', () => {
      expect(navigationButton.navigationButtonElement().classList).toContain('light');
      expect(navigationButton.navigationButtonElement().classList).not.toContain('dark');
    });

    it('should apply class light for type light', () => {
      component.type = 'light';
      fixture.detectChanges();

      expect(navigationButton.navigationButtonElement().classList).toContain('light');
      expect(navigationButton.navigationButtonElement().classList).not.toContain('dark');
    });

    it('should apply class dark for type dark', () => {
      component.type = 'dark';
      fixture.detectChanges();

      expect(navigationButton.navigationButtonElement().classList).toContain('dark');
      expect(navigationButton.navigationButtonElement().classList).not.toContain('light');
    });

    it('should not clear previous defined classes', () => {
      navigationButton.navigationButtonElement().classList.add('custom-class');
      fixture.detectChanges();

      expect(navigationButton.navigationButtonElement().classList).toContain('custom-class');

      component.type = 'light';
      fixture.detectChanges();

      expect(navigationButton.navigationButtonElement().classList).toContain('light');
      expect(navigationButton.navigationButtonElement().classList).not.toContain('dark');
      expect(navigationButton.navigationButtonElement().classList).toContain('custom-class');

      component.type = 'dark';
      fixture.detectChanges();

      expect(navigationButton.navigationButtonElement().classList).not.toContain('light');
      expect(navigationButton.navigationButtonElement().classList).toContain('dark');
      expect(navigationButton.navigationButtonElement().classList).toContain('custom-class');
    });
  });

  // Regular button tests
  describe('pgi-navigation-button', () => {
    it('should handle a click on the button', () => {
      navigationButton.buttonNativeElement().click();
      expect(fixture.componentInstance.clickCount).toBe(1);
    });

    it('should not increment if disabled', () => {
      fixture.componentInstance.isDisabled = true;
      fixture.detectChanges();

      navigationButton.buttonNativeElement().click();
      expect(fixture.componentInstance.clickCount).toBe(0);
    });

    it('should disable the native button element', () => {
      expect(navigationButton.buttonNativeElement().disabled)
        .toBeFalsy('Expected button not to be disabled');

      fixture.componentInstance.isDisabled = true;
      fixture.detectChanges();

      expect(navigationButton.buttonNativeElement().disabled)
        .toBeTruthy('Expected button to be disabled');
    });
  });

  describe('icon properties', () => {
    it('should add icon on the basis of icon property', () => {
      expect(navigationButton.svgElementId()).toContain('back');

      component.icon = 'pgi:forward';
      fixture.detectChanges();
      expect(navigationButton.svgElementId()).toContain('forward');

      component.icon = 'pgi:back';
      fixture.detectChanges();
      expect(navigationButton.svgElementId()).toContain('back');

      component.icon = 'pgi:close';
      fixture.detectChanges();
      expect(navigationButton.svgElementId()).toContain('close');
    });
  });
});
