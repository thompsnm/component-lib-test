@import 'theme';

/*
Navigation Button

Wraps the PGi design fab button.

### Attribute Values

- **disabled** - Disable the button.
- **type** - Set the type of this button. Type values are light or dark. Default is light.
- **icon** - One of the icons; pgi:back, pgi:forward or pgi:close. Default is pgi:back.
- **aria-label** - You should always specify an arial-label with icon buttons for screen readers.

### Events

- **click** will emit an event of type `buttonClickEvent`


Markup:
<pgi-navigation-button icon="pgi:back" aria-label="back nav"></pgi-navigation-button>
<pgi-navigation-button icon="pgi:forward" type="light"></pgi-navigation-button>
<pgi-navigation-button icon="pgi:back" type="light"></pgi-navigation-button>
<pgi-navigation-button icon="pgi:forward" type="light" disabled></pgi-navigation-button>
<pgi-navigation-button icon="pgi:back" type="light" disabled></pgi-navigation-button>
<br/>
<br/>
<pgi-navigation-button icon="pgi:close" aria-label="close btn"></pgi-navigation-button>
<pgi-navigation-button icon="pgi:close" type="light"></pgi-navigation-button>
<pgi-navigation-button icon="pgi:close" type="light" disabled></pgi-navigation-button>
<br/>
<br/>
<div class="pgi-dark-theme" style="padding: 5px;">
<pgi-navigation-button icon="pgi:back" aria-label="back nav"></pgi-navigation-button>
<pgi-navigation-button icon="pgi:forward" type="dark"></pgi-navigation-button>
<pgi-navigation-button icon="pgi:back" type="dark"></pgi-navigation-button>
<pgi-navigation-button icon="pgi:forward" type="dark" disabled></pgi-navigation-button>
<pgi-navigation-button icon="pgi:back" type="dark" disabled></pgi-navigation-button>
<br/>
<br/>
<pgi-navigation-button icon="pgi:close" aria-label="close btn"></pgi-navigation-button>
<pgi-navigation-button icon="pgi:close" type="dark"></pgi-navigation-button>
<pgi-navigation-button icon="pgi:close" type="dark" disabled></pgi-navigation-button>
</div>

Styleguide buttons.navigation-button
*/

// Color map dark type of navigation buttons (back & forward)
$pgi-dialog-navigation-dark-theme: (
  normal: rgba($black, 0.4),
  hover-stroke: rgba($white, 0.2),
  pressed: rgba($black, 0.4),
  pressed-stroke: rgba($white, 0.5),
  text: #f8fafb,
  pressed-text: $white,
  disabled-text: rgba(147, 171, 189, 0.25)
);

// Color map light type of navigation buttons (back & forward)
$pgi-dialog-navigation-light-theme: (
  normal: rgba(206, 212, 226, 0.15),
  hover-stroke: rgba(206, 212, 226, 0.3),
  pressed: rgba(206, 212, 226, 0.3),
  pressed-stroke: rgba(206, 212, 226, 0.3),
  text: #00aeef,
  pressed-text: #00aeef,
  disabled-text: rgba(147, 171, 189, 0.25)
);

// Color map dark type of navigation button (close)
$pgi-dialog-close-dark-theme: (
  text: #ced4e2,
  hover-text: $white,
  pressed-text: $white,
  disabled-text: rgba($white, 0.4)
);

// Color map light type of navigation button (close)
$pgi-dialog-close-light-theme: (
  text: #ced4e2,
  hover-text: #303031,
  pressed-text: #303031,
  disabled-text: rgba(147, 171, 189, 0.25)
);

// Size of navigation button container
$pgi-dialog-button-container-size: pgi-rem(56);

// Size of navigation buttons (back & forward)
$pgi-dialog-navigation-icon-size: pgi-rem(50);
$pgi-dialog-navigation-arrow-scale: 1.12;

// Size of navigation button (close)
$pgi-dialog-button-close-size: pgi-rem(24);
$pgi-dialog-navigation-close-icon-size: pgi-rem(16);

/// mixin which sets size, background, border shadow and border for navigation button
///   @param size
///   @param border-size
///   @param border-color
///   @param background
///   @acess public
@mixin pgi-dialog-navigation-button($size, $border-size, $border-color, $background) {
  height: $size - $border-size * 2;
  width: $size - $border-size * 2;
  border: solid $border-size $border-color;
  background: $background;
  box-shadow: none;
}

/// mixin which sets dialog button layout
///   @param size
///   @acess public
@mixin pgi-dialog-button-layout($size) {
  width: $size;
  height: $size;
}

/// mixin which sets close dialog button style
///   @param color
///   @acess public
@mixin pgi-dialog-close-style($color) {
  background-color: transparent;
  box-shadow: none;
  border: 0;
  color: $color;
}

%pgi-button-focus-overlay {
  .mat-button-focus-overlay {
    opacity: 0;
  }
}

/// mixin which holds dialog navigation button style based on theme provided
///   @param $palette
///   @access public
@mixin pgi-dialog-navigation-theme($palette) {
  &.pgi-inverse {
    @include pgi-dialog-button-layout($pgi-dialog-button-container-size);

    //defines navigation dialog button style for its
    //focus, hover, active, disbled states
    button {
      &.mat-fab {
        @extend %pgi-button-focus-overlay;

        &:not([disabled]) {
          @include pgi-dialog-navigation-button($pgi-dialog-button-container-size, pgi-border(s), map-get($palette, hover-stroke), map-get($palette, normal));
          color: map-get($palette, text);
          box-sizing: content-box;
          padding: 0;

          &:hover {
            @include pgi-dialog-navigation-button($pgi-dialog-button-container-size, pgi-rem(3), map-get($palette, hover-stroke), map-get($palette, normal));
          }

          &:active {
            @include pgi-dialog-navigation-button($pgi-dialog-button-container-size, pgi-rem(3), map-get($palette, pressed-stroke), map-get($palette, pressed));
            background-clip: padding-box;

            .mat-icon {
              fill: map-get($palette, pressed-text);
            }
          }
        }

        &[disabled] {
          background-color: map-get($palette, normal);
          color: map-get($palette, disabled-text);
          padding: 0;
        }

        //defines dialog button wrapper style
        .mat-button-wrapper {
          padding: 0;
        }

        //defines style for mat-icon and svg element
        .mat-icon {
          @include pgi-dialog-button-layout($pgi-dialog-navigation-icon-size);

          svg {
            transform: scale($pgi-dialog-navigation-arrow-scale);
          }
        }
      }
    }
  }
}

/// mixin which holds dialog close button style based on theme provided
///   @param $palette
///   @access public
@mixin pgi-dialog-close-theme($theme) {
  &.pgi-inverse {
    @include pgi-dialog-button-layout($pgi-dialog-button-close-size);

    //defines dialog close button style for its
    //focus, hover, active, disbled states
    button {
      &.mat-fab {
        @extend %pgi-button-focus-overlay;
        @include pgi-dialog-button-layout($pgi-dialog-button-close-size);
        padding: 0;

        .mat-button-wrapper {
          @include pgi-dialog-button-layout($pgi-dialog-navigation-close-icon-size);
          padding: 0;
        }

        .mat-icon {
          @include pgi-dialog-button-layout($pgi-dialog-navigation-close-icon-size);
        }

        &:not([disabled]) {
          @include pgi-dialog-close-style(map-get($theme, text));

          &:hover,
          &:active {
            @include pgi-dialog-button-layout($pgi-dialog-button-close-size);
            @include pgi-dialog-close-style(map-get($theme, hover-text));
            cursor: pointer;
          }
        }

        &[disabled] {
          @include pgi-dialog-button-layout($pgi-dialog-button-close-size);
          @include pgi-dialog-close-style(map-get($theme, disabled-text));
        }
      }
    }
  }
}

/// mixin which holds style for close, forward and back dialog buttons 
/// based on light and dark theme
///   @access public
@mixin pgi-navigation-button-component-theme {
  pgi-navigation-button {
    display: inline-flex;

    &.close {
      @include pgi-dialog-button-layout($pgi-dialog-button-close-size);

      .light {
        @include pgi-dialog-close-theme($pgi-dialog-close-light-theme);
      }

      .dark {
        @include pgi-dialog-close-theme($pgi-dialog-close-dark-theme);
      }
    }

    &.back-forward {
      @include pgi-dialog-button-layout($pgi-dialog-button-container-size);

      .light {
        @include pgi-dialog-navigation-theme($pgi-dialog-navigation-light-theme);
      }

      .dark {
        @include pgi-dialog-navigation-theme($pgi-dialog-navigation-dark-theme);
      }
    }
  }
}
