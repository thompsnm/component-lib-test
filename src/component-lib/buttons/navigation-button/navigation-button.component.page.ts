import { By } from '@angular/platform-browser';
import { TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PGiButtonsModule } from '../buttons.module';
import { PGI_ICON_SET_URL } from '../../icons/icon-registry.service';

export class PGiNavigationButtonPage {
  root = null;

  selectors = {
    navigationButtonElement: 'pgi-fab-button',
    navigationButtonNativeElement: 'button',
    svgElement: 'svg'
  };

  static makeTestBed(container) {
    return TestBed
      .configureTestingModule({
        imports: [PGiButtonsModule, HttpModule],
        declarations: [container],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [
          { provide: PGI_ICON_SET_URL, useValue: '/base/src/app/assets/iconSet.svg' }
        ],
      });
  }

  constructor(debugElement) {
    this.root = debugElement;
  }

  navigationButtonElement() {
    let navigationButton = this.root.query(By.css(this.selectors.navigationButtonElement))
      .nativeElement;

    return navigationButton;
  }

  buttonNativeElement() {
    let button = this.root.query(By.css(this.selectors.navigationButtonNativeElement))
      .nativeElement;

    return button;
  }

  svgElementId() {
    let svg = document.querySelector(this.selectors.svgElement);

    return svg.getAttribute('id');
  }
}
