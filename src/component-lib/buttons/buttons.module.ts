import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';
import { MdButton } from '@angular/material';
import { PGiIconsModule } from '../icons';

import { PGiTextButton } from './text-button.component';
import { PGiIconButton } from './icon-button.component';
import { PGiFabButton } from './fab-button.component';
import { PGiToggleButton } from './toggle-button.component';
import { PGiMenuButtonComponent } from './menu-button/menu-button.component';
import { PGiNavigationButtonComponent } from './navigation-button/navigation-button.component';

@NgModule({
  declarations: [
    PGiTextButton,
    PGiIconButton,
    PGiFabButton,
    PGiToggleButton,
    PGiMenuButtonComponent,
    PGiNavigationButtonComponent
  ],
  imports: [
    MaterialModule,
    PGiIconsModule
  ],
  exports: [
    PGiTextButton,
    PGiIconButton,
    PGiFabButton,
    PGiToggleButton,
    PGiMenuButtonComponent,
    PGiNavigationButtonComponent
  ]
})
export class PGiButtonsModule { }
