import { Component, Input, Output, ViewEncapsulation, EventEmitter } from '@angular/core';
import { PGiFabButton } from './fab-button.component';

/* tslint:disable:no-access-missing-member */
@Component({
  selector: 'pgi-toggle-button',
  template: `<pgi-fab-button
    (click)="toggle()"
    aria-label="{{ariaLabel}}"
    color="{{color}}"
    icon="{{icon}}" 
    [disabled]="disabled"
    class="{{class}} {{checkedClass}}"></pgi-fab-button>`,
  encapsulation: ViewEncapsulation.None
})
export class PGiToggleButton extends PGiFabButton {
  @Input() class: String;
  @Output() toggledChanged = new EventEmitter<Boolean>();

  toggled: Boolean = false;
  checkedClass: String = 'pgi-inverse';

  toggle() {
    if (this.disabled === false) {
      this.toggled = this.toggled ? false : true;
      this.checkedClass = this.toggled ? '' : 'pgi-inverse';

      this.toggledChanged.emit(this.toggled);
    }
  }
}
