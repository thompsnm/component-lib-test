import { Component, Input, ViewEncapsulation } from '@angular/core';
import { MdButton } from '@angular/material';

@Component({
  selector: 'pgi-icon-button',
  template: `<button md-button disableRipple
    color="{{color}}"
    [disabled]="disabled">
    <pgi-icon icon="{{icon}}"
    [attr.size]="size"
    class="{{iconClasses}}"
    aria-label="{{ariaLabel}}"></pgi-icon>
  </button>`,
  encapsulation: ViewEncapsulation.None
})
export class PGiIconButton {
  @Input() color: String;
  @Input() size: String;
  @Input() disabled: Boolean = false;
  @Input() icon: String;
  @Input() iconClasses: String;
  @Input('aria-label') ariaLabel: String;
}
