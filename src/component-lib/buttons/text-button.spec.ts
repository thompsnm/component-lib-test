// These test cases are the replica of the button test cases in Angular Material lib.
// we have copied them here to validate basic button behaviour with our changes included
// as there are no change in behaviour of buttons so these test cases are sufficient.

import { PGI_ICON_SET_URL } from './../icons/icon-registry.service';
import { PGiButtonsModule } from './buttons.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('pgi-text-button', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PGiButtonsModule],
      declarations: [TestApp],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: PGI_ICON_SET_URL, useValue: 'src/component-lib/assets/iconSet.svg' }
      ]
    });

    TestBed.compileComponents();
  }));

  // General button tests
  it('should apply class based on color attribute', () => {
    let fixture = TestBed.createComponent(TestApp);

    let testComponent = fixture.debugElement.componentInstance;
    let buttonDebugElement = fixture.debugElement.query(By.css('button'));

    testComponent.buttonType = 'primary';
    fixture.detectChanges();
    expect(buttonDebugElement.nativeElement.classList.contains('pgi-btn-primary')).toBe(true);

    testComponent.buttonType = 'secondary';
    fixture.detectChanges();
    expect(buttonDebugElement.nativeElement.classList.contains('pgi-btn-secondary')).toBe(true);

    testComponent.buttonType = 'destructive';
    fixture.detectChanges();
    expect(buttonDebugElement.nativeElement.classList.contains('pgi-btn-destructive')).toBe(true);
  });

  it('should should not clear previous defined classes', () => {
    let fixture = TestBed.createComponent(TestApp);
    let testComponent = fixture.debugElement.componentInstance;
    let buttonDebugElement = fixture.debugElement.query(By.css('button'));

    buttonDebugElement.nativeElement.classList.add('custom-class');

    testComponent.buttonType = 'primary';
    fixture.detectChanges();

    expect(buttonDebugElement.nativeElement.classList.contains('pgi-btn-primary')).toBe(true);
    expect(buttonDebugElement.nativeElement.classList.contains('custom-class')).toBe(true);

    testComponent.buttonType = 'secondary';
    fixture.detectChanges();

    expect(buttonDebugElement.nativeElement.classList.contains('pgi-btn-primary')).toBe(false);
    expect(buttonDebugElement.nativeElement.classList.contains('pgi-btn-secondary')).toBe(true);
    expect(buttonDebugElement.nativeElement.classList.contains('custom-class')).toBe(true);

  });

  // Regular button tests
  describe('pgi-text-button', () => {
    it('should handle a click on the button', () => {
      let fixture = TestBed.createComponent(TestApp);
      let testComponent = fixture.debugElement.componentInstance;
      let buttonDebugElement = fixture.debugElement.query(By.css('button'));

      buttonDebugElement.nativeElement.click();
      expect(testComponent.clickCount).toBe(1);
    });

    it('should not increment if disabled', () => {
      let fixture = TestBed.createComponent(TestApp);
      let testComponent = fixture.debugElement.componentInstance;
      let buttonDebugElement = fixture.debugElement.query(By.css('button'));

      testComponent.isDisabled = true;
      fixture.detectChanges();

      buttonDebugElement.nativeElement.click();

      expect(testComponent.clickCount).toBe(0);
    });

    it('should disable the native button element', () => {
      let fixture = TestBed.createComponent(TestApp);
      let buttonNativeElement = fixture.nativeElement.querySelector('button');
      expect(buttonNativeElement.disabled).toBeFalsy('Expected button not to be disabled');

      fixture.componentInstance.isDisabled = true;
      fixture.detectChanges();
      expect(buttonNativeElement.disabled).toBeTruthy('Expected button to be disabled');
    });
  });
});

/** Test component that contains an MdButton. */
@Component({
  selector: 'test-app',
  template: `
    <pgi-text-button (click)="increment()"
      [disabled]="isDisabled" [type]="buttonType" [disableRipple]="rippleDisabled">
      Go
    </pgi-text-button>
  `
})
class TestApp {
  clickCount = 0;
  isDisabled = false;
  rippleDisabled = false;

  increment() {
    this.clickCount++;
  }
};
