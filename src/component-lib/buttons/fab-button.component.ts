import { Component, Input, ViewEncapsulation } from '@angular/core';
import { PGiIconButton } from './icon-button.component';

/* tslint:disable:no-access-missing-member */
@Component({
  selector: 'pgi-fab-button',
  template: `<button md-fab disableRipple
    color="{{color}}"
    [disabled]="disabled">
    <pgi-icon icon="{{icon}}"
    [attr.size]="size"
    class="{{iconClasses}}"
    aria-label="{{ariaLabel}}"></pgi-icon>
  </button>`,
  encapsulation: ViewEncapsulation.None
})
export class PGiFabButton extends PGiIconButton { }
