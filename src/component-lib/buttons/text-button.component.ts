import {
  Component,
  ElementRef,
  Input,
  OnInit,
  Renderer2,
  ViewEncapsulation
} from '@angular/core';

// declare possible types of button.
type BtnType = 'primary' | 'secondary' | 'destructive';

@Component({
  selector: 'pgi-text-button',
  template:
  `<button class='pgi-btn-layout' 
    [attr.disabled]="disabled"
    [style.width]="buttonWidth" 
    [style.height]="buttonHeight">
      <span class='pgi-button-wrapper'>
        <ng-content></ng-content>
      </span>
   </button>`,
  styleUrls: ['./text-button.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class PGiTextButton implements OnInit {
  /** property for setting button disability */
  @Input() disabled: Boolean;
    /** Property for setting button Width */
  @Input() buttonWidth;
  /** Property for setting button Height */
  @Input() buttonHeight;


  /** property for setting button type {BtnType} */
  @Input()
  get type(): BtnType { return this._type; }
  set type(value: BtnType) { this._updateType(value); }
  private _type: BtnType = 'primary';

  constructor(private _renderer: Renderer2, private _elementRef: ElementRef) { }

  /**
   * This method is called whenever input bound property type changes..
   * @param { BtnType } newType The type of pgi-text-button
   * return { undefined }
   */
  _updateType(newType: BtnType) {
    this._setElementType(this._type, false);
    this._setElementType(newType, true);
    this._type = newType;
  }

  /**
   * This method returns the button element inside our custom component.
   *
   * return { Object } The button object.
   */
  _getBtnElement() {
    return this._elementRef.nativeElement.querySelector('.pgi-btn-layout');
  }

  /**
   * This method is called to set the class to button element based on button type.
   * @param { BtnType } type The type of pgi-text-button.
   * @param { boolean } isAdd This boolean value specifies whether we add class or remove it.
   * return { undefined }
   */
  _setElementType(type: BtnType, isAdd: boolean) {
    if (isAdd) {
      this._renderer.addClass(this._getBtnElement(), `pgi-btn-${type}`);
    } else {
      this._renderer.removeClass(this._getBtnElement(), `pgi-btn-${type}`);
    }
  }

  /**
   * This method is called once in Buttons life cycle and it initialises the component.
   */
  ngOnInit() {
    this._updateType(this._type);
  }
}
