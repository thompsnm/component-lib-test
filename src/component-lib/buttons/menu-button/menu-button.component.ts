import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'pgi-menu-button',
  templateUrl: './menu-button.component.html',
  styleUrls: ['./menu-button.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PGiMenuButtonComponent {
  @Input() disabled: Boolean = false;
  @Input('aria-label') ariaLabel: String;
}
