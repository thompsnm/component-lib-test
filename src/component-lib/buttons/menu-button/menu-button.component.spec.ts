import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

import { PGiMenuButtonComponent } from './menu-button.component';

describe('PGiMenuButtonComponent', () => {
  let component: PGiMenuButtonComponent;
  let fixture: ComponentFixture<PGiMenuButtonComponent>;
  let menuButton: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PGiMenuButtonComponent ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PGiMenuButtonComponent);
    component = fixture.componentInstance;
    menuButton = fixture.debugElement.query(By.css('pgi-icon-button'));
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should have disabled attr defaulted to false', () => {
    expect(component.disabled).toBe(false);
    expect(menuButton.properties['disabled']).toBe(false)
  });

  it('should allow disabled attr setting', () => {
    component.disabled = true;
    fixture.detectChanges();
    expect(menuButton.properties['disabled']).toBe(true);
  });

  it('should allow setting of aria-label', () => {
    var label = 'helpful';
    component.ariaLabel = label;
    fixture.detectChanges();
    expect(menuButton.properties['aria-label']).toBe('helpful')
  });
});