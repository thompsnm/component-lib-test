import { NgModule } from '@angular/core';
import { PGiDebugGridModule } from './debug-grid';
import { PGiButtonsModule } from './buttons';
import { PGiIconsModule } from './icons';
import { PGiSideMenuModule } from './side-menu';
import { PGiContactBubbleModule } from './contact-bubble';
import { PGiCollapseModule } from './collapse';
import { PGiToastModule } from './toast';
import { PGiToolBarModule } from './tool-bar';
import { PGiTooltipModule } from './tooltip';
import { PGiInputsModule } from './inputs';
import { PGiSlideToggleModule } from './slide-toggle';
import { PGiCheckBoxModule } from './check-box';
import { PGiPipesModule } from './pipes';
import { PGiListItemModule } from './list-item';
import { PGiDialogsModule } from './dialogs';
import { PGiMenuModule } from './menu';
import { PGiSpinnerModule } from './spinner/spinner.module';
import { PGiPanelModule } from './panel/panel.module';
import { PGiDropdownModule } from './drop-down/drop-down.module';

// Use this module to load the entire component library.
// It is generally recommended that you only import component
// modules individually but this aggregate module is also
// provided for your convenience.
@NgModule({
  imports: [
    PGiDebugGridModule,
    PGiButtonsModule,
    PGiIconsModule,
    PGiSideMenuModule,
    PGiContactBubbleModule,
    PGiCollapseModule,
    PGiToastModule,
    PGiToolBarModule,
    PGiPipesModule,
    PGiInputsModule,
    PGiListItemModule,
    PGiSlideToggleModule,
    PGiSpinnerModule,
    PGiTooltipModule,
    PGiCheckBoxModule,
    PGiMenuModule,
    PGiDialogsModule,
    PGiPanelModule,
    PGiDropdownModule
  ],
  exports: [
    PGiDebugGridModule,
    PGiButtonsModule,
    PGiIconsModule,
    PGiSideMenuModule,
    PGiContactBubbleModule,
    PGiCollapseModule,
    PGiToastModule,
    PGiToolBarModule,
    PGiInputsModule,
    PGiPipesModule,
    PGiListItemModule,
    PGiSlideToggleModule,
    PGiSpinnerModule,
    PGiTooltipModule,
    PGiCheckBoxModule,
    PGiDialogsModule,
    PGiMenuModule,
    PGiPanelModule,
    PGiDropdownModule
  ]
})
export class PGiComponentsModule { }
