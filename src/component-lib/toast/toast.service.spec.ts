import { NgModule, Component, Inject } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { PGiToastService, PGiToastDuration } from './index';
import { PGiToastPage } from './toast.service.page';

@Component({
  selector: 'simple-container',
  template: `<div></div>`,
  providers: [PGiToastService]
})
class SimpleContainer {
  constructor(
    @Inject(PGiToastService) public toastService: PGiToastService,
  ) { }
}

@NgModule({
  declarations: [
    SimpleContainer
  ],
  exports: [
    SimpleContainer
  ]
})
class TestModule { }

describe('The toast service', () => {
  let fixture: ComponentFixture<SimpleContainer>;
  let toastService: PGiToastService;
  let toastPage: PGiToastPage;

  beforeEach(async(() => {
    PGiToastPage.makeTestBed(TestModule)
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(SimpleContainer);
        toastService = fixture.componentInstance.toastService;
        toastPage = new PGiToastPage(fixture.debugElement.query(By.css('div')));
      });
  }));

  beforeEach(() => {
    jasmine.clock().install();
  });

  afterEach(() => {
    jasmine.clock().uninstall();
  });

  afterEach(() => {
    // Make sure to get rid of any lingering toast
    if (toastService) {
      toastService.dismiss();
      fixture.detectChanges();
    }
  });

  it('should initially not show a toast', () => {
    expect(toastPage.isToastShown).toBe(false);
  });

  describe('when a toast has been opened', () => {
    beforeEach((done) => {
      toastService.openToast('Slice 1');
      fixture.detectChanges();
      fixture.whenStable().then(() => done());
    });

    it('should show a toast', () => {
      expect(toastPage.isToastShown).toBe(true);
    });

    it('should show the supplied text', () => {
      expect(toastPage.toastContent).toContain('Slice 1');
    });

    describe('and another toast is opened', () => {
      beforeEach((done) => {
        toastService.openToast('Slice 2');
        fixture.detectChanges();
        fixture.whenStable().then(() => done());
      });

      it('should not show the old toast', () => {
        expect(toastPage.toastContent).not.toContain('Slice 1');
      });

      it('should show the new toast', () => {
        expect(toastPage.toastContent).toContain('Slice 2');
      });
    });
  });

  describe('when a toast is opened with a duration', () => {
    beforeEach((done) => {
      toastService.openToast('Slice 1', PGiToastDuration.MEDIUM);
      fixture.detectChanges();
      fixture.whenStable().then(() => done());
    });

    describe('and a time less than the duration passes', () => {
      beforeEach((done) => {
        jasmine.clock().tick(PGiToastDuration.MEDIUM - 1);
        fixture.detectChanges();
        fixture.whenStable().then(() => done());
      });

      it('should still show the toast', () => {
        expect(toastPage.isToastShown).toBe(true);
      });
    });

    describe('and a time greater than the duration passes', () => {
      beforeEach((done) => {
        jasmine.clock().tick(PGiToastDuration.MEDIUM + 1);
        fixture.detectChanges();
        fixture.whenStable().then(() => done());
      });

      it('should hide the toast', () => {
        expect(toastPage.isToastShown).toBe(false);
      });
    });
  });
});
