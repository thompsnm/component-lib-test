import { DebugElement } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MdSnackBarModule } from '@angular/material';

export class PGiToastPage {
  static overlayContainerElement: HTMLElement;

  static makeTestBed(testModule) {
    return TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        MdSnackBarModule,
        testModule
      ]
    });
  }

  constructor(private root: DebugElement) { }

  get toastContainer(): Element {
    return document.querySelector('snack-bar-container');
  }

  get isToastShown(): Boolean {
    return !!this.toastContainer && this.toastContainer.childNodes.length > 0;
  }

  get toastContent(): String {
    return this.toastContainer.textContent;
  }
}
