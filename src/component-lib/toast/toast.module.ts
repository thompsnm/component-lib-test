import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PGiToastService } from './toast.service';
import { MdSnackBarModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    MdSnackBarModule,
    NoopAnimationsModule
  ],
  providers: [
    PGiToastService
  ]
})
export class PGiToastModule { }
