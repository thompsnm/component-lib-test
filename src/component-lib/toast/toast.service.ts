import { Injectable } from '@angular/core';
import { MdSnackBar, MdSnackBarConfig, MdSnackBarRef } from '@angular/material';
import { PGiToastDuration } from './toast.duration';

@Injectable()
export class PGiToastService extends MdSnackBar {
  toastRef: MdSnackBarRef<any>;

  openToast(text: string, duration = PGiToastDuration.MEDIUM) {
    let config = new MdSnackBarConfig();
    config.extraClasses = ['pgi-toast'];
    config.duration = duration;

    this.toastRef = super.open(text, null, config);
  }
}
