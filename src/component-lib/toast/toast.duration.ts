export const enum PGiToastDuration {
  SHORT = 1500,
  MEDIUM = 3000,
  LONG = 5000
};
