import { PGiCircularSpinner } from './circular-spinner.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PGiIconsModule } from '../icons/icons.module';


@NgModule({
  declarations: [PGiCircularSpinner],
  imports: [BrowserModule, PGiIconsModule],
  exports: [PGiCircularSpinner],
})
export class PGiSpinnerModule { }
