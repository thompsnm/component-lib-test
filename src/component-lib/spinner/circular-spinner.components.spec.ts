import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, Component, Input, NO_ERRORS_SCHEMA } from '@angular/core';
import { PGiCircularSpinner } from './circular-spinner.component';

describe('PGiCircularSpinner', () => {
    let fixture: ComponentFixture<PGiCircularSpinner>;
    let comp: PGiCircularSpinner;
    let de, el, styles;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PGiCircularSpinner],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(PGiCircularSpinner);
            fixture.detectChanges();
            comp = fixture.componentInstance;
            de = fixture.debugElement.query(By.css('pgi-icon'));
            el = de.nativeElement;
        });
    }));

    it('should have a defined component', () => {
        expect(comp).toBeDefined();
    });

    it('should apply color of primary if no color is specified', () => {
        expect(comp.color).toBe('primary');
    });

    it('should have default class set to spinner', () => {
        expect(el.classList).toContain('spinner');
    });

    it('should not apply animations to the svg', () => {
        comp.skipAnim = true;
        fixture.detectChanges();
        expect(el.classList).not.toContain('spinner');
    });
});
