import { Component, ElementRef, Input, ViewEncapsulation, Renderer, OnInit } from '@angular/core';

@Component({
  selector: 'pgi-circular-spinner',
  templateUrl: './circular-spinner.component.html',
  styleUrls: ['./circular-spinner.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PGiCircularSpinner implements OnInit {
  @Input() color;
  @Input() icon;
  @Input() skipAnim = false;

  constructor(private renderer: Renderer, private elementRef: ElementRef) {
    // This is used by default from the iconSet.
    // You can use any icon from iconSet instead of this.
    this.icon = 'pgi:circular-spinner';
  }

  ngOnInit() {
    if (!this.color) {
      this.color = 'primary';
      this.renderer.setElementStyle(this.elementRef.nativeElement, 'color', this.color);
    }
  }
}
