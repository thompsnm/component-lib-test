import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { DropdownOptions } from './drop-down-options';

export class PGiDropdownComponentPage {

  selectors = {
    selectedDiv: '.selected-div',
    pgiDropDown: 'pgi-drop-down',
    overlayContainer: 'overlay-container',
    titleLabel: '.title-label'
  };

  //  private document: DebugElement : D

  constructor(private root: DebugElement) {

  }

  public getDropDownListTextField() {
    return this.root.query(By.css(this.selectors.selectedDiv)).nativeElement;
  }

  public getDropDownListContainer() {
    return this.root.query(By.css(this.selectors.pgiDropDown)).nativeElement;
  }

  public getOverLay() {
    return <any>document.getElementsByClassName(this.selectors.overlayContainer).item(0) as HTMLElement;
  }
  public getDropDownOnOverLay() {
    return this.getOverLay().getElementsByClassName(this.selectors.pgiDropDown).item(0);
  }

  public getTitleLabel() {
    return this.root.query(By.css(this.selectors.titleLabel)).nativeElement;
  }

  public getOptionsArray() {
    return [
      new DropdownOptions('Item1', 'desc1'),
      new DropdownOptions('Item2', 'desc2'),
      new DropdownOptions('Item3', 'desc3', true),
      new DropdownOptions('Item4', 'desc4'),
      new DropdownOptions('Item5', 'desc5'),
      new DropdownOptions('Item5', 'desc6')
    ];
  }
}
