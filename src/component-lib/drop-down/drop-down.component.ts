import { Component, Input, ElementRef, Renderer, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { DropdownOptions } from './drop-down-options';
import { WindowRef } from './window.service';

type Options = Array<DropdownOptions>;
type DropdownType = 'singleLine' | 'doubleLine';
const OVERLAY_CONTAINER = 'overlay-container';
const TYPE_DOUBLE_LINE = 'doubleLine';
const TYPE_SINGLE_LINE = 'singleLine';
const ITEM_COUNT = 3;

@Component({
  selector: 'pgi-drop-down',
  templateUrl: './drop-down.component.html',
  styleUrls: ['./drop-down.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PGiDropdownComponent {

  @Output() clickItemEvent = new EventEmitter<any>();
  @Input() disabled: String;
  @Input() id: String;
  @Input() titleText: String;
  @Input() type: DropdownType = TYPE_SINGLE_LINE;
  @Input() itemsCount = ITEM_COUNT;
  @Input() selectedIndex: number;
  @Input() errorMessage: String;

  @Input() set options(val: Options) {
    this._options = val;
    this.setSelectedElement();
  }

  get options(): Options {
    return this._options;
  }

  public isListOpen = false;
  public mainTxt: String;
  public secTxt: String;
  public dropDownHeight: number;

  private selected: number;
  private _options: Options = [];
  private listCount = 0;
  private selectedDiv: HTMLElement;

  /**
   * Creates an instance of PGiDropdownComponent.
   * @param {ElementRef} elementRef instance
   * @param {Renderer} renderer instance
   * @param {WindowRef} window instance
   * @memberof PGiDropdownComponent
   * Create instance of drop down menu component.
   */
  constructor(private elementRef: ElementRef, private renderer: Renderer, private window: WindowRef) { }

  /**
   * @private
   * @memberof PGiDropdownComponent
   * Sets the first item of the array as selected item on assigning options array
   */
  private setSelectedElement() {
    if (this.options.length > 0) {
      this.selectedDiv = this.elementRef.nativeElement.querySelector('.selected-div');
      this.listCount = this.getListElementsSize();
      this.heightOfDropDown();
      if (this.selectedIndex && this.selectedIndex < this.listCount) {
        this.mainTxt = this.options[this.selectedIndex].value;
        this.secTxt = this.options[this.selectedIndex].description;
        if (this.options[this.selectedIndex].disabled === false) {
          this.selected = this.selectedIndex;
        }
      } else {
        this.mainTxt = "";
        this.secTxt = "";
      }
    }
  }

  /**
   * @private
   * @param {MouseEvent} event intance of event object.
   * @memberof PGiDropdownComponent
   * This method is called to remove the overlay and drop-down menu
   * when user clicks on overlay.
   */
  private hideDropdownMenu(event: MouseEvent) {
    this.isListOpen = false;
    let select: HTMLElement;
    let target: HTMLElement = <HTMLElement>event.target;
    let selectedItemObj = {};
    if (target.id !== OVERLAY_CONTAINER) {
      select = this.findAncestor(target, 'list-elements');
      if (select) {
        this.dropDownSelected(parseInt(select.dataset.index, 10));
        selectedItemObj = {
          selectedIndex: select.dataset.index,
          selectedItem: this.options[select.dataset.index]
        };
        this.clickItemEvent.emit(selectedItemObj);
      }
    }
    if (select || target.id === OVERLAY_CONTAINER) {
      this.renderer.setElementClass(this.elementRef.nativeElement, 'absolute', false);
      let overlay = document.getElementById(OVERLAY_CONTAINER);
      overlay.outerHTML = '';
    }
  }

  /**
   * @private
   * @param {HTMLElement} el reference
   * @param {string} cls parent class
   * @returns {HTMLElement} reference of parent element
   * @memberof PGiDropdownComponent
   */
  private findAncestor(el: HTMLElement, cls: string): HTMLElement {
    while ((el = el.parentElement) && !el.classList.contains(cls)) { };
    return el;
  }

  /**
   * @private
   * @memberof PGiDropdownComponent
   * This method opens the dropdown list on an overlay.
   */
  private openList() {
    if (this.options.length > 0) {
      let overlay = document.createElement('div');
      overlay.id = OVERLAY_CONTAINER;
      this.renderer.setElementClass(overlay, OVERLAY_CONTAINER, true);
      overlay.addEventListener('click', (event) => { this.hideDropdownMenu(<MouseEvent>event); }, false);

      if (document.body.firstChild) {
        document.body.insertBefore(overlay, document.body.firstChild);
      }
      else {
        document.body.appendChild(overlay);
      }
      if (this.selected) {
        this.renderer.setElementClass(this.elementRef.nativeElement.querySelectorAll('li')[this.selected], 'selected-row', true);
      }
      let clone = this.elementRef.nativeElement.querySelector('.pgi-drop-down').cloneNode(true);
      this.computePos(clone);
      overlay.appendChild(clone);
    }
  }

  /**
   * @private
   * @param {HTMLElement} ele The cloned element which will open on overlay
   * @memberof PGiDropdownComponent
   * This method computes the position of the element to open. It can open on top
   * if there is no space below it otherwise it open below.
   */
  private computePos(ele: HTMLElement) {
    let top = this.selectedDiv.getBoundingClientRect().top;
    let left = this.selectedDiv.getBoundingClientRect().left;
    let visible = this.isDropDownVisible(top);
    this.renderer.setElementClass(ele, 'absolute', true);
    this.renderer.setElementStyle(ele, 'left', left + 'px');
    this.renderer.setElementStyle(ele, 'display', 'inline-flex');
    if (visible) {
      this.renderer.setElementStyle(ele, 'top', (top + this.heightOfTextElement()) + 'px');
    } else {
      this.renderer.setElementStyle(ele, 'top', (top - this.dropDownHeight) + 'px');
    }
  }

  /**
   * @private
   * @param {number} val The dropdown list element selected
   * @memberof PGiDropdownComponent
   * This method selects the particular list element and removes the selected-row class
   * from the previous selected list element
   *
   */
  private dropDownSelected(val: number) {
    this.mainTxt = this.options[val].value;
    this.secTxt = this.options[val].description;
    if (this.selected) {
      this.renderer.setElementClass(this.elementRef.nativeElement.querySelectorAll('li')[this.selected], 'selected-row', false);
    }
    this.selected = val;
  }

  /**
   * @private
   * @returns {number} the height of the dropDown text field.
   * @memberof PGiDropdownComponent
   * returns the height of the dropDown text field.
   */
  private heightOfTextElement(): number {
    return this.selectedDiv.offsetHeight;
  }

  /**
   * @private
   * @returns {number} the height of the dropDown List.
   * @memberof PGiDropdownComponent
   * Computes the height of the dropDown List and returns it.
   */
  private heightOfDropDown(): number {
    let heightOfText = this.heightOfTextElement();
    let dropDownType = TYPE_DOUBLE_LINE;
    let paddingValue = 12;
    let listElementPadding = this.type === dropDownType ? 0 : paddingValue;
    let heightOfDropDowm = (this.listCount > this.itemsCount ? this.itemsCount : this.listCount) * (heightOfText + listElementPadding);
    this.dropDownHeight = heightOfDropDowm;
    return heightOfDropDowm;
  }

  /**
   * @private
   * @param {number} top The offset top of the dropdown.
   * @returns {boolean} This method return whether the dropDowm list is visible on the view port or not.
   * @memberof PGiDropdownComponent
   */
  private isDropDownVisible(top: number): boolean {
    let heightOfText = this.heightOfTextElement();
    let isVisible = true;
    if (this.window.nativeWindow.innerHeight < (top + this.dropDownHeight + heightOfText)) {
      isVisible = false;
    }
    return isVisible;
  }

  /**
   * @private
   * @returns {number}
   * @memberof PGiDropdownComponent
   * This method returns the size of the drop down list element.
   */
  private getListElementsSize(): number {
    return this.options.length;
  }
}
