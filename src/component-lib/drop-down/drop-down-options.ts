export class DropdownOptions {
  value: string;
  description: string;
  disabled: boolean;

  constructor(value, desc?, disabled = false) {
    this.value = value;
    this.description = desc;
    this.disabled = disabled;
  }
}
