import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';
import { PGiDropdownComponent } from './drop-down.component';
import { PGiDropdownComponentPage } from './drop-down.page';
import { PGiIconsModule, PGI_ICON_SET_URL } from '../icons';
import { WindowRef } from './window.service';
import { HttpModule } from '@angular/http';


@Component({
  template: `<pgi-drop-down id='123' (click)='dropDownClick()' titleText='Choose your option'
   type = 'doubleLine' ></pgi-drop-down>`
})

class TestWrapper {
  clickCount = 0;
  isDisabled = false;

  dropDownClick() {
    this.clickCount++;
  }
}

describe('DropDownComponent', () => {
  let component: PGiDropdownComponent;
  let fixture: ComponentFixture<TestWrapper>;
  let componentPage: PGiDropdownComponentPage;
  let fixtureInstance;

  beforeEach(async(() => {
    let windowMock: Window = <any>{ nativeWindow: <any>{ innerHeight: 760 } };
    TestBed.configureTestingModule({
      declarations: [PGiDropdownComponent, TestWrapper],
      imports: [PGiIconsModule, HttpModule],
      providers: [
        { provide: PGI_ICON_SET_URL, useValue: 'src/component-lib/assets/iconSet.svg', },
        { provide: WindowRef, useFactory: (() => { return windowMock; }) }
      ],
    })
      .compileComponents().then(() => {
        fixture = TestBed.createComponent(TestWrapper);
        fixture.autoDetectChanges();

        let el = fixture.debugElement.query(By.css('pgi-drop-down'));
        componentPage = new PGiDropdownComponentPage(el);
        component = el.componentInstance;
        fixtureInstance = fixture.debugElement.componentInstance;

      });
  }));

  afterEach(() => {
    let comp = document.body.getElementsByClassName('overlay-container')[0];
    if (comp) {
      comp.parentNode.removeChild(comp);
    }
  });


  it('should be created', () => {
    expect(componentPage).toBeTruthy();
  });

  it('type to be double line', () => {
    component.type = 'doubleLine';
    fixture.detectChanges();
    expect(componentPage.getDropDownListTextField().classList.contains('double-line')).toBe(true);
  });

  it('type to be single line', () => {
    component.type = 'singleLine';
    fixture.detectChanges();
    expect(componentPage.getDropDownListTextField().classList.contains('double-line')).toBe(false);

  });

  it('should be clickable', () => {
    componentPage.getDropDownListTextField().click();
    expect(fixtureInstance.clickCount).toBe(1);
  });

  it('should add class disabled when disabled', () => {
    component.disabled = "true";
    fixture.detectChanges();
    expect(componentPage.getDropDownListTextField().parentNode.classList.contains('drop-down-disabled')).toBe(true);
  });


  it('should open on overlay when clicked', () => {
    component.options = componentPage.getOptionsArray();
    fixture.detectChanges();
    componentPage.getDropDownListTextField().click();
    expect(componentPage.getOverLay()).toBeTruthy();
  });

  it('should open pgi drop-down-list on overlay when clicked', () => {
    component.options = componentPage.getOptionsArray();
    fixture.detectChanges();
    componentPage.getDropDownListTextField().click();
    expect(componentPage.getDropDownOnOverLay()).toBeTruthy();
  });

  it('should close when clicked on overlay', () => {
    component.options = componentPage.getOptionsArray();
    fixture.detectChanges();
    componentPage.getDropDownListTextField().click();
    expect(componentPage.getDropDownOnOverLay()).toBeTruthy();
    componentPage.getOverLay().click();
    expect(componentPage.getOverLay()).toBeFalsy();

  });

  it('should show error if error message', () => {
    component.errorMessage = "Error";
    fixture.detectChanges();
    expect(componentPage.getDropDownListTextField().classList.contains('selected-div-error')).toBe(true);
  });

  it('should show titleText if titleText given', () => {
    component.titleText = "Pgi Drop Down";
    fixture.detectChanges();
    expect(componentPage.getTitleLabel().textContent).toEqual("Pgi Drop Down");
  });

});
