import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PGiDropdownComponent } from './drop-down.component';
import { PGiIconsModule } from '../icons';
import { WindowRef } from './window.service';


@NgModule({
  imports: [
    CommonModule,
    PGiIconsModule
  ],
  providers: [WindowRef],
  declarations: [PGiDropdownComponent],
  exports: [PGiDropdownComponent]
})
export class PGiDropdownModule { }
