// NOTE This file expects the $pgi-font map to be defined
@import './utils';

/*
pgi-font-face

The `pgi-font-face($name)` function returns an available font face
for a theme.

### Options
- **heading**
- **body**
- **code**

```scss
.my-component {
  font-family: pgi-font-face(body);
}
```

Styleguide theming.font-face
*/
@function pgi-font-face($name){
  @return pgi-theme-property($pgi-fonts, faces, $name);
}

/*
pgi-font-weight

The `pgi-font-weight($weight)` function returns an available font weight
for a theme.

### Options
- **light**
- **normal**
- **semi-bold**
- **bold**

```scss
.my-component {
  font-weight: pgi-font-weight(normal);
}
```

Styleguide theming.font-weight
*/
@function pgi-font-weight($weight){
  @return pgi-theme-property($pgi-fonts, weights, $weight);
}

/*
pgi-font-size

The `pgi-font-size($size)` function returns an available font size
for a theme.

### Options
- **xs**
- **s**
- **m**
- **l**
- **xl**
- **xxl**

```scss
.my-component {
  font-size: pgi-font-size(l);
}
```

Styleguide theming.font-size
*/
@function pgi-font-size($size){
  @return pgi-rem( pgi-theme-property($pgi-fonts, sizes, $size) );
}

/*
pgi-line-height

The `pgi-line-height($size)` function returns an available line height
for a theme.

### Options
- **normal**
- **tight**

```scss
.my-component {
  line-height: pgi-line-height(normal);
}
```

Styleguide theming.line-height
*/
@function pgi-line-height($height){
  @return pgi-theme-property($pgi-fonts, line-height, $height);
}

/*
pgi-base-text

The `pgi-base-text($theme)` mixin outputs the styles for the base text
for an application (font-family, color, font-size, line-height). It
can be used anywhere you need to explicity set text metrics to match
the base text styling.

In most cases you should not need to use this mixin directly since
these styles should cascade from the html element.

### Parameters
- **$theme** The current theme map to use when setting colors.

```scss
.my-component {
  @include pgi-base-text($pgi-light-theme);
}
```

Styleguide theming.base-text
*/
@mixin pgi-base-text($theme){
  $foreground: pgi-get($theme, foreground);

  @include pgi-text(base);
  font-family: pgi-font-face(body);
  color: pgi-color($foreground, text);
}

/*
pgi-text

The `pgi-text($size)` mixin sets the font size, line height and margins
for a element. It is useful for styling the default html elements or reusing
the default html element styles in your own components.

### Options
- **h1**   : A large title with lots of spacing.
- **h2**   : A medium title with some spacing.
- **h3**   : A small title with space around it.
- **h4**   : A small title that fits in the same space as a line of regular text.
- **large**: Large text (same size as h3) that does *not* leave spacing around itself.
- **small**: Small-ish text (between "large" and "base") that does *not* leave spacing around itself.
- **p**    : Regular text that leaves space below itself.
- **base** : Regular text that does *not* leave spacing around itself.
- **sub**  : Subscript text that does *not* leave space around itself.

```scss
h1, .my-heading {
  @include pgi-text(h1);
}
```

Styleguide theming.text
*/
@mixin pgi-text($size){
  @include pgi-text-size($size);
  @include pgi-text-margins($size);
}


/*
pgi-text-size

The `pgi-text-size($size)` mixin is similar to `pgi-text()` except that
it will **only** set the font size and line height. This is useful if you
need to replicate an element's sizing in your custom component.

### Options
Takes the same options as `pgi-text()`.


```scss
.my-component {
  @include pgi-text-size(large);
}
```

> NOTE: This method sets both font-size and line-height. To get just
a font-size, use the pgi-font-size function.

Styleguide theming.text-size
*/
@mixin pgi-text-size($size){
  @if $size == h1 {
    font-size: pgi-font-size(xxl);
    line-height: gp-rows() * 3;
  }
  @else if $size == h2 {
    font-size: pgi-font-size(xl);
    line-height: gp-rows() * 2;
  }
  @else if $size == h3 {
    font-size: pgi-font-size(l);
    line-height: gp-rows() * 2;
  }
  @else if $size == h4 {
    font-size: pgi-font-size(m);
    line-height: gp-rows();
  }
  @else if $size == large {
    font-size: pgi-font-size(l);
    line-height: gp-rows();
  }
  @else if $size == small or $size == button {
    font-size: pgi-font-size(s);
    line-height: gp-rows();
  }
  @else if $size == sub {
    font-size: pgi-font-size(xs);
    line-height: gp-rows();
  }
  @else {
    font-size: pgi-font-size(m);
    line-height: gp-rows();
  }
}

/*
pgi-text-margins

The `pgi-text-margins($size)` mixin is similar to `pgi-text()` except
it will **only** set margins. This is most
useful if you want a block level element (or custom component) to
flow on the page with the same spacing as one of the existing
elements (usually "p" or "h1").

### Options
Takes the same options as `pgi-text()`.

```scss
.my-component {
  @include pgi-text-margins(p);
}
```

Styleguide theming.text-margins
*/

@mixin pgi-text-margins($size){
  @if $size == h1 {
    margin-top: gp-rows();
    margin-bottom: gp-rows() * 2;
  }
  @else if $size == h2 {
    margin-top: gp-rows();
    margin-bottom: 0;
  }
  @else if $size == h3 or $size == h4 {
    margin-top: gp-rows();
    margin-bottom: 0;
  }
  @else if $size == p {
    margin-top: 0;
    margin-bottom: gp-rows();
  } 
  @else if $size == button {
    margin: 0;
  }

}

/*
 pgi-letter-spacing($size)

Similar to `pgi-text-margins()` except it will only set letter spacing. You can pass
the type of element and it will set its letter-spacing.

**$size** - See the documentation for `pgi-text()`. The parameter
            value is the element whose letter-spacing we want to set.


```scss
.my-component {
  @include pgi-letter-spacing(button);
}
```

Styleguide theming.sizing
*/

@function pgi_spacing($size) {
  @return pgi-rem( pgi-theme-property($pgi-fonts, letter-spacing, $size) );
}

@mixin pgi-letter-spacing($size){
  @if $size == h1 {
    letter-spacing: pgi_spacing(xl);
  }
  @else if $size == h2 {
    letter-spacing: pgi_spacing(l);
  }
  @else if $size == h3 or $size == h4 {
    letter-spacing: pgi_spacing(m);
  }
  @else if $size == p {
    letter-spacing: pgi_spacing(s);
  }
  @else if $size == button {
    letter-spacing: pgi_spacing(m);
  }
}

/*
pgi-ellipsis

The `pgi-ellipsis()` mixin will add ellipsis to a line of text
when it gets truncated.

> NOTE This will only work for lines of text
that do not wrap to multiple lines.

```scss
.my-component {
  @include pgi-ellipsis();
}
```

Styleguide theming.ellipsis
*/
@mixin pgi-ellipsis {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
