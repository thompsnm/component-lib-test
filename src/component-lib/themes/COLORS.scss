/*
About Colors

This project uses a similar theming paradigm to Angular 2 Material.
Colors are broken into several palettes (primary, accent, warn, background, foreground).
You can see examples of each of these palettes below.

- [**Brand**](/colors#brand-palette) is used to specify colors that match the customer's brand.
- [**Primary**](/colors#primary-palette) is used to call special attention to the primary actions on a page.
- [**Accent**](/colors#accent-palette) is used in places that need special attention but are not the primary action of a page
  or for actions that need to be placed on top of a primary palette color.
- [**Warn**](/colors#warn-palette) is used for error messaging.
- [**Foreground**](/colors#foreground-palette) is used to specify text and border colors.
- [**Background**](/colors#background-palette) is used to define element background colors and hover states.

More information about the original Marterial color spec can be found in the
[Material Guide](https://material.io/guidelines/style/color.html#color-color-schemes)

Styleguide colors
*/


/*
Understanding Theme Colors

Themes allow an application to change its colors and element sizing for different customers/use cases.
For example, this project contains themes for PGi and GlobalMeet. The GlobalMeet theme defines
colors that are specific to the GlobalMeet brand such as the orange brand color used in its logo.
Any application that uses this component library can be built with either theme to completely
change the colors of that theme.

Themes are always interchangable without any code modifications. To acheive this, the components
in this library and any applications that want to be theme-able must implement colors and sizing
in a generic manner. This is acheived by referencing colors using color palettes in a theme object
and by using the `pgi-*` SASS mixins to access themable values.

In addition to having themes like PGi and GlobalMeet, there are also always dark and light versions
of a theme. This allows applications to dynamically switch their background color and make sure
that all foreground content is still readable. You can easily query whether a theme is dark so that you
can style elements conditionally.

```scss
// Here is an example of the theme object structure:
$theme: (
  brand: (),      // Color palette defining the current brand colors
  primary: (),    // Color palette defining the primary element colors
  accent: (),     // Color palette for page accents
  warn: (),       // Color palette for error messages
  grayscale: (),  // Color palette for grayscale colors
  foreground: (), // Color palette to use for foreground text, dividers, borders, etc.
  background: (), // Color palette to use for element backgrounds, hover states, etc.
  other: (),      // Color palette used for custom PGi needs that can't be described in the palettes above.
  isDark: true    // Tells you whether this palette has a dark background with light foreground.
);

// You can access and use these palettes as follows:
$brand: pgi-get( $theme, brand );
$dark: pgi-get( $theme, isDark );

// If this is a dark theme, use the 200 hue from the palette
// as the background color. Otherwise, use the 700 hue.
$hue: if( $dark, 200, 700 );
$background-color: pgi-get( $brand, $hue );

.my-thing {
  background-color: $background-color;
}
```

Styleguide colors.understanding-themes
*/


/*
Understanding Palettes

Colors in this component library are always accessed from color palettes. There are 3 main
color palette types:

### Scaled Color Palette

These include the brand, primary, accent and warn palettes as described below.
They list their colors as a range of "hues" from 50 - 900, plus four high contrast
variants (A100 - A700). The 500 value from the color range is always the base (default) color
for the palette. For example, in the brand palette, the 500 hue will usually match the
main spot color from a customer's logo. From there the color values get lighter or darker
by assending/descending through the hue numbers. This way designers and developers have
an easy way to get colors that work together without having to define many one-off color
variations when they cannot use one of the main colors.

In addition to the hue values, scaled color palettes include a contrast color for each
hue in the palette. This contrast color is the text color value that should be used as
the foreground on top of that color. This way developers can always be sure that the
text color is readable on top of a palette color.

```scss
// Here is an example of the scaled color palette object:
$primary: (
  50: 'dddddd',     // This will be the lightest hue in the palette.
  100: 'cccccc',
  200: 'bbbbbb',
  300: 'aaaaaa',
  400: '999999',
  500: '888888',    // This is the main (default) hue in the palette.
  600: '777777',
  700: '666666',
  800: '555555',
  900: '444444',    // This will be the darkest hue in the palette.
  A100: 'ffffff',   // This is a higher contrast version of the lightest hue.
  A200: 'eeeeee',
  A400: '111111',
  A700: '000000'    // This is a higher contrast version of the darkest hue.
  contrast: (       // This property lists contrast colors for each hue.
    50: '000000',   // This contrast color will always be legible on top of hue 50 above.
    100: '000000',
    ...
    900: 'ffffff',  // This color will always be legible on top of hue 900 above.
    A100: '000000',
    ...
    A700: 'ffffff'
  )
)

// And now you can use these colors like this:
$background-color: pgi-get( $primary, 500 );
$contrasts: pgi-get( $primary, contrast );
$foreground-color: pgi-get( $contrasts, 500 );

.my-thing {
  background-color: $background-color;
  color: $foreground-color;
}
```

### Foreground Color Palettes

Foreground palettes define the colors for foreground elements such as text color,
icon color, horizontal rules, disabled text, ect. Unlike the scaled color palettes,
you cannot ask for a lighter or darker version of a foreground palette color. There
is only one horizontal rule color defined in this palette.

However, foreground colors should generally be defined as partially transparent.
This way the foreground color can be placed on any other color and still look good.
For example, if your foreground text color is defined as `rgba( #000000, 0.8 )`, you
know it will look just as good on a blue background as it will an orange background.

See [the foreground palette](/colors#foreground-palette) definition
for the available foreground palette colors. Also notice that there are both light
and dark variations of the foreground palette which are used by their respective
light and dark themes.

### Background Color Palettes

Background palettes define the colors for background elements such as cards, dialogs,
hover states, etc. Like the foreground palettes, there is only one color variation for
each property (ie. you cannot get a lighter/darker version of the hover style from
the palette).

See [the background palette](/colors#background-palette) definition
for the available background palette colors. Also notice that there are both light
and dark variations of the palette which are used by their respective
light and dark themes.

Styleguide colors.understanding-palettes
*/



/*
Brand Palette

This palette is used to theme an application to match the brand of the company
for whom the product is built. It is intended to make the application feel like
their own application but this palette is not used for all primary actions (instead
the primary palette is used for that).

It is generally used for
titles, page headers and other similar branded sections of the application.
The colors in this palette may be very similar to those in one of the other
palettes (ex. an orange or red logo may be similar to the warn colors/states).
For that reason, this palette should be used minimally to convey the brand
and not used for action elements (buttons) which could be confused for
destructive actions if the brand color is similar to the warn palette.

Markup:
<palette color="brand"></palette>

Styleguide colors.brand-palette
*/

/*
Primary Palette

The primary palette is used to represent primary actions on a page.
It is differentiated from the brand palette for cases where the
brand color is very close to the warn palette.

It is possible for the primary and brand palettes to be the same
(the PGi palette is an example of this).

Markup:
<palette color="primary"></palette>

Styleguide colors.primary-palette
*/

/*
Accent Palette

The accent palette is used in situations that require drawing special
attention to functionality but where the brand/primary palettes cannot be used.
For example, if the primary palette is being used to color the page header,
the accent palette can be used for primary actions in that header.

Markup:
<palette color="accent"></palette>

Styleguide colors.accent-palette
*/

/*
Warn Palette

The warn palette is used for error messaging/states.

Markup:
<palette color="warn"></palette>

Styleguide colors.warn-palette
*/

/*
Grayscale Palette

This palette is used to define the background colors of the application
as well as text and border colors.

Markup:
<palette color="background"></palette>

Styleguide colors.grayscale-palette
*/


/*
Background Colors

The following shows the background colors used for different Material layout
sections using the light theme. These colors come from the background
palette.

Markup:
<div class="debug ex-background ex-background-status-bar">status-bar</div>
<div class="debug ex-background ex-background-control-bar-v1">control-bar-v1</div>
<div class="debug ex-background ex-background-control-bar-v2">control-bar-v2</div>
<div class="debug ex-background ex-background-nav-bar">nav-bar</div>
<div class="debug ex-background ex-background-app-bar">app-bar</div>
<div class="debug ex-background ex-background-background">background</div>
<div class="debug ex-background ex-background-background-v2">background-v2</div>
<div class="debug ex-background ex-background-card">card</div>
<div class="debug ex-background ex-background-dialog">dialog</div>
<div class="debug ex-background ex-background-button">button</div>
<div class="debug ex-background ex-background-hover">hover</div>
<div class="debug ex-background ex-background-hover-stroke">hover-stroke</div>
<div class="debug ex-background ex-background-pressed">pressed</div>
<div class="debug ex-background ex-background-pressed-stroke">pressed-stroke</div>
<div class="debug ex-background ex-background-disabled-button">disabled-button</div>
<div class="debug ex-background ex-background-raised-button">raised-button</div>

Styleguide colors.background-palette
*/



/*
Dark Theme Background Colors

Here are the background colors that come with the dark theme. These colors
are also pulled from the background palette.

Markup:
<div class="ex-dark ex-background-background">
  <div class="debug ex-background ex-background-status-bar">status-bar</div>
  <div class="debug ex-background ex-background-control-bar">control-bar</div>
  <div class="debug ex-background ex-background-nav-bar">nav-bar</div>
  <div class="debug ex-background ex-background-app-bar">app-bar</div>
  <div class="debug ex-background ex-background-background">background</div>
  <div class="debug ex-background ex-background-background-v2">background-v2</div>
  <div class="debug ex-background ex-background-button">button</div>
  <div class="debug ex-background ex-background-hover">hover</div>
  <div class="debug ex-background ex-background-hover-stroke">hover-stroke</div>
  <div class="debug ex-background ex-background-card">card</div>
  <div class="debug ex-background ex-background-pressed">pressed</div>
  <div class="debug ex-background ex-background-pressed-stroke">pressed-stroke</div>
  <div class="debug ex-background ex-background-dialog">dialog</div>
  <div class="debug ex-background ex-background-disabled-button">disabled-button</div>
  <div class="debug ex-background ex-background-raised-button">raised-button</div>
</div>

Styleguide colors.dark-background-palette
*/



/*
Foreground Colors

These are the colors that are used to define foreground elements like
text and border colors.

Markup:
<div class="ex-background"><span class="ex-foreground-base">base                       </span><span class="debug ex-color-bar ex-background-base"            ></span></div>
<div class="ex-background"><span class="ex-foreground-divider">divider                 </span><span class="debug ex-color-bar ex-background-divider"         ></span></div>
<div class="ex-background"><span class="ex-foreground-disabled-button">disabled-button </span><span class="debug ex-color-bar ex-background-disabled-button" ></span></div>
<div class="ex-background"><span class="ex-foreground-disabled-text">disabled-text     </span><span class="debug ex-color-bar ex-background-disabled-text"   ></span></div>
<div class="ex-background"><span class="ex-foreground-hint-text">hint-text             </span><span class="debug ex-color-bar ex-background-hint-text"       ></span></div>
<div class="ex-background"><span class="ex-foreground-secondary-text">secondary-text   </span><span class="debug ex-color-bar ex-background-secondary-text"  ></span></div>
<div class="ex-background"><span class="ex-foreground-icon">icon                       </span><span class="debug ex-color-bar ex-background-icon"            ></span></div>
<div class="ex-background"><span class="ex-foreground-text">text                       </span><span class="debug ex-color-bar ex-background-text"            ></span></div>

Styleguide colors.foreground-palette
*/



/*
Dark Foreground Colors

These are the colors that are used to define foreground elements when using
the dark theme.

Markup:
<div class="ex-foreground-dark">
  <div class="ex-background"><span class="ex-foreground-base">base                       </span><span class="debug ex-color-bar ex-background-base"            ></span></div>
  <div class="ex-background"><span class="ex-foreground-divider">divider                 </span><span class="debug ex-color-bar ex-background-divider"         ></span></div>
  <div class="ex-background"><span class="ex-foreground-disabled-button">disabled-button </span><span class="debug ex-color-bar ex-background-disabled-button" ></span></div>
  <div class="ex-background"><span class="ex-foreground-disabled-text">disabled-text     </span><span class="debug ex-color-bar ex-background-disabled-text"   ></span></div>
  <div class="ex-background"><span class="ex-foreground-hint-text">hint-text             </span><span class="debug ex-color-bar ex-background-hint-text"       ></span></div>
  <div class="ex-background"><span class="ex-foreground-secondary-text">secondary-text   </span><span class="debug ex-color-bar ex-background-secondary-text"  ></span></div>
  <div class="ex-background"><span class="ex-foreground-icon">icon                       </span><span class="debug ex-color-bar ex-background-icon"            ></span></div>
  <div class="ex-background"><span class="ex-foreground-text">text                       </span><span class="debug ex-color-bar ex-background-text"            ></span></div>
</div>

Styleguide colors.dark-foreground-palette
*/



/*
Color Classes

In general, you should style your components using the
[Material theming directions](https://material.angular.io/guide/theming-your-components).
However, if you need it, there are classes for each of
the palettes.

Markup:
<div class="ex-background brand pgi-palette-background-800">Brand Palette 800</div>
<div class="ex-background primary pgi-palette-background-500">Primary Palette 500</div>
<div class="ex-background accent pgi-palette-background-400">Accent Palette 400</div>
<div class="ex-background warn pgi-palette-background-300">Warn Palette 300</div>
<div class="ex-background background pgi-palette-background-200">Background Palette 200</div>
<div class="primary">
  <div class="ex-background pgi-palette-background-200">Or nested 200</div>
  <div class="ex-background pgi-palette-background-50">Or nested 50</div>
</div>

Styleguide colors.palette-classes
*/

