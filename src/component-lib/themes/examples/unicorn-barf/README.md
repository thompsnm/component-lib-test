# Unicorn Barf Example Theme

This theme shows how you can radically change the styles of the component library
by overriding all of the color, font and size data.

## Running this example

You can run any of the themes by passing the '--theme' ('-t' for short) parameter
when building the example. It's also a good idea to pass the '--clean' ('-c' for short)
parameter to make sure all assets are cleaned out.

`npm start -- -c -t examples/unicorn-barf`

## Things to note in this example

- It is possible to override all of the colors in a theme.
- It is even possible to override the colors used for black and white which are
  the basis for many of the text and divider colors.
- You can change the fonts used and the relative font sizes, including the base font
  size which is used to determine the sizing grid for components.
- The sizing of all components fits the large text size used in this theme by setting
  the graph paper row size in the sizes file.
- Some parts of the theme still use the default pgi theme. Specifically the animation
  and typeography settings.
- This theme falls back to using the icons from the pgi theme because there is no
  icons folder in this theme.
- This theme specifies some custom fonts in the `assets/fonts/google-fonts.neon` file.

