# Minimal Example Theme

This theme shows the most minimal theme possible. It uses all of the default
pgi theme settings except for overriding a single color. It is unlikely that
you would ever need such a skinny theme but it can be a good starting place
for defining a new theme.

## Running this example

You can run any of the themes by passing the '--theme' ('-t' for short) parameter
when building the example. It's also a good idea to pass the '--clean' ('-c' for short)
parameter to make sure all assets are cleaned out.

`npm start -- -c -t examples/minimal`

## Things to note in this example

- It demonstrates the minimum requirements of a new theme.
- It shows how to extend the pgi theme color palette.
- It shows how fallbacks are used for assets like icons, fonts and shared SASS/JS data.

