# PGi Theme

This is the default component library theme. It is used as the basis for PGi applications
as well as other themes. You can checkout the `themes/examples` folder to see how to
extend this theme for your own applications.

It is not required that you define your own themes in this project. It is possible to
define custom themes directly in your app or in a separate repo so it can be shared
by multiple projects.

## Running this theme

You can run any of the themes by passing the '--theme' ('-t' for short) parameter
when building the example. It's also a good idea to pass the '--clean' ('-c' for short)
parameter to make sure all assets are cleaned out.

To run this theme, simply use:

`npm start -c`

