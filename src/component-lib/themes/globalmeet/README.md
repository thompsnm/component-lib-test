# Global Meet Theme

Use this theme to provide Global Meet styling to an application.

## Running this example

You can run any of the themes by passing the '--theme' ('-t' for short) parameter
when building the example. It's also a good idea to pass the '--clean' ('-c' for short)
parameter to make sure all assets are cleaned out.

`npm start -- -c -t globalmeet`

