import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { MdTooltip } from '@angular/material';

@Directive({
  selector: '[pgiTooltip]',
  exportAs: 'pgiTooltip'
})
export class PGiTooltip extends MdTooltip {

  private _positionSet = false;

  ngOnInit() {
    if (this._positionSet === false) {
      this._matPosition = "above";
    }
  }

  @Input('pgiTooltip')
  get _pgiMessage() { return this._matMessage; }
  set _pgiMessage(v) { this._matMessage = v; }

  @Input('pgiTooltipPosition')
  get _pgiPosition() { return this._matPosition; }
  set _pgiPosition(p) {
    this._matPosition = p;
    this._positionSet = true;
  }
}
