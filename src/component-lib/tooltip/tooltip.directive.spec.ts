import { Component } from '@angular/core';
import { async, TestBed, ComponentFixture, tick, fakeAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { PGiTooltipPage } from './tooltip.directive.page';

@Component({
  template: `<span pgiTooltip="Tippy">Tooltip</span>`
})
class TestWrapper { }

describe('PGiTooltip', () => {
  let fixture: ComponentFixture<TestWrapper>;
  let testComponent: TestWrapper;
  let tooltipDirective: PGiTooltipPage;

  beforeEach(async(() => {
    PGiTooltipPage.makeTestBed(TestWrapper)
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(TestWrapper);
        fixture.autoDetectChanges();

        testComponent = fixture.componentInstance;
        let el = fixture.debugElement.query(By.css('span'));
        tooltipDirective = new PGiTooltipPage(el);
      });
  }));

  it('should exist', () => {
    expect(tooltipDirective).toBeTruthy();
  });

  it('should not show the tooltip message.', () => {
    expect( tooltipDirective.messageIsVisible() ).toBe(false);
  });

  describe('on mouse over', () => {
    beforeEach( fakeAsync(() => {
      tooltipDirective.mouseEnter();
      tick();
    }));

    it('should show the tooltip message element.', () => {
      expect(tooltipDirective.messageIsVisible()).toBe(true);
    });

    it('should show the correct message.', () => {
      expect(tooltipDirective.message()).toEqual('Tippy');
    });

    describe('and then on mouse leave', () => {
      beforeEach( fakeAsync(() => {
        tooltipDirective.mouseLeave();
        tick();
      }));

      it('should hide the tooltip message.', () => {
        expect(tooltipDirective.messageIsVisible()).toBe(false);
      });
    })
  });
});
