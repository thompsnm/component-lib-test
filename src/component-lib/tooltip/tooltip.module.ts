import { NgModule } from '@angular/core';
import { PGiTooltip } from './tooltip.directive';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '@angular/material';

@NgModule({
  declarations: [
    PGiTooltip
  ],
  imports: [
    NoopAnimationsModule,
    MaterialModule
  ],
  exports: [
    PGiTooltip
  ]
})
export class PGiTooltipModule { }
