import { TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { PGiTooltipModule } from './tooltip.module';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';

export class PGiTooltipPage {
  static makeTestBed(container) {
    return TestBed
      .configureTestingModule({
        imports: [PGiTooltipModule],
        declarations: [container]
      })
      // Remove sub-dependencies of module under test
      .overrideModule(PGiTooltipModule, {
        remove: {
          imports: [ BrowserAnimationsModule ]
        },
        add: {
          imports: [ NoopAnimationsModule ]
        }
      });
  }

  constructor(private root: DebugElement) { }

  private makeMouseEnterEvent(): Event {
    let event = new Event('MouseEvents');
    event.initEvent('mouseenter', true, true);
    return event;
  }

  private makeMouseLeaveEvent(): Event {
    let event = new Event('MouseEvents');
    event.initEvent('mouseleave', true, true);
    return event;
  }

  mouseEnter() {
    this.root.nativeElement.dispatchEvent(this.makeMouseEnterEvent());
  }

  mouseLeave() {
    this.root.nativeElement.dispatchEvent(this.makeMouseLeaveEvent());
  }

  _getTooltipOverlayElement() {
    return document.querySelector('md-tooltip-component');
  }

  messageIsVisible() {
    let overlayWrapper = this._getTooltipOverlayElement();
    if (overlayWrapper) {
      return overlayWrapper.querySelectorAll('.mat-tooltip').length === 1;
    }
    else {
      return false;
    }
  }

  message() {
    return this._getTooltipOverlayElement().querySelector('.mat-tooltip').textContent.trim();
  }
}
