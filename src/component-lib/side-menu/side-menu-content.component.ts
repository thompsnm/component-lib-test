import { Component } from '@angular/core';

@Component({
  selector: 'pgi-side-menu-content',
  template: '<ng-content></ng-content>'
})
export class PGiSideMenuContent {}
