import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { CollapseService } from '../collapse.service';

@Component({
  selector: 'pgi-side-menu-item',
  templateUrl: './side-menu-item.component.html',
  encapsulation: ViewEncapsulation.None
})
export class PGiSideMenuItem implements OnInit {
  @Input() linkTarget: string;

  constructor(private collapseService: CollapseService) {}

  getCollapse() {
    return this.collapseService.collapsed;
  }

  ngOnInit() {}
}
