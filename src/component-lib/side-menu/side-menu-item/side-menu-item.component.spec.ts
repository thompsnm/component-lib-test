import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PGiSideMenuItem } from './side-menu-item.component';
import { CollapseService } from '../collapse.service';

describe('PGiSideMenuItem', () => {
  let component: PGiSideMenuItem;
  let fixture: ComponentFixture<PGiSideMenuItem>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PGiSideMenuItem ],
      providers: [ CollapseService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PGiSideMenuItem);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
