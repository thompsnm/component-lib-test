import { Component, Input, ViewEncapsulation } from '@angular/core';

import { CollapseService } from './collapse.service';

@Component({
  selector: 'pgi-side-menu',
  templateUrl: './side-menu.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [CollapseService]
})
export class PGiSideMenu {
  @Input() contentClasses: string;
  @Input() menuClasses: string;

  collapse: boolean;
  constructor(public collapseService: CollapseService) {
    this.collapse = this.collapseService.collapsed;
  }

  toggleCollapse() {
    this.collapseService.toggleCollapse();
    this.collapse = this.collapseService.collapsed;
  }

  isMenuCollapsed() {
    return this.collapse;
  }

}
