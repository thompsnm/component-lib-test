import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {By} from '@angular/platform-browser';

import { PGiSideMenu } from './side-menu.component';
import { CollapseService } from './collapse.service';

describe('PGiSideMenu', () => {
  let component: PGiSideMenu;
  let fixture: ComponentFixture<PGiSideMenu>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PGiSideMenu ],
      providers: [ CollapseService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PGiSideMenu);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('collapse', () => {
    it('should call the service', () => {
      // NOTE have to spy on instance of service in component
      const serviceSpy = spyOn(component.collapseService, 'toggleCollapse');
      component.toggleCollapse();
      expect(serviceSpy).toHaveBeenCalled();
    });

    it('should update the collapse propery', () => {
      expect(component.isMenuCollapsed()).toBeFalsy();
      component.toggleCollapse();
      expect(component.isMenuCollapsed()).toBeTruthy();
    });

    it('should have collapsed class', () => {
      component.toggleCollapse();
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('.collapsed'))).not.toBe(null);
    });
  });
});
