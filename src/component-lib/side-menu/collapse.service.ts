import { Injectable } from '@angular/core';

@Injectable()
export class CollapseService {

  collapsed = false;

  toggleCollapse() {
    this.collapsed = !this.collapsed;
  }
}
