import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PGiSideMenu } from './side-menu.component';
import { PGiSideMenuItem } from './side-menu-item/side-menu-item.component';
import { PGiSideMenuContent } from './side-menu-content.component';

@NgModule({
  declarations: [
    PGiSideMenu,
    PGiSideMenuItem,
    PGiSideMenuContent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PGiSideMenu,
    PGiSideMenuItem,
    PGiSideMenuContent
  ],
  entryComponents: [
  ],
  providers: []
})
export class PGiSideMenuModule { }
