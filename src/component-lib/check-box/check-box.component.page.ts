import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

import { PGiCheckBoxComponent } from './check-box.component';
export class PGiCheckBoxPage {
    root: DebugElement = null;

    constructor (debugElement: DebugElement) {
        this.root = debugElement;
    }

    getComponent(): PGiCheckBoxComponent {
      let el = this.root.query(By.all());
      return el.componentInstance;
    }

    getInputElement() {
        return this.root.nativeElement.querySelector('input');
    }
}
