import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PGiCheckBoxComponent } from './check-box.component';
import { MdCheckboxModule } from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    MdCheckboxModule
  ],

  declarations: [PGiCheckBoxComponent],

  exports: [
    PGiCheckBoxComponent
  ]
})
export class PGiCheckBoxModule { }
