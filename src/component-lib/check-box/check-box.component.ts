import { Component, ViewEncapsulation, Input, Output, EventEmitter, ElementRef, AfterViewInit } from '@angular/core';
import { PGiValueAccessor } from '../inputs/value-accessor';

@Component({
  selector: 'pgi-check-box',
  templateUrl: './check-box.component.html',
  styleUrls: ['./check-box.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PGiCheckBoxComponent extends PGiValueAccessor implements AfterViewInit {
  @Input() ariaLabel: String;
  @Input() ariaLabelledby: String;
  @Input() id: String;
  @Input() required: Boolean = false;
  @Input() labelPosition: String = 'after';
  @Input() disabled: Boolean;
  @Input() name: String;
  @Input() value: String;
  @Input() color: String = 'primary';

  private _checked = false;
  @Input() set checked(checked: boolean) {
    this._checked = checked;
    this.change.emit(checked);
  }
  get checked(): boolean {
    return this._checked;
  }

  @Output() change: EventEmitter<Boolean> = new EventEmitter<Boolean>();

  constructor(private element: ElementRef) {
    super();
  }

  ngAfterViewInit() {
    let baseElement = this.element.nativeElement.querySelector('.mat-checkbox-inner-container');
    let halo = document.createElement('div');
    halo.classList.add('halo');
    baseElement.appendChild(halo);
  }

  checkChange($event: any) {
    this._checked = $event.checked;
    this.change.emit($event.checked);
  }
}
