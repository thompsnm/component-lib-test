import { Component, HostListener, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { PGiCheckBoxComponent } from './check-box.component';
import { PGiCheckBoxPage } from './check-box.component.page';
import { MaterialModule, MdCheckboxModule, MdCheckbox } from '@angular/material';

@Component({
  template: `
  <pgi-check-box (change)='changeWatcher($event)' [checked]='startChecked' [disabled]='disabledValue' [color]='colorName' value='test'>
    TestLabel
  </pgi-check-box>`
})
class TestWrapper {
  disabledValue = false;
  startChecked = true;
  colorName = 'primary';
  changeWatcher(newValue: boolean) {}
}

describe('PGiCheckBoxComponent', () => {
  let component: PGiCheckBoxComponent;
  let fixture: ComponentFixture<TestWrapper>;
  let page: PGiCheckBoxPage;
  let testWrapper, input;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MdCheckboxModule],
      declarations: [ PGiCheckBoxComponent, TestWrapper ]
    })
    .compileComponents().then( () => {
      fixture = TestBed.createComponent(TestWrapper);
      fixture.autoDetectChanges();
      page = new PGiCheckBoxPage(fixture.debugElement);
      component = page.getComponent();
      testWrapper = fixture.componentInstance;
      input = page.getInputElement();
      spyOn(testWrapper, 'changeWatcher').and.callThrough();
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have defaulted to checked', () => {
    expect(component.checked).toBe(true);
  });

  it('should register changes with a click', () => {
    input.click();
    expect(testWrapper.changeWatcher).toHaveBeenCalledWith(false);
    expect(component.checked).toBe(false);
  });

  it('should update checked value when input is changed', () => {
    expect(component.checked).toBe(true);
    testWrapper.startChecked = false;
    fixture.detectChanges();
    expect(component.checked).toBe(false);
  });

  it('should become disabled when the disabled value is updated', () => {
    testWrapper.disabledValue = true;
    fixture.detectChanges();
    input.click();
    expect(component.checked).toBe(true);
  });

  it('should send an event when the checked value is changed' , () => {
    testWrapper.startChecked = false;
    fixture.detectChanges();
    expect(testWrapper.changeWatcher).toHaveBeenCalledWith(false);
    expect(component.checked).toBe(false);
  });

  it('should have the propper value', () => {
    expect(component.value).toBe('test');
    expect(input.value).toBe('test');
  });

  it('should have default color', () => {
    expect(component.color).toBe('primary');
  });

  it('should have accent color', () => {
    testWrapper.colorName = 'accent';
    fixture.detectChanges();
    expect(component.color).toBe('accent');
  });
});
