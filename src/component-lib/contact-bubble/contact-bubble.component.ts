import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { initServicesIfNeeded } from '@angular/core/src/view/services';

const PRESENCES = ['available', 'away', 'do-not-disturb', 'offline'];
const DEFAULT_PRESENCE = 'offline';

@Component({
  selector: 'pgi-contact-bubble',
  templateUrl: './contact-bubble.component.html',
  encapsulation: ViewEncapsulation.None
})
export class PGiContactBubble implements OnInit {
  @Input() contactImage: string;
  @Input() presence: string;
  @Input() name: string;
  @Input() subtext: string;
  @Input() subtext2: string;
  @Input() initials: string;
  @Input() mode = 'grid';

  constructor() { }

  ngOnInit() { }

  presenceClass(presence: string): string {
    if (PRESENCES.indexOf(presence) !== -1) {
      return presence;
    }
    return DEFAULT_PRESENCE;
  }

  getInitials() {
    let initial;
    if (this.initials) {
      initial = this.stripExtraInitials(this.initials);
    } else {
      initial = this.extractInitials(this.name);
    }
    return initial;
  }
  private stripExtraInitials(initial: string) {
    if (initial && initial.trim().length > 2) {
      return initial.trim().substring(0, 2);
    }
    return initial;
  }

  private extractInitials(name: string) {
    let nameList, firstName, lastName, initial;
    if (name && !this.initials) {
      nameList = name.split(' ');
      firstName = nameList[0].substring(0, 1);
      lastName = nameList[1].substring(0, 1);
      initial = firstName + lastName;
    }
    return initial;
  }
}
