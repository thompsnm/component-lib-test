import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PGiContactBubble } from './contact-bubble.component';

@NgModule({
  declarations: [
    PGiContactBubble
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PGiContactBubble
  ]
})
export class PGiContactBubbleModule { }
