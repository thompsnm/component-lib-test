import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Component } from '@angular/core';

import { PGiContactBubble } from './contact-bubble.component';
import { PGiContactBubblePage } from './contact-bubble.component.page';

@Component({
  template: `<pgi-contact-bubble ></pgi-contact-bubble>`
})
class TestWrapper { }

describe('PGiContactBubble', () => {

  let fixture: ComponentFixture<TestWrapper>,
    contactBubble: PGiContactBubblePage,
    component: PGiContactBubble;

  beforeEach(async(() => {
    PGiContactBubblePage.makeTestBed(TestWrapper)
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(TestWrapper);
        fixture.autoDetectChanges();

        let el = fixture.debugElement.query(By.css('pgi-contact-bubble'));
        contactBubble = new PGiContactBubblePage(el);
        component = el.componentInstance;
      });
  }));

  describe('optional inputs', () => {
    it('should not have presence', () => {
      component.presence = undefined;
      component.contactImage = 'foo';
      fixture.detectChanges();
      expect(contactBubble.presence()).toBeNull();
    });

    it('should have presence', () => {
      component.presence = 'online';
      component.contactImage = 'foo';
      fixture.detectChanges();
      expect(contactBubble.presence()).not.toBeNull();
    });

    it('should default to offline', () => {
      component.presence = 'garbage';
      component.contactImage = 'foo';
      fixture.detectChanges();
      expect(contactBubble.offlinePresence()).not.toBeNull();
    });


    it('should not have a subtext placement', () => {
      component.subtext = undefined;
      fixture.detectChanges();
      expect(contactBubble.subText1()).toBe(null);
    });

    it('should not have a subtext2 placement', () => {
      component.subtext2 = undefined;
      fixture.detectChanges();
      expect(contactBubble.subText2()).toBe(null);
    });

    it('should have subtext and not have a subtext2 placement', () => {
      component.subtext = '(303) 555-1212';
      component.subtext2 = undefined;
      fixture.detectChanges();
      expect(contactBubble.subText1()).not.toBe(null);
      expect(contactBubble.subText1Content()).toContain('(303) 555-1212');
      expect(contactBubble.subText2()).toBe(null);
    });

    it('should have subtext2', () => {
      component.subtext2 = '(303) 555-1212';
      fixture.detectChanges();
      expect(contactBubble.subText2()).not.toBe(null);
      expect(contactBubble.subText2Content()).toContain('(303) 555-1212');
    });

    it('should show initials when no image', () => {
      component.initials = "sc";
      fixture.detectChanges();
      expect(contactBubble.image()).toBe(null);
      expect(contactBubble.initial()).not.toBe(null);
    });
    it('should be blank if no initials is passed', () => {
      fixture.detectChanges();
      expect(contactBubble.image()).toBe(null);
      expect(contactBubble.initial()).not.toBe(null);
      expect(contactBubble.initialContent()).toBe('');
    });
    it('should not have presence when no image', () => {
      component.initials = "sc";
      fixture.detectChanges();
      expect(contactBubble.presence()).toBeNull();
    });
    it('should convert initials to uppercase', () => {
      component.initials = "sr";
      fixture.detectChanges();
      expect(contactBubble.image()).toBe(null);
      expect(contactBubble.initial()).not.toBe(null);
      expect(contactBubble.initialContent()).toBe('SR');
    });
    it('should strip initials if initials length is greater than two', () => {
      component.initials = "src";
      fixture.detectChanges();
      expect(contactBubble.image()).toBe(null);
      expect(contactBubble.initial()).not.toBe(null);
      expect(contactBubble.initialContent()).toBe('SR');
    });
    it('should extract initials if no initial is specified', () => {
      component.name = "sudheer chilumula";
      fixture.detectChanges();
      expect(contactBubble.image()).toBe(null);
      expect(contactBubble.initialContent()).toBe('SC');
    });
  });
});
