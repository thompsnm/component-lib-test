import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import { Component } from '@angular/core';

import { PGiListItem } from './list-item.component';
import { PGiListItemPage } from './list-item.component.page';
import { PGiContactBubbleModule } from '../contact-bubble/contact-bubble.module';

@Component({
  template: `<pgi-list-item></pgi-list-item>`
})
class TestWrapper {};

describe('ListItemComponent', () => {
  let fixture: ComponentFixture<TestWrapper>,
      listItem: PGiListItemPage,
      component: PGiListItem;

  beforeEach(async(() => {
    PGiListItemPage.makeTestBed(TestWrapper)
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(TestWrapper);
        fixture.autoDetectChanges();

        let el = fixture.debugElement.query(By.css('pgi-list-item'));
        listItem = new PGiListItemPage(el);
        component = el.componentInstance;
      });
  }));


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('optional inputs', () => {
    it('should not have presence', () => {
      component.presence = undefined;
      component.contactImage = 'foo';
      fixture.detectChanges();
      expect(listItem.presence()).toBeNull();
    });

    it('should have presence', () => {
      component.presence = 'online';
      component.contactImage = 'foo';
      fixture.detectChanges();
      expect(listItem.presence()).not.toBeNull();
    });

    it('should default to offline', () => {
      component.presence = 'garbage';
      component.contactImage = 'foo';
      fixture.detectChanges();
      expect(listItem.offlinePresence()).not.toBeNull();
    });

    it('should not have a subtext placement', () => {
      component.subheader1 = undefined;
      fixture.detectChanges();
      expect(listItem.subText1()).toBe(null);
    });

    it('should not have a subtext2 placement', () => {
      component.subheader2 = undefined;
      fixture.detectChanges();
      expect(listItem.subText2()).toBe(null);
    });

    it('should have subtext and not have a subtext2 placement', () => {
      component.subheader1 = '(303) 555-1212';
      component.subheader2 = undefined;
      fixture.detectChanges();
      expect(listItem.subText1()).not.toBe(null);
      expect(listItem.subText1Content()).toContain('(303) 555-1212');
      expect(listItem.subText2()).toBe(null);
    });

    it('should have subtext2', () => {
      component.subheader2 = '(303) 555-1212';
      fixture.detectChanges();
      expect(listItem.subText2()).not.toBe(null);
      expect(listItem.subText2Content()).toContain('(303) 555-1212');
    });
  });
});
