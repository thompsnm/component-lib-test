import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PGiListItem } from './list-item.component';
import { PGiInformationCell } from './information-cell/information-cell.component';
import { PGiContactBubble } from '../contact-bubble/contact-bubble.component';
import { PGiContactBubbleModule } from '../contact-bubble/contact-bubble.module';

@NgModule({
  declarations: [
    PGiListItem,
    PGiInformationCell
  ],
  imports: [
    CommonModule,
    PGiContactBubbleModule
  ],
  exports: [
    PGiListItem,
    PGiInformationCell
  ]
})
export class PGiListItemModule { }
