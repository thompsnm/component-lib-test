import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'pgi-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PGiListItem implements OnInit {

  @Input() contactImage: string;
  @Input() presence: string;
  @Input() header: string;
  @Input() subheader1: string;
  @Input() subheader2: string;
  @Input() mode = 'list';

  constructor() { }

  ngOnInit() {
  }

}
