import { DebugElement } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { PGiListItemModule } from '../list-item.module';

export class PGiInformationCellPage {
  root: DebugElement;

  static makeTestBed(container) {
    return TestBed.configureTestingModule({
      imports: [
        PGiListItemModule
      ],
      declarations: [
        container
      ]
    });
  }

  constructor(debugElement: DebugElement ) {
    if (!debugElement || !debugElement.nativeElement) {
      console.error('PGiInformationCell requires a DebugElement');
    } else {
      this.root = debugElement;
    }
  }

  get title(): String {
    return this.root.nativeElement.querySelector('.title').textContent.trim();
  }

  get status(): String {
    return this.root.nativeElement.querySelector('.status').textContent.trim();
  }
}
