import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { PGiInformationCellPage } from './information-cell.component.page';
import { PGiInformationCell } from './information-cell.component';

@Component({
  template: `<pgi-information-cell><pgi-information-cell>`
})
class TestWrapper{ }

describe('Simple List Item', () => {
  let fixture: ComponentFixture<TestWrapper>;
  let component: PGiInformationCell;
  let page: PGiInformationCellPage;

  beforeEach(async(() => {
    PGiInformationCellPage.makeTestBed(TestWrapper)
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestWrapper);

    let el = fixture.debugElement.query(By.css('pgi-information-cell'));
    page = new PGiInformationCellPage(el);
    component = el.componentInstance;
  });

  it('should exist', () => {
    expect(component).toBeTruthy();
  });

  describe('when created with a title and status', () => {
    beforeEach(() => {
      component.title = 'Title';
      component.status = 'status';
      fixture.detectChanges();
    });

    it('should display the correct title', () => {
      expect(page.title).toBe('Title');
    });

    it('should display the correct status', () => {
      expect(page.status).toBe('status');
    })
  });
});
