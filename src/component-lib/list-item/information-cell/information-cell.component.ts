import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'pgi-information-cell',
  templateUrl: './information-cell.component.html',
  styleUrls: ['./information-cell.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PGiInformationCell {
  @Input() title: String;
  @Input() status: String;

  constructor() { }
}
