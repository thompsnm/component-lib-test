import { TestBed } from '@angular/core/testing';
import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { PGiContactBubbleModule } from '../contact-bubble/contact-bubble.module';
import { PGiListItemModule } from './list-item.module';
import {By} from '@angular/platform-browser';

export class PGiListItemPage {
  root = null;
  selectors = {
    header: '[name=header]',
    subtext1: '[name=subtext1]',
    subtext2: '[name=subtext2]',
    presence: '[name=image-container].presence',
    offline: '[name=image-container].presence.offline'
  };

  static makeTestBed(container) {
    return TestBed
      .configureTestingModule( {
        imports: [ PGiListItemModule, PGiContactBubbleModule ],
        declarations: [ container ]
      });
  }

  constructor(debugElement) {
     if ( !debugElement ||
      !debugElement.nativeElement ) {
      console.error('You need to pass a DebugElement to PGiListItemPage constructor.');
    } else {
      this.root = debugElement;
    }
  }

  presence() {
    return this.root.query(By.css(this.selectors.offline));
  }

  offlinePresence() {
    return this.root.query(By.css(this.selectors.offline));
  }

  header() {
    return this.root.query(By.css(this.selectors.header));
  }
  subText1() {
    return this.root.query(By.css(this.selectors.subtext1));
  }

  subText1Content() {
    return this.root.nativeElement.querySelector(this.selectors.subtext1).textContent;
  }

  subText2() {
    return this.root.query(By.css(this.selectors.subtext2));
  }

  subText2Content() {
    return this.root.nativeElement.querySelector(this.selectors.subtext2).textContent;
  }
}
