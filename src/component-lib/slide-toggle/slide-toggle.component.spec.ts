// These test cases are the replica of the slide toggle test cases in Angular Material lib.
// we have copied them here to validate basic slide toggle behaviour with our changes included
// as there are no change in behaviour of toggle so these test cases are sufficient.
import { PGiSlideToggleModule } from './slide-toggle.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { By } from '@angular/platform-browser';

import {MdSlideToggleChange} from '@angular/material';

describe('pgi-slide-toggle', () => {
  let testComponent: SlideToggleTestApp;
  let fixture: ComponentFixture<any>;
  let toggleDebugElement: any;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PGiSlideToggleModule],
      declarations: [SlideToggleTestApp],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    });

    TestBed.compileComponents();
    fixture = TestBed.createComponent(SlideToggleTestApp);
    toggleDebugElement = fixture.debugElement.query(By.css('md-slide-toggle'));
    testComponent = fixture.debugElement.componentInstance;
  }));

  // General toggle tests
  it('should apply class based on color attribute', () => {
    testComponent.color = 'primary';
    fixture.detectChanges();
    expect(toggleDebugElement.nativeElement.classList.contains('mat-primary')).toBe(true);

    testComponent.color = 'accent';
    fixture.detectChanges();
    expect(toggleDebugElement.nativeElement.classList.contains('mat-accent')).toBe(true);

    testComponent.color = 'warn';
    fixture.detectChanges();
    expect(toggleDebugElement.nativeElement.classList.contains('mat-warn')).toBe(true);
  });

  it('should should not clear previous defined classes', () => {
    toggleDebugElement.nativeElement.classList.add('custom-class');

    testComponent.color = 'primary';
    fixture.detectChanges();

    expect(toggleDebugElement.nativeElement.classList.contains('mat-primary')).toBe(true);
    expect(toggleDebugElement.nativeElement.classList.contains('custom-class')).toBe(true);

    testComponent.color = 'accent';
    fixture.detectChanges();

    expect(toggleDebugElement.nativeElement.classList.contains('mat-primary')).toBe(false);
    expect(toggleDebugElement.nativeElement.classList.contains('mat-accent')).toBe(true);
    expect(toggleDebugElement.nativeElement.classList.contains('custom-class')).toBe(true);

  });

  describe('basic behavior', () => {
    let slideToggle: any;
    let inputElement: HTMLInputElement;
    let labelElement: HTMLLabelElement;
    let slideToggleElement: HTMLElement;

    beforeEach(async(() => {
      fixture = TestBed.createComponent(SlideToggleTestApp);
      testComponent = fixture.debugElement.componentInstance;

      spyOn(fixture.debugElement.componentInstance, 'onSlideChange').and.callThrough();
      spyOn(fixture.debugElement.componentInstance, 'onSlideClick').and.callThrough();

      fixture.detectChanges();

      let slideToggleDebug = fixture.debugElement.query(By.css('md-slide-toggle'));
      slideToggle = slideToggleDebug.componentInstance;
      slideToggleElement = slideToggleDebug.nativeElement;

      inputElement = fixture.debugElement.query(By.css('input')).nativeElement;
      labelElement = fixture.debugElement.query(By.css('label')).nativeElement;
    }));

    it('should correctly update the disabled property', () => {
      expect(inputElement.disabled).toBeFalsy();

      testComponent.isDisabled = true;
      fixture.detectChanges();

      expect(inputElement.disabled).toBeTruthy();
    });

    it('should correctly update the checked property', () => {
      expect(slideToggle.checked).toBeFalsy();

      testComponent.slideChecked = true;
      fixture.detectChanges();

      expect(inputElement.checked).toBeTruthy();
    });

    it('should not trigger the change event by changing the native value', async(() => {
      testComponent.slideChecked = false;
      expect(inputElement.checked).toBe(false);
      expect(slideToggleElement.classList).not.toContain('mat-checked');

      testComponent.slideChecked = true;
      fixture.detectChanges();

      expect(inputElement.checked).toBe(true);
      expect(slideToggleElement.classList).toContain('mat-checked');
    }));

    it('should set a element class if labelPosition is set to before', () => {
      expect(slideToggleElement.classList).not.toContain('mat-slide-toggle-label-before');

      testComponent.labelPosition = 'before';
      fixture.detectChanges();

      expect(slideToggleElement.classList).toContain('mat-slide-toggle-label-before');
    });

    it('should trigger the change event properly', () => {
      expect(inputElement.checked).toBe(false);
      expect(slideToggleElement.classList).not.toContain('mat-checked');

      labelElement.click();
      fixture.detectChanges();

      expect(inputElement.checked).toBe(true);
      expect(slideToggleElement.classList).toContain('mat-checked');
      expect(testComponent.onSlideChange).toHaveBeenCalledTimes(1);
    });
  });
});
/** Test component. */
@Component({
  selector: 'test-slide-toggle-app',
  template: `
    <pgi-slide-toggle 
      [disabled]="isDisabled"
      [color]="color"
      [checked]="slideChecked"
      [labelPosition]="labelPosition"
      (change)="onSlideChange($event)"
      (click)="onSlideClick($event)">
      Test Slide Toggle
    </pgi-slide-toggle>
  `
})

class SlideToggleTestApp {
  isDisabled: Boolean = false;
  slideChecked: Boolean = false;
  color: String;
  labelPosition: String;
  lastEvent: MdSlideToggleChange;

  onSlideClick(event: Event) {}
  onSlideChange(event: MdSlideToggleChange) {
    this.lastEvent = event;
  }
};
