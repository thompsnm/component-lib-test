import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MdSlideToggleModule } from '@angular/material';

import { PGiSlideToggleComponent } from './slide-toggle.component';

@NgModule({
  declarations: [
    PGiSlideToggleComponent
  ],
  imports: [
    CommonModule,
    MdSlideToggleModule
  ],
  exports: [
    PGiSlideToggleComponent
  ]
})
export class PGiSlideToggleModule { }
