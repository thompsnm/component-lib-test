import {Component, Input, ViewEncapsulation, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'pgi-slide-toggle',
  templateUrl: './slide-toggle.component.html',
  styleUrls: ['./slide-toggle.component.scss', './slide-toggle.theme.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PGiSlideToggleComponent {
  /* Input properties*/
  @Input() id: String;
  @Input() color: String = 'primary';
  @Input() disabled: Boolean;
  @Input() checked: Boolean;
  @Input() labelPosition: String = 'before';
  @Input() ariaLabel: String;
  @Input() ariaLabelledby: String;

  /* Output properties*/
  @Output() change: EventEmitter<Boolean> = new EventEmitter<Boolean>();

  /** This is a conustructor method */
  constructor() { }

  /** This method will be called on slide toggle change event and
   * will emit the change property value. */
  slideChange($event: any) {
    this.checked = $event.checked;
    this.change.emit($event.checked);
  }
}
