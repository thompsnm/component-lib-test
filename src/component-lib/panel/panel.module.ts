import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PGiPanelComponent } from './panel.component';
import { PGiPanelContentBaseComponent } from './panel-content-base/panel-content-base.component';


@NgModule({
  imports: [
    CommonModule
  ],

  declarations: [
    PGiPanelComponent,
    PGiPanelContentBaseComponent
  ],

  exports: [
    PGiPanelComponent,
    PGiPanelContentBaseComponent
  ]
})
export class PGiPanelModule { }
