import { Component, ViewEncapsulation } from '@angular/core';

/**
 * Component class for panel content base component.
 *
 * @export
 * @class PGiPanelContentBaseComponent
 */
@Component({
  selector: 'pgi-panel-content-base',
  template: '<ng-content></ng-content>',
  styleUrls: ['./panel-content-base.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class PGiPanelContentBaseComponent {
  constructor() { }
}
