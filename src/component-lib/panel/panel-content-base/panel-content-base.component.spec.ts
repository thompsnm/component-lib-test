import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { PGiPanelContentBaseComponent } from './panel-content-base.component';


describe('PanelContentBase component', () => {
  let fixture: ComponentFixture<PGiPanelContentBaseComponent>;
  let component: PGiPanelContentBaseComponent;
  let fixtureInstance;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PGiPanelContentBaseComponent]
    })
      .compileComponents().then(() => {
        fixture = TestBed.createComponent(PGiPanelContentBaseComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
