import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'pgi-panel',
  template: '<ng-content></ng-content>',
  styleUrls: ['./panel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PGiPanelComponent {
}
