import { Component, Input, ViewEncapsulation, ViewChild } from '@angular/core';
import { MdMenuTrigger, MdMenu } from '@angular/material';
import { PGiMenuTrigger } from './menu-trigger/menu-trigger.component';

type MenuSizes = 'small' | 'medium' | 'large' | 'x-large' | 'xx-large';

@Component({
  selector: 'pgi-menu',
  templateUrl: './menu-container.component.html',
  styleUrls: ['./menu-container.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class PGiMenuContainer {
  /** Pgi Menu reference */
  @ViewChild('menu') menu: MdMenu;

  /** Pgi Menu trigger reference */
  @ViewChild('menuTrigger') menuTrigger: MdMenuTrigger;

  /** Size of the menu container - values could be small, medium, large, x-large, xx-large  */
  @Input() size: MenuSizes = 'large';

  /** Position of the menu in the X axis. */
  @Input() xPosition: 'before' | 'after' = 'after';

  /** Position of the menu in the Y axis. */
  @Input() yPosition: 'above' | 'below' = 'above';

  /** Whether the menu should overlap its trigger.**/
  @Input() overlapTrigger = true;

  /** This method takes classes set on the host md-menu element and applies them on the menu template
   *  that displays in the overlay container. Otherwise, it's difficult to style the containing menu
   *  from outside the component.*/
  @Input() classList = '';

  /** Focus the first item in the menu. This method is used by the menu trigger to focus
   *  the first item when the menu is opened by the ENTER key. */
  public focusFirstItem() {
    if (this.menu) {
      this.menu.focusFirstItem();
    }
  }

  /**
  * It's necessary to set position-based classes to ensure the menu panel animation
  * folds out from the correct direction.
  */
  public setPositionClasses(posX?: "before" | "after", posY?: "above" | "below") {
    if (this.menu) {
      this.menu.setPositionClasses(posX, posY);
    }
  }

  /** Toggles the menu between the open and closed states. */
  public toggleMenu() {
    if (this.menuTrigger) {
      this.menuTrigger.toggleMenu();
    }
  }

  /** Opens the menu. */
  public openMenu() {
    if (this.menuTrigger) {
      this.menuTrigger.openMenu();
    }
  }

  /** Closes the menu. */
  public closeMenu() {
    if (this.menuTrigger) {
      this.menuTrigger.closeMenu();
    }
  }

  /** Removes the menu from the DOM. */
  public destroyMenu() {
    if (this.menuTrigger) {
      this.menuTrigger.destroyMenu();
    }
  }

  /** Focuses the menu trigger. */
  public focusMenuTrigger() {
    if (this.menuTrigger) {
      this.menuTrigger.focus();
    }
  }
}
