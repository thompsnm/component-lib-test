import { By } from '@angular/platform-browser';
import { Component } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { PGiMenuModule } from '../menu.module';

export class PGiLabelMenuItemPage {
  root = null;

  selectors = {
    labelElement: '.labelText',
    descriptionElement: '.desc',
    dividerElement: '.divider'
  };

  static makeTestBed(container) {
    return TestBed
      .configureTestingModule({
        imports: [PGiMenuModule],
        declarations: [container]
      });
  }

  constructor(debugElement) {
    if (!debugElement ||
      !debugElement.nativeElement ||
      debugElement.nativeElement.tagName.toLowerCase() !== 'pgi-label-menu-item') {
      console.error('You need to pass a <pgi-label-menu-item> element to PGiLabelMenuItemPage constructor.');
    } else {
      this.root = debugElement;
    }
  }

  labelElement() {
    return this.root.query(By.css(this.selectors.labelElement));
  }

  labelElementAttribute(attr: string) {
    return this.labelElement().nativeElement.getAttribute(attr);
  }

  descriptionElement() {
    return this.root.query(By.css(this.selectors.descriptionElement));
  }

  dividerElement() {
    return this.root.query(By.css(this.selectors.dividerElement));
  }

  labelContent() {
    return this.root.nativeElement.querySelector(this.selectors.labelElement).textContent;
  }

  descriptionContent() {
    return this.root.nativeElement.querySelector(this.selectors.descriptionElement).textContent;
  }
}

