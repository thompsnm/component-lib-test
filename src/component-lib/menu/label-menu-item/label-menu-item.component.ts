import { Component, Input, Output, ViewEncapsulation, EventEmitter, ViewChild } from '@angular/core';
import { MdMenuItem } from '@angular/material';

@Component({
  selector: 'pgi-label-menu-item',
  templateUrl: './label-menu-item.component.html',
  styleUrls: ['./label-menu-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class PGiLabelMenuItem {

  /** Pgi Menu item reference */
  @ViewChild('menuItem') menuItem: MdMenuItem;

  /* Menu click item event*/
  @Output() clickItemEvent = new EventEmitter<any>();

  /** Text for the menu item. */
  @Input() labelText: string;

  /** If you need a description for the menu item, then add it here. */
  @Input() description: string;

  /** Set type=destructive if you want a destructive menu item i.e label color is red. */
  @Input() type: 'destructive' | '';

  /** Set hr=true if you want a horizontal rule added after the menu item. Default is set to false. */
  @Input() hr = false;

  /** Whether the menu item is disabled. Default is set to false. */
  @Input() disabled = false;

  /** Focuses the menu item */
  public focusMenuItem() {
    if (this.menuItem) {
      this.menuItem.focus();
    }
  }

  /** Event emitted when menu item is clicked */
  menuItemClick(event: any) {
    let isDisabled;
    if (event) {
      isDisabled = (<Element>event.currentTarget).getAttribute('disabled');

      if (isDisabled === null || isDisabled === 'false') {
        this.clickItemEvent.emit(event);
      }
    }
  }

  /** Called on mouse enter of labelItem and description items */
  showTooltip(element: any) {
    if (element && element.currentTarget &&
      this.isEllipsisActive(element.currentTarget)) {
      if (element.currentTarget.classList.contains('labelText')) {
        element.currentTarget.setAttribute('title', this.labelText);
      } else {
        element.currentTarget.setAttribute('title', this.description);
      }
    }
  }

  /** Checks if the element has ellipsis */
  isEllipsisActive(e) {
    return (e.offsetWidth < e.scrollWidth);
  }
}
