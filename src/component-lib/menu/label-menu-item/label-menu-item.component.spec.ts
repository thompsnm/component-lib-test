import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Component } from '@angular/core';

import { PGiLabelMenuItem } from './label-menu-item.component';
import { PGiLabelMenuItemPage } from './label-menu-item.component.page';

@Component({
  template: `<pgi-label-menu-item 
    labelText="Item 1" 
    description="Item Detail" 
    type="destructive" 
    hr="true"
    ></pgi-label-menu-item>`
})
class TestWrapper { }

describe('PGiLabelMenuItem', () => {

  let fixture: ComponentFixture<TestWrapper>,
    labelMenuItem: PGiLabelMenuItemPage,
    component: PGiLabelMenuItem;

  beforeEach(async(() => {
    PGiLabelMenuItemPage.makeTestBed(TestWrapper)
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(TestWrapper);
        fixture.autoDetectChanges();

        let el = fixture.debugElement.query(By.css('pgi-label-menu-item'));
        labelMenuItem = new PGiLabelMenuItemPage(el);
        component = el.componentInstance;
      });
  }));

  describe('check label item', () => {
    it('should have label element', () => {
      component.labelText = 'Item 1';
      fixture.detectChanges();
      expect(labelMenuItem.labelElement()).not.toBe(null);
    });
    it('should have the label text', () => {
      component.labelText = 'Item 1';
      fixture.detectChanges();
      expect(labelMenuItem.labelContent()).toBe(component.labelText);
    });
    it('should not have the label text', () => {
      component.labelText = '';
      fixture.detectChanges();
      expect(labelMenuItem.labelElement()).not.toBe(null);
    });
    it('should have destructive label type', () => {
      component.type = 'destructive';
      fixture.detectChanges();
      expect(labelMenuItem.labelElementAttribute('class')).toContain('destructive');
    });
    it('should not have destructive label type', () => {
      component.type = '';
      fixture.detectChanges();
      expect(labelMenuItem.labelElementAttribute('class')).not.toContain('destructive');
    });
  });

  describe('check description', () => {
    it('should have description element', () => {
      component.description = 'Item Detail';
      fixture.detectChanges();
      expect(labelMenuItem.descriptionElement()).not.toBe(null);
    });
    it('should have the description text', () => {
      component.description = 'Item Detail';
      fixture.detectChanges();
      expect(labelMenuItem.descriptionContent()).toBe(component.description);
    });
    it('it should not have description element and text', () => {
      component.description = '';
      fixture.detectChanges();
      expect(labelMenuItem.descriptionElement()).toBeNull();
    });
  });

  describe('check if horizontal rule is present', () => {
    it('should have horizontal element', () => {
      component.hr = true;
      fixture.detectChanges();
      expect(labelMenuItem.dividerElement()).not.toBe(null);
    });
    it('should not have horizontal element', () => {
      component.hr = false;
      fixture.detectChanges();
      expect(labelMenuItem.dividerElement()).toBeNull();
    });
  });
});
