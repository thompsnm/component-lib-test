import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Component, ViewChild, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MenuPositionX, MenuPositionY, OverlayContainer, Dir, LayoutDirection } from '@angular/material';

import { PGiMenuModule } from './menu.module';
import { PGiMenuContainer } from './menu-container.component';

describe('pgi-menu', () => {
  let overlayContainerElement: HTMLElement;
  let dir: LayoutDirection;

  beforeEach(async(() => {
    dir = 'ltr';
    TestBed.configureTestingModule({
      imports: [PGiMenuModule, NoopAnimationsModule],
      declarations: [SimpleMenu, PositionedMenu],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: OverlayContainer, useFactory: () => {
            overlayContainerElement = document.createElement('div');
            overlayContainerElement.classList.add('cdk-overlay-container');
            document.body.appendChild(overlayContainerElement);

            document.body.style.padding = '0';
            document.body.style.margin = '0';
            return { getContainerElement: () => overlayContainerElement };
          }
        },
        {
          provide: Dir, useFactory: () => {
            return { value: dir };
          }
        }
      ]
    });

    TestBed.compileComponents();
  }));

  afterEach(() => {
    document.body.removeChild(overlayContainerElement);
  });

  it('should open the menu as an idempotent operation', () => {
    const fixture = TestBed.createComponent(SimpleMenu);
    fixture.detectChanges();
    expect(overlayContainerElement.textContent).toBe('');
    expect(() => {
      fixture.componentInstance.menu.openMenu();
      expect(overlayContainerElement.textContent).toContain('Item 1');
    }).not.toThrowError();
  });

  it('should close the menu when a click occurs outside the menu', (done) => {
    const fixture = TestBed.createComponent(SimpleMenu);
    fixture.detectChanges();
    fixture.componentInstance.menu.openMenu();

    const backdrop = <HTMLElement>overlayContainerElement.querySelector('.cdk-overlay-backdrop');
    backdrop.click();
    fixture.detectChanges();
    setTimeout(function () {
      done();
      expect(overlayContainerElement.textContent).toBe('');
    }, 2000);
  });

  it('should set the panel direction based on the trigger direction', () => {
    dir = 'rtl';
    const fixture = TestBed.createComponent(SimpleMenu);
    fixture.detectChanges();
    fixture.componentInstance.menu.openMenu();
    fixture.detectChanges();

    const overlayPane = overlayContainerElement.querySelector('.cdk-overlay-pane');
    expect(overlayPane.getAttribute('dir')).toEqual('rtl');
  });

  describe('positions', () => {
    let fixture: ComponentFixture<PositionedMenu>;
    let panel: HTMLElement;

    beforeEach(() => {
      fixture = TestBed.createComponent(PositionedMenu);
      fixture.detectChanges();
      fixture.componentInstance.menu.openMenu();
      fixture.detectChanges();
      panel = overlayContainerElement.querySelector('.mat-menu-panel') as HTMLElement;
    });

    it('should append mat-menu-before if the x position is changed', () => {
      expect(panel.classList).toContain('mat-menu-before');
      expect(panel.classList).not.toContain('mat-menu-after');

      fixture.componentInstance.xPosition = 'after';
      fixture.detectChanges();

      expect(panel.classList).toContain('mat-menu-after');
      expect(panel.classList).not.toContain('mat-menu-before');
    });

    it('should set the value to size attribute', () => {
      fixture.componentInstance.menu.size = 'small';
      fixture.detectChanges();

      fixture.componentInstance.menu.size = 'large';
      fixture.detectChanges();

      expect(panel.classList).toContain('large');
      expect(panel.classList).not.toContain('small');
    });

    it('should append mat-menu-above if the y position is changed', () => {
      fixture.componentInstance.yPosition = 'below';
      fixture.detectChanges();

      expect(panel.classList).toContain('mat-menu-below');
      expect(panel.classList).not.toContain('mat-menu-above');
    });

    it('should default to the "below" and "after" positions', () => {
      fixture.destroy();

      let newFixture = TestBed.createComponent(SimpleMenu);

      newFixture.detectChanges();
      newFixture.componentInstance.menu.openMenu();
      newFixture.detectChanges();
      panel = overlayContainerElement.querySelector('.mat-menu-panel') as HTMLElement;

      expect(panel.classList).toContain('mat-menu-below');
      expect(panel.classList).toContain('mat-menu-after');
    });
  });
  describe('animations', () => {
    it('should remove the ripple on disabled items', () => {
      const fixture = TestBed.createComponent(SimpleMenu);
      fixture.detectChanges();

      fixture.componentInstance.menu.openMenu();
      const items = fixture.debugElement.queryAll(By.css('.mat-menu-item'));

      const ripple = items[1].query(By.css('.mat-ripple'));
      expect(ripple).toBeNull();
    });
  });

  describe('destroy', () => {
    it('does not throw an error on destroy', () => {
      const fixture = TestBed.createComponent(SimpleMenu);
      expect(fixture.destroy.bind(fixture)).not.toThrow();
    });
  });

});

@Component({
  template: `
    <pgi-menu #menu>
    <pgi-menu-trigger><button>Toggle menu</button></pgi-menu-trigger>
      <pgi-label-menu-item labelText="Item 1"></pgi-label-menu-item>
      <pgi-label-menu-item disabled labelText="Disabled"> </pgi-label-menu-item>
    </pgi-menu>
  `
})
class SimpleMenu {
  @ViewChild('menu') menu: PGiMenuContainer;
}

@Component({
  template: `
    <pgi-menu #menu [xPosition]="xPosition" [yPosition]="yPosition">
    <pgi-menu-trigger #menuTrigger><button>Toggle menu</button></pgi-menu-trigger>
      <pgi-label-menu-item labelText="Item 1"></pgi-label-menu-item>
    </pgi-menu>
  `
})
class PositionedMenu {
  @ViewChild('menu') menu: PGiMenuContainer;
  xPosition: MenuPositionX = 'before';
  yPosition: MenuPositionY = 'above';
}

