import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { MdMenuModule } from '@angular/material';
import { NgModule } from '@angular/core';

import { PGiLabelMenuItem } from './label-menu-item/label-menu-item.component';
import { PGiMenuContainer } from './menu-container.component';
import { PGiMenuTrigger } from './menu-trigger/menu-trigger.component';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    MdMenuModule,
  ],
  declarations: [
    PGiLabelMenuItem,
    PGiMenuContainer,
    PGiMenuTrigger
  ],
  exports: [
    PGiLabelMenuItem,
    PGiMenuContainer,
    PGiMenuTrigger
  ]
})

export class PGiMenuModule { }
