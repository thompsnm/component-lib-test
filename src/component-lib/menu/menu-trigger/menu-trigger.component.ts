import { Component, ViewEncapsulation } from '@angular/core';
@Component({
  selector: 'pgi-menu-trigger',
  template: `<ng-content></ng-content>`,
  encapsulation: ViewEncapsulation.None
})

export class PGiMenuTrigger {
}
