import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MdRadioModule } from '@angular/material';

import { PGiIconsModule } from '../icons';
import { PGiButtonsModule } from '../buttons';
import { PGiTooltipModule } from '../tooltip';

import { PGiRadioButtonComponent } from './radio-button/radio-button.component';
import { PGiIconInput } from './icon-input/icon-input.component';
import { PGiLabelInput } from './label-input/label-input.component';
import { PGiSettingsInput } from './settings-input/settings-input.component';
import { PGiAutocomplete } from './autocomplete/autocomplete.component';
import { PGiAutocompleteResult } from './autocomplete/autocomplete-result.component';
import { PGiFocusEvents } from './focus-events.directive';

@NgModule({
  imports: [
    PGiIconsModule,
    FormsModule,
    PGiButtonsModule,
    CommonModule,
    PGiTooltipModule,
    MdRadioModule
  ],
  declarations: [
    PGiFocusEvents,
    PGiIconInput,
    PGiLabelInput,
    PGiSettingsInput,
    PGiAutocomplete,
    PGiAutocompleteResult,
    PGiRadioButtonComponent
  ],
  exports: [
    PGiFocusEvents,
    PGiIconInput,
    PGiLabelInput,
    PGiSettingsInput,
    PGiAutocomplete,
    PGiAutocompleteResult,
    PGiRadioButtonComponent
  ]
})
export class PGiInputsModule { }
