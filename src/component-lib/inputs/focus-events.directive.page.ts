import { By } from '@angular/platform-browser';
import { TestBed } from '@angular/core/testing';
import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { PGiInputsModule } from './inputs.module';
import { PGiIconsModule } from '../icons';
import { PGiButtonsModule } from '../buttons';

@Component({
  selector: 'pgi-icon-button',
  template: '<button>x</button>'
})
class PGiIconButtonFake {}

@Component({
  selector: 'pgi-icon',
  template: '<i>icon</i>'
})
class PGiIconFake {}

export class PGiFocusEventsPage {
  root = null;

  static makeTestBed( container ) {
    return TestBed
      .configureTestingModule( {
        imports: [ PGiInputsModule, FormsModule ],
        declarations: [ container ]
      })
      // Remove sub-dependencies from the module under test.
      .overrideModule(PGiInputsModule, {
        remove: {
          imports: [ PGiIconsModule, PGiButtonsModule ]
        },
        add: {
          schemas: [ NO_ERRORS_SCHEMA ],
          declarations: [PGiIconFake, PGiIconButtonFake]
        }
      });
  }

  constructor(debugElement) {
    if ( !debugElement ||
      !debugElement.nativeElement ) {
      console.error('You need to pass a DebugElement to PGiFocusEventsPage constructor.');
    } else {
      this.root = debugElement;
    }
  }

  makeClickEvent() {
    let event = document.createEvent('MouseEvent');
    event.initEvent('click', true, true);
    return event;
  }

  typeTabKey() {
    let event = document.createEvent('CustomEvent');
    event.initEvent('keyup', true, true);
    (<any>event).key = 'Tab';
    window.dispatchEvent(event);
  }

  focusinElement() {
    this.root.triggerEventHandler('focusin', {target: this.root.nativeElement});
  }

  focusoutElement() {
    this.root.triggerEventHandler('focusout', {target: this.root.nativeElement});
  }

  clickInsideElement() {
    this.focusinElement();
    this.root.nativeElement.dispatchEvent( this.makeClickEvent() );
  }

  clickOutsideElement() {
    this.focusoutElement();
    document.dispatchEvent( this.makeClickEvent() );
  }

  tabOutOfElement() {
    this.focusoutElement();
    this.typeTabKey();
  }

  tabIntoElement() {
    this.focusinElement();
    this.typeTabKey();
  }
}

