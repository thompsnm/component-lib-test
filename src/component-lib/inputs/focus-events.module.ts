import { NgModule } from '@angular/core';
import { PGiFocusEvents } from './focus-events.directive';

@NgModule({
  declarations: [
    PGiFocusEvents
  ],
  exports: [
    PGiFocusEvents
  ]
})
export class PGiFocusEventsModule {}
