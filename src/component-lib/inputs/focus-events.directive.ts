import { Directive, EventEmitter, Output, HostListener } from '@angular/core';

@Directive({
  selector: '[pgi-focus-events]'
})
export class PGiFocusEvents {
  private _focused = false;
  private _entered = false;
  @Output() enter = new EventEmitter();
  @Output() leave = new EventEmitter();

  @HostListener('focusin')
  public onComponentFocusin() {
    this._focused = true;
  }

  @HostListener('focusout')
  public onComponentFocusout() {
    this._focused = false;
  }

  @HostListener('document:click', ['$event'])
  public onDocumentClickEvent($event) {
    this._handleEnterHappened($event);
  }

  @HostListener('window:keyup', ['$event'])
  public onDocumentTabKey($event) {
    if ( $event.key === 'Tab' ) {
      this._handleEnterHappened($event);
    }
  }

  private _handleEnterHappened(event) {
    if ( this._focused && !this._entered ) {
      this._entered = true;
      this.enter.emit(event);
    }
    else if ( !this._focused && this._entered ) {
      this._entered = false;
      this.leave.emit(event);
    }
  }
}
