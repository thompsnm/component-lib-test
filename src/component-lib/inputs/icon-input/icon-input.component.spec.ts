import { Component } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { PGiIconInputPage } from './icon-input.component.page';

@Component({
  template: `<pgi-icon-input
    icon="foo"
    placeholder="Hi"
    color="primary"
    name="data-name"
    [(ngModel)]="data.name"
  ></pgi-icon-input>`
})
class TestForm {
  data: {
    name: string;
  };
  constructor() {
    this.data = {
      name: "foo"
    };
  }
}

describe( 'PGiIconInput', () => {
  let fixture, form, iconInput, component, clearListeners;

  beforeEach(async( () => {
    PGiIconInputPage.makeTestBed( TestForm )
      .compileComponents()
      .then( () => {
        fixture = TestBed.createComponent( TestForm );
        fixture.autoDetectChanges();

        form = fixture.componentInstance;
        const el = fixture.debugElement.query( By.css('pgi-icon-input') );

        component = el.componentInstance;
        iconInput = new PGiIconInputPage( el );
      });
  }));

  beforeEach( () => {
    jasmine.clock().install();
  });

  afterEach( () => {
    jasmine.clock().uninstall();
  });

  clearListeners = () => {
    form.onFocus.calls.reset();
    form.onBlur.calls.reset();
    form.onEnter.calls.reset();
    form.onLeave.calls.reset();
  };

  it('should display the initial model value.', () => {
    expect(iconInput.inputValue()).toEqual( form.data.name );
  });

  it('should show the clear button.', () => {
    expect(iconInput.clearButtonIsVisible()).toBe(true);
  });

  describe('after the user inputs data', () => {
    const newValue = 'bar';
    beforeEach( () => {
      iconInput.userSetValue(newValue);
    });

    it('should update the model passed in when the user types input.', () => {
      expect(form.data.name).toEqual(newValue);
    });

    describe('after clicking the clear input button', () => {
      beforeEach( () => {
        iconInput.clickClearButton();
        fixture.detectChanges();
      });

      it('should clear the model value.', () => {
        expect(form.data.name).toEqual('');
      });

      it('should hide the clear button.', () => {
        expect(iconInput.clearButtonIsVisible()).toBe(false);
      });
    });
  });
});
