import { By } from '@angular/platform-browser';
import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { PGiIconsModule } from '../../icons';
import { PGiButtonsModule } from '../../buttons';
import { PGiInputsModule } from '../inputs.module';

@Component({
  selector: 'pgi-icon-button',
  template: '<button>x</button>'
})
class PGiIconButtonFake {}

@Component({
  selector: 'pgi-icon',
  template: '<i>icon</i>'
})
class PGiIconFake {}

export class PGiIconInputPage {
  root = null;

  selectors = {
    inputElement: 'input',
    clearInputButton: 'pgi-icon-button'
  };

  static makeTestBed( container ) {
    return TestBed
      // Our wrapping module should import the module under test.
      .configureTestingModule( {
        imports: [ PGiInputsModule, FormsModule ],
        declarations: [ container ]
      })
      // Remove sub-dependencies from the module under test.
      .overrideModule(PGiInputsModule, {
        remove: {
          imports: [ PGiIconsModule, PGiButtonsModule ]
        },
        add: {
          schemas: [ NO_ERRORS_SCHEMA ],
          declarations: [PGiIconFake, PGiIconButtonFake]
        }
      });
  }

  constructor(debugElement) {
    if ( !debugElement ||
      !debugElement.nativeElement ||
      debugElement.nativeElement.tagName.toLowerCase() !== 'pgi-icon-input' ) {
      console.error('You need to pass a <pgi-icon-input> element to PGiIconInputPage constructor.');
    } else {
      this.root = debugElement;
    }
  }

  inputElement() {
    return this.root.query( By.css( this.selectors.inputElement ) );
  }

  inputValue() {
    return this.inputElement().nativeElement.value;
  }

  userSetValue(value) {
    const debugEl = this.inputElement();
    const el = debugEl.nativeElement;
    el.value = value;
    el.dispatchEvent(new Event('input'));
  }

  clearButtonElement() {
    return this.root.query( By.css( this.selectors.clearInputButton ) );
  }

  clickClearButton() {
    let button = this.clearButtonElement();
    button.triggerEventHandler('click', {target: button.nativeElement});
  }

  clearButtonIsVisible() {
    const button = this.clearButtonElement();
    return button !== null && button !== undefined;
  }

  focusInputElement() {
    let input = this.inputElement();
    input.triggerEventHandler('focus', {target: input.nativeElement});
  }

  blurInputElement() {
    let input = this.inputElement();
    input.triggerEventHandler('blur', {target: input.nativeElement});
  }

  focusClearButtonElement() {
    let button = this.clearButtonElement();
    button.triggerEventHandler('focus', {target: button.nativeElement});
  }

  blurClearButtonElement() {
    let button = this.clearButtonElement();
    button.triggerEventHandler('blur', {target: button.nativeElement});
  }

  // Simulate the user tabbing from the input element to the clear button
  // element. This will blur the input and focus the clear button but
  // will not emit the enter or leave events.
  //
  // Requires jasmine.clock() to be installed.
  tabFromInputToClearButton() {
    this.blurInputElement();
    this.focusClearButtonElement();
    jasmine.clock().tick(1);
  }

  // Simulate tabbing out of the component when the focus is currently on
  // the clear button. This will emit blur and leave events from the clear
  // button.
  //
  // Requires jasmine.clock() to be installed.
  tabOutFromClearButton() {
    this.blurClearButtonElement();
    jasmine.clock().tick(1);
  }

  // Simulate tabbing out of the component when the focus is currently on
  // the input element. This will emit blur and leave events from the input.
  //
  // Requires jasmine.clock() to be installed.
  tabOutFromInput() {
    this.blurInputElement();
    jasmine.clock().tick(1);
  }
}

