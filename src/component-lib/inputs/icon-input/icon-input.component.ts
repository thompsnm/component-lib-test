/* tslint:disable */
import { Component, Input, Output, EventEmitter, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, NgModel } from '@angular/forms';
import { PGiValueAccessor } from '../value-accessor';

@Component({
  selector: 'pgi-icon-input',
  templateUrl: './icon-input.component.html',
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: PGiIconInput, multi: true}
  ],
  encapsulation: ViewEncapsulation.None
})
export class PGiIconInput extends PGiValueAccessor {
  @Input() color: String;
  @Input() disabled: Boolean;
  @Input() icon: String;
  @Input() placeholder: String;
  // TODO Dynamic validators
  @Input() required: Boolean;
  @Input() name: String;
  @ViewChild(NgModel) model: NgModel;

  private button;
  private input;

  get filled() {
    return this.value && this.value.length > 0;
  }

  constructor(private _element: ElementRef){
    super();
  }

  ngOnInit(){
    this.input = this._element.nativeElement.querySelector('input');
  }

  clear() {
    this.value = '';
    if( this.input ){
      // Set focus back to the input so the user can start typing again.
      this.input.focus();
    }
  }
}
