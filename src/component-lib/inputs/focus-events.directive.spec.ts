import { Component, HostListener } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { PGiFocusEventsPage } from './focus-events.directive.page';

@Component({
  template: `<button (enter)="doEnter()" (leave)="doLeave()" pgi-focus-events>I am a good listener</button>`
})
class TestWrapper {
  doEnter() {}
  doLeave() {}
}

describe('PGiFocusEvents', () => {
  let fixture, testComponent, focusDirective;

  beforeEach(async( () => {
    PGiFocusEventsPage.makeTestBed(TestWrapper)
      .compileComponents()
      .then( () => {
        fixture = TestBed.createComponent( TestWrapper );
        fixture.autoDetectChanges();

        testComponent = fixture.componentInstance;
        spyOn(testComponent, 'doEnter').and.callThrough();
        spyOn(testComponent, 'doLeave').and.callThrough();

        const el = fixture.debugElement.query( By.css('button') );
        focusDirective = new PGiFocusEventsPage( el );
      });
  }));

  it('should not emit any events on startup.', () => {
    expect(testComponent.doEnter).not.toHaveBeenCalled();
    expect(testComponent.doLeave).not.toHaveBeenCalled();
  });

  describe('after clicking the button', () => {
    beforeEach( () => {
      focusDirective.clickInsideElement();
      fixture.detectChanges();
    });

    it('should emit the enter event.', () => {
      expect(testComponent.doEnter).toHaveBeenCalled();
      expect(testComponent.doLeave).not.toHaveBeenCalled();
    });

    describe('and then clicking outside the button', () => {
      beforeEach( () => {
        testComponent.doEnter.calls.reset();
        focusDirective.clickOutsideElement();
      });

      it('should emit the leave event.', () => {
        expect(testComponent.doLeave).toHaveBeenCalled();
        expect(testComponent.doEnter).not.toHaveBeenCalled();
      });
    });

    describe('and then typing the TAB key', () => {
      beforeEach( () => {
        testComponent.doEnter.calls.reset();
        testComponent.doLeave.calls.reset();
        focusDirective.tabOutOfElement();
      });

      it('should emit the leave event.', () => {
        expect(testComponent.doLeave).toHaveBeenCalled();
        expect(testComponent.doEnter).not.toHaveBeenCalled();
      });
    });
  });

  describe('after tabbing to the button', () => {
    beforeEach( () => {
      testComponent.doEnter.calls.reset();
      testComponent.doLeave.calls.reset();
      focusDirective.tabIntoElement();
    });

    it('should emit the enter event.', () => {
      expect(testComponent.doEnter).toHaveBeenCalled();
      expect(testComponent.doLeave).not.toHaveBeenCalled();
    });
  });
});
