import { By } from '@angular/platform-browser';
import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { PGiIconsModule } from '../../icons';
import { PGiButtonsModule } from '../../buttons';
import { PGiInputsModule } from '../inputs.module';


export class PGiLabelInputPage {
  root = null;

  selectors = {
    inputElement: 'input',
    labelElement: 'label',
    errorElement: '.error-section',
    prefixElement: 'span'
  };

  static makeTestBed(container) {
    return TestBed
      // Our wrapping module should import the module under test.
      .configureTestingModule({
        imports: [PGiInputsModule, FormsModule],
        declarations: [container]
      })
      // Remove sub-dependencies from the module under test.
      .overrideModule(PGiInputsModule, {
        remove: {
          imports: [PGiIconsModule, PGiButtonsModule]
        },
        add: {
          schemas: [NO_ERRORS_SCHEMA]
        }
      });
  }

  constructor(debugElement) {
    if (!debugElement ||
      !debugElement.nativeElement ||
      debugElement.nativeElement.tagName.toLowerCase() !== 'pgi-label-input') {
      console.error('You need to pass a <pgi-label-input> element to PGiLabelInputPage constructor.');
    } else {
      this.root = debugElement;
    }
  }

  inputElement() {
    return this.root.query(By.css(this.selectors.inputElement));
  }

  prefixElement() {
    return this.root.query(By.css(this.selectors.prefixElement));
  }

  labelElement() {
    return this.root.query(By.css(this.selectors.labelElement));
  }

  errorElement() {
    return this.root.query(By.css(this.selectors.errorElement));
  }

  labelContent() {
    return this.labelElement().nativeElement.innerText;
  }

  errorContent() {
    return this.errorElement().nativeElement.innerText;
  }

  prefixContent() {
    return this.prefixElement().nativeElement.innerText;
  }

  inputValue() {
    return this.inputElement().nativeElement.value;
  }

  inputElementAttribute(attr: string) {
    return this.inputElement().nativeElement.getAttribute(attr);
  }

  labelElementAttribute(attr: string) {
    return this.labelElement().nativeElement.getAttribute(attr);
  }

  prefixElementAttribute(attr: string) {
    return this.prefixElement().nativeElement.getAttribute(attr);
  }

  errorElementAttribute(attr: string) {
    return this.errorElement().nativeElement.getAttribute(attr);
  }

  userSetValue(value) {
    const debugEl = this.inputElement();
    const el = debugEl.nativeElement;
    el.value = value;
    el.dispatchEvent(new Event('input'));
  }


  focusInputElement() {
    let input = this.inputElement();
    input.triggerEventHandler('focus', { target: input.nativeElement });
  }

  blurInputElement() {
    let input = this.inputElement();
    input.triggerEventHandler('blur', { target: input.nativeElement });
  }

}

