import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { PGiLabelInput } from './label-input.component';
import { PGiLabelInputPage } from './label-input.component.page';
import { Component } from '@angular/core';
@Component({
  template: `<pgi-label-input
    placeholder="Hi"
    color="primary"
    name="data-name"
    [labelText]="data.labelText"
    [errorMessage]="data.errorMessage"
    [(ngModel)]="data.name"
    [disabled] = "data.disabled"
    [type] = "data.type"
    [size] = "data.size"
    [maxlength] = "data.maxlength"
    [id] = "data.id"
    [prefix] = "data.prefix"
    [prefixWidth] = "data.prefixWidth"
    [inputWidth] = "data.inputWidth"
  ></pgi-label-input>`
})
class TestForm {
  data: object;
  constructor() {
    this.data = {
      name: 'foo',
      labelText: '',
      errorMessage: '',
      disabled: '',
      type: '',
      size: '',
      maxlength: '',
      id: null,
      prefix: '',
      prefixWidth: '',
      inputWidth: ''
    };
  }
}


describe('LabelInputComponent', () => {
  let fixture, form, labelInput, component, clearListeners;

  beforeEach(async(() => {
    PGiLabelInputPage.makeTestBed(TestForm)
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(TestForm);
        fixture.autoDetectChanges();

        form = fixture.componentInstance;
        let el = fixture.debugElement.query(By.css('pgi-label-input'));

        component = el.componentInstance;
        labelInput = new PGiLabelInputPage(el);
      })
      .catch((e) => {
        console.warn('LabelInputComponent ERROR' + e);
      });
  }));

  it('should display the initial model value.', () => {
    expect(labelInput.inputValue()).toEqual(form.data.name);
  });

  describe('when labelText is present and errorMessage is empty', () => {
    beforeEach(() => {
      form.data.labelText = 'Title';
      form.data.errorMessage = '';
      fixture.detectChanges();
    });

    it('should have only one label element', () => {
      expect(labelInput.labelElement()).not.toBe(null);
    });
    it('should have the labelText', () => {
      expect(labelInput.labelContent()).toBe(form.data.labelText);
    });
  });

  describe('when unsupported type passed', () => {
    it('should throw an error', () => {
      expect(function () {
        component.type = 'search';
      })
        .toThrow(new Error('Input type must be one of text, email, or password'));
    });
  });

  describe('when supported type passed', () => {
    it('should not throw an error', () => {
      expect(function () {
        component.type = 'email';
      })
        .not.toThrow(new Error('Input type must be one of text, email, or password'));
    });
  });

  describe('when size passed', () => {
    beforeEach(() => {
      form.data.size = '10';
      fixture.detectChanges();
    });

    it('should have size attribute in input', () => {
      expect(labelInput.inputElementAttribute('size')).toBe(form.data.size);
    });
  });

  describe('when there is no size passed', () => {
    beforeEach(() => {
      form.data.size = null;
      fixture.detectChanges();
    });

    it('should not have size attribute in input', () => {
      expect(labelInput.inputElementAttribute('size')).toBe(null);
    });
  });

  describe('when maxlength passed', () => {
    beforeEach(() => {
      form.data.maxlength = '10';
      fixture.detectChanges();
    });

    it('should have maxlength attribute in input', () => {
      expect(labelInput.inputElementAttribute('maxlength')).toBe(form.data.maxlength);
    });
  });

  describe('when there is no maxlength passed', () => {
    beforeEach(() => {
      form.data.maxlength = null;
      fixture.detectChanges();
    });

    it('should not have maxlength attribute in input', () => {
      expect(labelInput.inputElementAttribute('maxlength')).toBe(null);
    });
  });

  describe('when labelText is empty and errorMessage is empty ', () => {
    beforeEach(() => {
      form.data.labelText = '';
      form.data.errorMessage = '';
      fixture.detectChanges();
    });

    it('should not have the labelText or errorMessage label', () => {
      expect(labelInput.labelElement()).toBe(null);
    });
  });

  describe('when labelText and errorMessage are present', () => {
    beforeEach(() => {
      form.data.errorMessage = 'Error Message';
      form.data.labelText = 'Title';
      fixture.detectChanges();
    });

    it('should have the labelText as label', () => {
      expect(labelInput.labelContent()).toBe(form.data.labelText);
    });

    it('should have the errorMessage element', () => {
      expect(labelInput.errorContent()).toBe(form.data.errorMessage);
    });

    it('should have the `warn` and `input-error-label` classes', () => {
      expect(labelInput.errorElementAttribute('class')).toContain('input-error-label');
      expect(labelInput.labelElementAttribute('class')).toContain('input-error-label');
      expect(component.color).toBe('warn');
    });
  });

  describe('when labelText is empty and errorMessage is present', () => {
    beforeEach(() => {
      form.data.errorMessage = 'Error Message';
      form.data.labelText = '';
      fixture.detectChanges();
    });

    it('should have only one errorMessage section', () => {
      expect(labelInput.labelElement()).toBe(null);
    });

    it('should have the errorMessage element', () => {
      expect(labelInput.errorContent()).toBe(form.data.errorMessage);
    });

    it('should have the color `warn`', () => {
      expect(component.color).toBe('warn');
    });
    it('should have `input-error-label` as elements class', () => {
      expect(labelInput.errorElementAttribute('class')).toContain('input-error-label');
    });
  });

  describe('when input is focused', () => {
    beforeEach(() => {
      form.data.labelText = 'Title';
      labelInput.focusInputElement();
      fixture.detectChanges();
    });

    it('the labelText should have the `input-label-focused` class', () => {
      expect(labelInput.labelElementAttribute('class')).toContain('input-label-focused');
      expect(component.inputFocusClass).toBe('input-label-focused');
    });

    describe('when input is blurred', () => {
      beforeEach(() => {
        form.data.labelText = 'Title';
        labelInput.blurInputElement();
        fixture.detectChanges();
      });

      it('should have the `input-label-focused` class', () => {
        expect(labelInput.labelElementAttribute('class')).not.toContain('input-label-focused');
        expect(component.inputFocusClass).toBe('');
      });
    });
  });

  describe('when input is disabled', () => {
    beforeEach(() => {
      form.data.labelText = 'Title';
      form.data.disabled = 'disabled';
      fixture.detectChanges();
    });

    it('the labelText should have the `input-disabled-label` class', () => {
      expect(labelInput.labelElementAttribute('class')).toContain('input-disabled-label');
    });
    describe('user try to inputs data', () => {
      beforeEach(() => {
        labelInput.userSetValue('foo');
      });
      it('should not update model value', () => {
        expect(form.data.name).toEqual('foo');
      });
    });
  });

  describe('after the user inputs data', () => {
    let newValue = 'bar';
    beforeEach(() => {
      labelInput.userSetValue(newValue);
    });

    it('should update the model passed in when the user types input.', () => {
      expect(form.data.name).toEqual(newValue);
    });
  });

  describe('when id is passed to component', () => {
    beforeEach(() => {
      form.data.labelText = 'Title';
      form.data.id = 'inputId';
      fixture.detectChanges();
    });

    it('should add the id to the input element', () => {
      expect(labelInput.inputElementAttribute('id')).toBe('input_' + form.data.id);
    });

    it('should add the for attirbute to the label element', () => {
      expect(labelInput.labelElementAttribute('for')).toBe('input_' + form.data.id);
    });
  });

  describe('when no prefix is passed', () => {
    it('should not have a prefix element', () => {
      expect(labelInput.prefixElement()).toBe(null);
    });
  });

  describe('when a prefix is passed', () => {
    beforeEach(() => {
      form.data.prefix = 'Title';
      fixture.detectChanges();
    });

    it('should have a prefix element', () => {
      expect(labelInput.prefixElement()).not.toBe(null);
    });

    it('should populate the prefix element with the form data', () => {
      expect(labelInput.prefixContent()).toBe(form.data.prefix);
    });

  });

  describe('when a prefixWidth is passed', () => {
    it('should set the width attribute of the prefix element', () => {
      form.data.prefix = 'Title';
      form.data.prefixWidth = '2px';
      fixture.detectChanges();
      expect(labelInput.prefixElementAttribute('style')).toContain('width: ' + form.data.prefixWidth);
    });

  });


});
