import { Component, Input, Output, EventEmitter, ViewEncapsulation, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { NG_VALUE_ACCESSOR, NgModel } from '@angular/forms';
import { PGiValueAccessor } from '../value-accessor';

@Component({
  selector: 'pgi-label-input',
  templateUrl: './label-input.component.html',
  styleUrls: ['./label-input.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: PGiLabelInput, multi: true }
  ],
  encapsulation: ViewEncapsulation.None
})
export class PGiLabelInput extends PGiValueAccessor implements AfterViewInit {
  _color;
  _originalColor;
  _errorMessage;
  inputFocusClass;
  _type = 'text';
  focused;
  _disableTooltip = true;

  @Input()
  set color(color: string) {
    this._color = color;
    this._originalColor = color;
  }
  @Input() disabled = false;
  @Input()
  set errorMessage(errorMessage: string) {
    this._errorMessage = errorMessage;
    if (errorMessage && errorMessage.length > 0) {
      this._color = 'warn';
    } else {
      this._color = this._originalColor;
    }
  }
  @Input() id;
  @Input() labelText;
  @Input() maxlength;
  @Input() name;
  @Input() placeholder;
  @Input() required = false;
  @Input() size;
  @Input() inputWidth;
  @Input() prefix;
  @Input() prefixWidth;
  @Input()
  set type(type: string) {
    const supportedTypes: Array<string> = ['text', 'password', 'email'];
    if (type && supportedTypes.indexOf(type) < 0) {
      throw new Error('Input type must be one of text, email, or password');
    }
    this._type = type;
  }

  @ViewChild(NgModel) model: NgModel;

  constructor(private _element: ElementRef) {
    super();
  }

  ngAfterViewInit() {
    let span = this._element.nativeElement.querySelector('span');
    if (span && span.scrollWidth > span.clientWidth) {
      this._disableTooltip = false;
    }
  }

  get color(): string {
    return this._color;
  }

  isWarn() {
    return this._color === 'warn';
  }

  toggleFocus() {
    if (!this.isWarn()) {
      this.focused = !this.focused;
      this.inputFocusClass = this.focused ? 'input-label-focused' : '';
    }
  }

  renderAttr(attrValue: string) {
    return attrValue ? attrValue : null;
  }

  getInputId(): string {
    if (this.id == null) {
      return null;
    } else {
      return 'input_' + this.id;
    }
  }
  get disableTooltip(): Boolean {
    return this._disableTooltip;
  }
}
