import { Component, ElementRef, Input, ViewEncapsulation } from '@angular/core';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'pgi-settings-input',
  templateUrl: './settings-input.component.html',
  styleUrls: ['./settings-input.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PGiSettingsInput {
  _errorMessage;

  @Input() color;
  @Input()
  set errorMessage(errorMessage: string) {
    this._errorMessage = errorMessage;
    if (errorMessage && errorMessage.length > 0) {
      this._element.nativeElement.classList.add('warned');
    } else {
      this._element.nativeElement.classList.remove('warned');
    }
  }
  @Input() labelText;
  @Input() placeholder;
  @Input() inputWidth;
  @Input() disabled = false;
  @Input() labelWidth;

  get errorMessage(): string {
    return this._errorMessage;
  }

  constructor(private _element: ElementRef) {
  }
}
