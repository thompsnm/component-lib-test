import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PGiSettingsInput } from './settings-input.component';
import { PGiInputsModule } from '../inputs.module';

describe('PGiSettingsInput', () => {
  let component: PGiSettingsInput;
  let fixture: ComponentFixture<PGiSettingsInput>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PGiSettingsInput],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PGiSettingsInput);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should not have a label', () => {
    expect(fixture.debugElement.nativeElement.querySelector('label')).toBe(null);
  });

  it('should have a label', () => {
    component.labelText = 'Label';
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('label').textContent).toBe('Label');
  });

  it('should clear disabled class on label if enabled', () => {
    component.disabled = false;
    component.labelText = 'My Label';
    fixture.detectChanges();

    expect(fixture.debugElement.nativeElement.querySelector('label').classList.contains('input-disabled-label')).toBe(false);
  });

  it('should set disabled class on label if disabled', () => {
    component.disabled = true;
    component.labelText = 'My Label';
    fixture.detectChanges();

    expect(fixture.debugElement.nativeElement.querySelector('label').classList.contains('input-disabled-label')).toBe(true);
  });

  it('should set the width attribute of the label element if prefixWidth set', () => {
    component.labelText = 'My Label';
    component.labelWidth = '20px';
    fixture.detectChanges();

    expect(fixture.debugElement.nativeElement.querySelector('label').getAttribute('style')).toContain('width: ' + component.labelWidth);
  });

  it('should clear the warned class when error message is NOT set', () => {
    component.labelText = 'My Label';
    component.labelWidth = '200px';
    fixture.detectChanges();

    expect(fixture.debugElement.nativeElement.classList).not.toContain('warned');
  });

  it('should set the warned class when error message set', () => {
    component.labelText = 'My Label';
    component.errorMessage = 'ERROR';
    fixture.detectChanges();

    expect(fixture.debugElement.nativeElement.classList).toContain('warned');
  });

  it('should remove the warned class when error message is cleared', () => {
    component.labelText = 'My Label';
    component.errorMessage = 'ERROR';
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.classList).toContain('warned');

    component.errorMessage = '';
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.classList).not.toContain('warned');
  });
});
