import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

import { PGiRadioButtonComponent } from './radio-button.component';
export class PGiRadioButtonPage {
    root: DebugElement = null;

    constructor (debugElement: DebugElement) {
        this.root = debugElement;
    }

    getComponent(): PGiRadioButtonComponent {
      let el = this.root.query(By.all());
      return el.componentInstance;
    }

    getInputElement() {
        return this.root.nativeElement.querySelector('input');
    }
}
