import { Component, HostListener, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { PGiRadioButtonComponent } from './radio-button.component';
import { PGiRadioButtonPage } from './radio-button.component.page';
import { MaterialModule, MdRadioModule, MdRadioButton } from '@angular/material';

@Component({
  template: `
  <pgi-radio-button (change)='changeWatcher($event)' [checked]='startChecked' [disabled]='disabledValue' [color]='colorName' value='test'>
    TestLabel
  </pgi-radio-button>`
})
class TestWrapper {
  disabledValue = false;
  startChecked = false;
  changeWatcher(newValue: boolean) {}
}

describe('PGiRadioButtonComponent', () => {
  let component: PGiRadioButtonComponent;
  let fixture: ComponentFixture<TestWrapper>;
  let page: PGiRadioButtonPage;
  let testWrapper, input;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MdRadioModule],
      declarations: [ PGiRadioButtonComponent, TestWrapper ]
    })
    .compileComponents().then( () => {
      fixture = TestBed.createComponent(TestWrapper);
      fixture.autoDetectChanges();
      page = new PGiRadioButtonPage(fixture.debugElement);
      component = page.getComponent();
      testWrapper = fixture.componentInstance;
      input = page.getInputElement();
      spyOn(testWrapper, 'changeWatcher').and.callThrough();
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have defaulted to checked', () => {
    expect(component.checked).toBe(false);
  });

  it('should register changes with a click', () => {
    input.click();
    expect(component.checked).toBe(true);
  });

  it('should update checked value when input is changed', () => {
    expect(component.checked).toBe(false);
    testWrapper.startChecked = true;
    fixture.detectChanges();
    expect(component.checked).toBe(true);
  });

  it('should become disabled when the disabled value is updated', () => {
    testWrapper.disabledValue = true;
    fixture.detectChanges();
    input.click();
    expect(component.checked).toBe(false);
  });

  it('should send an event when the checked value is changed' , () => {
    testWrapper.startChecked = true;
    fixture.detectChanges();
    expect(testWrapper.changeWatcher).toHaveBeenCalledWith(true);
    expect(component.checked).toBe(true);
  });

  it('should have the proper value', () => {
    expect(component.value).toBe('test');
    expect(input.value).toBe('test');
  });

  it('should have accent color', () => {
    testWrapper.colorName = 'accent';
    fixture.detectChanges();
    expect(component.color).toBe('accent');
  });
});
