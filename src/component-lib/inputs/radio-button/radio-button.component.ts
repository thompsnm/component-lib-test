import { Component, ViewEncapsulation, Input, Output, EventEmitter, ElementRef, AfterViewInit } from '@angular/core';
import { PGiValueAccessor } from '../value-accessor';

@Component({
  selector: 'pgi-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PGiRadioButtonComponent extends PGiValueAccessor implements AfterViewInit {
  @Input() ariaLabel: String;
  @Input() ariaLabelledby: String;
  @Input() id: String;
  @Input() required: Boolean = false;
  @Input() labelPosition: String = 'after';
  @Input() disabled: Boolean;
  @Input() name: String;
  @Input() value: String;
  @Input() color: String;

  @Input() set checked(checked: boolean) {
    this.element.nativeElement.querySelector('.mat-radio-input').checked = checked;
    this.change.emit(checked);
  }
  get checked(): boolean {
    return this.element.nativeElement.querySelector('.mat-radio-input').checked;
  }

  @Output() change: EventEmitter<Boolean> = new EventEmitter<Boolean>();

  constructor(private element: ElementRef) {
    super();
  }

  ngAfterViewInit() {
    let this_element = this.element.nativeElement;
    let input_element = this_element.querySelector('.mat-radio-input');
    input_element.value = this_element.getAttribute('value');
    let baseElement = this_element.querySelector('.mat-radio-outer-circle');
    let halo = document.createElement('div');
    halo.classList.add('halo');
    baseElement.appendChild(halo);
  }

  checkChange($event: any) {
    let allpgiradiobuttons = document.getElementsByTagName("pgi-radio-button");
    let i: number;
    for (i = allpgiradiobuttons.length; i >= 1; i--) {
      let this_button = allpgiradiobuttons[i - 1];
      if (this_button.id === $event.source.id) {
        this_button.classList.add('radio-checked');
      } else if (this_button.classList.contains('radio-checked')) {
        this_button.classList.remove('radio-checked');
      }
    }
  }
}
