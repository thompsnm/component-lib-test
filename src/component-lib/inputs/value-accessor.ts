import { ControlValueAccessor } from '@angular/forms';

/*
 * PGiValueAccessor provides a base class that implements the
 * requirements for an NgForm control. It should be used as the
 * base for custom form controls in the component library. See
 * the pgi-icon-input for an example of how this can be used
 * with a custom control.
 *
 * @see http://blog.rangle.io/angular-2-ngmodel-and-custom-form-components/
 */
export class PGiValueAccessor implements ControlValueAccessor {
  private _value;

  private _changeListeners = new Array<(value) => void>();
  private _touchListeners = new Array<() => void>();

  get value() {
    return this._value;
  }

  set value(value) {
    if (this._value !== value) {
      this._value = value;
      this._changeListeners.forEach(f => f(value));
    }
  }

  touch() {
    this._touchListeners.forEach(f => f());
  }

  writeValue(value) {
    this._value = value;
  }

  registerOnChange(fn: (value) => void) {
    this._changeListeners.push(fn);
  }

  registerOnTouched(fn: () => void) {
    this._touchListeners.push(fn);
  }
}
