import { By } from '@angular/platform-browser';
import { TestBed } from '@angular/core/testing';
import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { PGiFocusEventsPage } from '../focus-events.directive.page';
import { PGiInputsModule } from '../inputs.module';
import { PGiIconsModule } from '../../icons';
import { PGiButtonsModule } from '../../buttons';

@Component({
  selector: 'pgi-icon-button',
  template: '<button>x</button>'
})
class PGiIconButtonFake {}

@Component({
  selector: 'pgi-icon',
  template: '<i>icon</i>'
})
class PGiIconFake {}

export class PGiAutocompletePage extends PGiFocusEventsPage {
  selectors = {
    dropdownWrapper: '[name=autocompleteResults]',
    result: 'pgi-autocomplete-result',
    openClass: 'pgi-open'
  };

  static makeTestBed( container ) {
    return super.makeTestBed( container );
  }

  constructor(debugElement) {
    super(debugElement);
  }

  dropdownElement() {
    return this.root.query( By.css( this.selectors.dropdownWrapper ) );
  }

  resultElements() {
    return this.root.queryAll( By.css( this.selectors.result ) );
  }

  resultCount() {
    return this.resultElements().length;
  }

  hostHasClass(className) {
    return !!this.root.classes[className];
  }

  isOpen() {
    const results = this.resultElements();
    const openClass = this.hostHasClass( this.selectors.openClass );
    return !!(results && results.length > 0 && openClass );
  }

  clickResult(index) {
    const result = this.resultElements()[index];
    result.nativeElement.dispatchEvent( this.makeClickEvent() );
  }
}
