import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'pgi-autocomplete-result',
  template: '<ng-content></ng-content>',
  encapsulation: ViewEncapsulation.None
})
export class PGiAutocompleteResult {}
