import { Component, HostListener } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { PGiAutocompletePage } from './autocomplete.component.page';

@Component({
  template: `<pgi-autocomplete>
    <input type="text">
    <pgi-autocomplete-result>Foo</pgi-autocomplete-result>
    <pgi-autocomplete-result>Bar</pgi-autocomplete-result>
    <pgi-autocomplete-result>Baz</pgi-autocomplete-result>
  </pgi-autocomplete>`
})
class TestWrapper {}

describe('PGiAutocomplete', () => {
  let fixture, testComponent, autocomplete;

  beforeEach(async( () => {
    PGiAutocompletePage.makeTestBed(TestWrapper)
      .compileComponents()
      .then( () => {
        fixture = TestBed.createComponent( TestWrapper );
        fixture.autoDetectChanges();

        testComponent = fixture.componentInstance;

        const el = fixture.debugElement.query( By.css('pgi-autocomplete') );
        autocomplete = new PGiAutocompletePage( el );
      });
  }));

  it('should create the autocomplete.', () => {
    expect(autocomplete.root).toBeTruthy();
  });

  it('should hide the results by default.', () => {
    expect(autocomplete.isOpen()).toBe(false);
  });

  describe('after focusing the input', () => {
    beforeEach( () => {
      autocomplete.tabIntoElement();
    });

    it('should show the dropdown.', () => {
      expect(autocomplete.isOpen()).toBe(true);
    });

    it('should show all results elements.', () => {
      expect(autocomplete.resultCount()).toBe(3);
    });

    describe('and then clicking on a result', () => {
      let documentClickSpy;

      beforeEach( () => {
        documentClickSpy = jasmine.createSpy('documentClick');
        document.addEventListener('click', documentClickSpy);

        autocomplete.clickResult(0);
      });

      it('should not allow dropdown clicks to bubble and thereby unfocus the autocomplete.', () => {
        expect(documentClickSpy).not.toHaveBeenCalled();
        expect(autocomplete.isOpen()).toBe(true);
      });
    });

    describe('and then clicking on the document', () => {
      beforeEach( () => {
        autocomplete.clickOutsideElement();
      });

      it('should hide the dropdown.', () => {
        expect(autocomplete.isOpen()).toBe(false);
      });
    });
  });
});
