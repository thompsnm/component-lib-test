import { Component, ViewEncapsulation, ElementRef, HostBinding, HostListener, ChangeDetectorRef } from '@angular/core';
import { PGiFocusEvents } from '../focus-events.directive';

@Component({
  selector: 'pgi-autocomplete',
  templateUrl: './autocomplete.component.html',
  encapsulation: ViewEncapsulation.None
})
export class PGiAutocomplete extends PGiFocusEvents {
  private _selectors = {
    dropdown: '[name=autocompleteResults]'
  };

  private _dropdown;
  private _open = false;

  constructor( private _element: ElementRef, private _changeDetector: ChangeDetectorRef ) {
    super();
  }

  // TODO Keyboard listeners for ESC, UP, DOWN?
  // TODO Open to the top if we are too close to the bottom of the page.

  @HostBinding('class.pgi-open')
  public get openClass (){ return this.isOpen(); }

  @HostListener('enter')
  public open()     { this._open = true; }
  @HostListener('leave')
  public close()    { this._open = false; }
  public toggle()   { this._open = !this._open; }

  public isClosed() { return !this._open; }
  public isOpen()   { return this._open; }

  ngAfterViewInit() {
    this._dropdown = this._element.nativeElement.querySelector( this._selectors.dropdown );

    // Prevent clicks inside the results overlay from removing
    // focus from this component.
    this._dropdown.addEventListener('click', (event) => {
      event.stopPropagation();
    });
  }
}
