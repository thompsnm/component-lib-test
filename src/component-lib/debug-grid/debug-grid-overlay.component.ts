import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'pgi-debug-grid-overlay',
  template: '<div pgi-debug-grid></div>',
  encapsulation: ViewEncapsulation.None
})
export class PGiDebugGridOverlay { }
