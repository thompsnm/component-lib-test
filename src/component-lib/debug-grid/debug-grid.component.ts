import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[pgi-debug-grid]',
})
export class PGiDebugGrid {
  @Input('pgi-debug-grid') targetSelector;
  @Input('pgi-debug-grid-key') keyCodeConfig;
  @Input('pgi-debug-grid-class') classConfig;

  className = 'pgi-debug-grid';
  targetEl;
  keyCode = '\'';

  constructor(private el: ElementRef) {}

  ngOnInit() {
    this.targetEl = this.targetSelector ? document.querySelector( this.targetSelector ) : this.el.nativeElement;
    this.keyCode = this.keyCodeConfig ? this.keyCodeConfig : this.keyCode;
    this.className = this.classConfig ? this.classConfig : this.className;
  }

  get visible() {
    return this.targetEl.classList.contains(this.className);
  }

  @HostListener('window:keydown', ['$event'])
  keydownListener($event) {
    if ( $event.key === this.keyCode && $event.ctrlKey ) {
      this.toggleGrid();
    }
  }

  public toggleGrid() {
    if ( this.visible ) {
      this.targetEl.classList.remove(this.className);
    }
    else {
      this.targetEl.classList.add(this.className);
    }
  }
}
