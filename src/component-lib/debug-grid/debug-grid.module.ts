import { NgModule } from '@angular/core';
import { PGiButtonsModule } from '../buttons';
import { PGiDebugGrid } from './debug-grid.component';
import { PGiDebugGridOverlay } from './debug-grid-overlay.component';

@NgModule({
  declarations: [
    PGiDebugGrid,
    PGiDebugGridOverlay
  ],
  imports: [
    PGiButtonsModule
  ],
  exports: [
    PGiDebugGrid,
    PGiDebugGridOverlay
  ]
})
export class PGiDebugGridModule { }
