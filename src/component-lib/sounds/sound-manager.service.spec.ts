import { Observable } from 'rxjs/Rx';
import { PGiAudioElement } from './audio-element';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { Http, Response, BaseRequestOptions, ResponseOptions, HttpModule } from '@angular/http';
import { PGI_SOUND_SET } from './sound-registry.service';
import { PGiSoundService } from './sound-manager.service';
import { TestBed, async, inject, getTestBed } from '@angular/core/testing';

let testSoundObj = {
  'testSound': 'path/to/test/sound'
};

describe('PGiSoundService', () => {
  let mockBackend, soundSet,
    originalAudio, loadSpy, playSpy, pauseSpy,
    originalContext, creatGainSpy, createBufferSourceSpy, connectSpy, startSpy, stopSpy, disconnectSpy, destinationSpy, decodeAudioDataSpy, playbackRateSpy, fecthAudioSpy,
    bufferSpy,
    mockResponse = {
      arrayBuffer: () => { }
    };
  function audioContextMock() {
    this.createGain = creatGainSpy;
    this.createBufferSource = createBufferSourceSpy;
    this.decodeAudioData = decodeAudioDataSpy;
    this.destination = destinationSpy;
  }
  function audioMock() {
    this.load = loadSpy;
    this.play = playSpy;
    this.pause = pauseSpy;
    this.loop = false;
  };
  function audioBufferSourceNodeMock() {
    this.connect = connectSpy,
      this.start = startSpy,
      this.stop = stopSpy,
      this.disconnect = disconnectSpy,
      this.loop = false
  }
  function gainNodeMock() {
    this.connect = connectSpy;
  }
  beforeAll(() => {
    originalAudio = (window as any).Audio;
    originalContext = (window as any).AudioContext;
  });
  beforeEach(() => {
    decodeAudioDataSpy = jasmine.createSpy('decodeAudioData Spy').and.returnValue(mockResponse);
    createBufferSourceSpy = jasmine.createSpy('createBufferSource Spy').and.returnValue(new audioBufferSourceNodeMock());
    creatGainSpy = jasmine.createSpy('creatGain Spy').and.returnValue(new gainNodeMock());
    destinationSpy = jasmine.createSpy('destination Spy');
    connectSpy = jasmine.createSpy('connect Spy');
    startSpy = jasmine.createSpy('start Spy');
    stopSpy = jasmine.createSpy('stop Spy');
    disconnectSpy = jasmine.createSpy('disconnect Spy');
    (window as any).AudioContext = audioContextMock;

    loadSpy = jasmine.createSpy('load Spy');
    playSpy = jasmine.createSpy('play Spy');
    pauseSpy = jasmine.createSpy('pause Spy');
    (window as any).Audio = audioMock;

    TestBed.configureTestingModule({
      providers: [
        PGiSoundService,
        MockBackend,
        BaseRequestOptions,
        {
          provide: Http,
          useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(backendInstance, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        { provide: PGI_SOUND_SET, useValue: testSoundObj }],
      imports: [
        HttpModule
      ]
    });
    mockBackend = getTestBed().get(MockBackend);
    mockBackend.connections.subscribe(
      (connection: MockConnection) => {
        connection.mockRespond(
          new Response(
            new ResponseOptions({
              body: { mockResponse }
            })))
      });
  });
  afterAll(() => {
    (window as any).AudioContext = originalContext;
    (window as any).Audio = originalAudio;
  });
  it('should have a sound service', async(inject(
    [PGiSoundService, PGI_SOUND_SET], (service, SoundSet) => {
      expect(service).toBeDefined();
    })));
  it('Should play the sound from soundset', async(inject(
    [PGiSoundService, PGI_SOUND_SET], (service, SoundSet) => {
      service.play('testSound').subscribe(value => {
        expect(value).toBe(true);
      });
    })));
  it('Should play sound using DOM Audio', async(inject(
    [PGiSoundService, PGI_SOUND_SET], (service, SoundSet) => {
      (window as any).AudioContext = null;
      service.play('testSound').subscribe(value => {
        expect(value).toBe(true);
      });
    })));
  it('Should play remote sound', async(inject(
    [PGiSoundService, PGI_SOUND_SET], (service, SoundSet) => {
      service.fetchAndPlay('http://samples.com/sound.mp3').subscribe(value => {
        expect(value).toBe(true);
      });
    })));
  it('Should stop playing sound', async(inject(
    [PGiSoundService, PGI_SOUND_SET], (service, SoundSet) => {
      service.play('testSound');
      service.stop('testSound').subscribe(value => {
        expect(value).toBe(true);
      });
    })));
  it('Should stop playing sound using DOM Audio', async(inject(
    [PGiSoundService, PGI_SOUND_SET], (service, SoundSet) => {
      (window as any).AudioContext = null;
      service.play('testSound');
      service.stop('testSound').subscribe(value => {
        expect(value).toBe(true);
      });
    })));
  it('Should stop playing remote sound', async(inject(
    [PGiSoundService, PGI_SOUND_SET], (service, SoundSet) => {
      service.fetchAndPlay('http://samples.com/sound.mp3');
      service.stop('http://samples.com/sound.mp3').subscribe(value => {
        expect(value).toBe(true);
      });
    })));
  it('Should stop playing all sounds', async(inject(
    [PGiSoundService, PGI_SOUND_SET], (service, SoundSet) => {
      service.play('testSound');
      service.fetchAndPlay('http://samples.com/sound.mp3');
      service.stopAll().subscribe(value => {
        expect(value).toBe(true);
      });
    })));
  it('Should stop playing all sounds using DOM Audio', async(inject(
    [PGiSoundService, PGI_SOUND_SET], (service, SoundSet) => {
      (window as any).AudioContext = null;
      service.play('testSound');
      service.fetchAndPlay('http://samples.com/sound.mp3');
      service.stopAll().subscribe(value => {
        expect(value).toBe(true);
      });
    })));
  it('Should throw error if prefetch fails', async(inject(
    [PGiSoundService, PGI_SOUND_SET], (service, SoundSet) => {
      spyOn(service, 'fetchAudio').and.callFake(() => { Promise.reject('cant play file') });
      service.play('testSound').subscribe(
        val => { },
        err => { expect(err.message).toBe('cant play file'); });
    })));
  it('Should throw error if play fails', async(inject(
    [PGiSoundService, PGI_SOUND_SET], (service, SoundSet) => {
      startSpy.and.callFake(() => { throw Error('cant play file') })
      service.play('testSound').subscribe(
        val => { },
        err => { expect(err.message).toBe('cant play file'); });
    })));
  it('Should throw error if play fails using Dom Audio', async(inject(
    [PGiSoundService, PGI_SOUND_SET], (service, SoundSet) => {
      (window as any).AudioContext = null;
      loadSpy.and.callFake(() => { throw Error('cant play file') })
      service.play('testSound').subscribe(
        val => { },
        err => {
          expect(err.message).toBe('cant play file');
        });
    })));
});

