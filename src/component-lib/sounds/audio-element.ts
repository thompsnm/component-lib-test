export class PGiAudioElement {
  private audio: any;
  private context: AudioContext;
  private gainNode: GainNode;
  private scriptProcessNode: ScriptProcessorNode;
  private analyserNode: AnalyserNode;
  private feed: any;
  private freqs: Uint8Array;
  private autoplay: boolean;
  private _loop: boolean;

  set loop(value: boolean) {
    this._loop = value;
  }

  get loop(): boolean {
    return this._loop;
  }
  /**
   * @param  {any} audiosrc - This should be a audiobuffer when using Web Audio Api and a path to sound source when using DOM audio
   * @param  {AudioContext} context?
   */
  constructor(audiosrc: any, context?: AudioContext) {
    this.feed = audiosrc;
    this.context = context ? context : null;
    if (!this.context) {
      this.audio = new Audio();
      this.audio.src = this.feed;
      this.audio.load();
      this.audio.volume = 0.00000001;
      this.audio.play();
    }
  }
  play() {
    if (this.context) {
      this.stop();
      this.gainNode = this.context.createGain();
      this.audio = this.context.createBufferSource();
      this.audio.buffer = this.feed;
      this.audio.connect(this.gainNode);
      this.gainNode.connect(this.context.destination);
      this.audio.connect(this.context.destination);
      if (!this.audio.start) {
        this.audio.start = this.audio.noteOn;
      }
      this.audio.loop = this._loop;
      this.audio.start(0);
    } else {
      this.audio.volume = 1;
      this.audio.currentTime = 0;
      this.audio.loop = this._loop;
      this.audio.play();
    }
  }
  // Need to see if we need to pass this data in future for visualization
  createAnalyserNode() {
    this.scriptProcessNode = this.context.createScriptProcessor(1024);
    this.analyserNode = this.context.createAnalyser();
    this.scriptProcessNode.connect(this.context.destination);
    this.analyserNode.connect(this.scriptProcessNode);
    this.analyserNode.fftSize = 2048;
    this.freqs = new Uint8Array(this.analyserNode.frequencyBinCount);
    this.audio.connect(this.analyserNode);
  }
  stop() {
    if (this.audio) {
      if (this.context) {
        if (!this.audio.stop) {
          this.audio.start = this.audio.noteOff;
        }
        this.audio.stop();
        this.audio.disconnect();
      } else {
        this.audio.pause();
        this.audio.currentTime = 0;
      }
    } else {
      console.log('this.audio is undefined');
    }
  }
  setVolume(value: number) {
    if (this.context) {
      this.gainNode.gain.value = value;
    } else {
      this.audio.volume = value;
    }
  }
  mute() {
    this.setVolume(0);
  }
  isPlaying(): boolean {
    if (this.context) {
      // return this.audio.suspend
      // figure out how to detect web audio play state
      return true;
    } else {
      return !this.audio.paused;
    }
  }
  getAnalyseData() {
    return this.analyserNode.getByteFrequencyData(this.freqs);
  }
}
