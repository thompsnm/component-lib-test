import { PGiAudioElement } from './audio-element';
import { PGI_SOUND_SET } from './sound-registry.service';
import { Observable } from 'rxjs/Observable';
import { Http, ResponseContentType } from '@angular/http';
import { Injectable, Inject, OnDestroy } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
@Injectable()
export class PGiSoundService implements OnDestroy {

  private _context: AudioContext;
  private _audioElements: object = {};
  private _audioBuffers: object = {};
  /**
   * @param  {Http} privatehttp
   * @param  {} @Inject(PGI_SOUND_SET
   * @param  {any} privatesoundSet
   */
  constructor(private http: Http, @Inject(PGI_SOUND_SET) private soundSet: any) {
    try {
      this._context = new AudioContext();
    } catch (error) {
      // Need to log web Audio Api isn't supported
      // Need to push this info into analytics.
    }
    this.preFetchAudioFromSoundSet();
  }
  /**
   * @param  {string} audio
   * @returns Observable
   */
  private fetchAudio(audio: string): Promise<AudioBuffer> {
    return this.http.get(audio, { responseType: ResponseContentType.ArrayBuffer })
      .toPromise()
      .then(data => { return this._context.decodeAudioData(data.arrayBuffer()); })
      .catch(err => { return Promise.reject(err); });
  }
  private preFetchAudioFromSoundSet() {
    for (let sound in this.soundSet) {
      if (this._context) {
        this.fetchAudio(this.soundSet[sound]).then(
          (audioBuff) => {
            if (audioBuff) {
              this._audioBuffers[sound] = audioBuff;
            }
          },
          (err) => {
            console.log(`Failed prefecthing audio: ${err}`);
          });
      } else {
        try {
          this._audioElements[sound] = new PGiAudioElement(this.soundSet[sound]);
        } catch (error) {
          this._audioElements[sound] = null;
        }
      }
    }
  }
  /**
   * @param  {string} audioName
   * @param  {boolean} loop?
   * @returns Observable
   */
  play(audioName: string, loop?: boolean): Observable<boolean> {
    let playSuccess: boolean;
    let _loop = loop ? loop : false;
    try {
      if (!this._audioElements[audioName]) {
        if (this._context && this._audioBuffers[audioName]) {
          this._audioElements[audioName] = new PGiAudioElement(this._audioBuffers[audioName], this._context);
        } else {
          this._audioElements[audioName] = new PGiAudioElement(audioName);
        }
      }
      this._audioElements[audioName].loop = _loop;
      this._audioElements[audioName].play();
      playSuccess = true;
    } catch (error) {
      return Observable.throw(error);
    }
    return Observable.of(playSuccess);
  }
  /**
   * @param  {string} audioUrl
   * @param  {boolean} loop?
   * @returns Observable
   */
  fetchAndPlay(audioUrl: string, loop?: boolean): Observable<boolean> {
    let playSuccess: boolean;
    let _loop = loop ? loop : false;
    try {
      // Using Dom audio for processing remote audio files as we run in to CORS issue when loading remote files using http
      let audio = this.soundSet[audioUrl] ? this.soundSet[audioUrl] : audioUrl;
      if (!this._audioElements[audio]) {
        this._audioElements[audio] = new PGiAudioElement(audio);
      }
      this._audioElements[audio].loop = _loop;
      this._audioElements[audio].play();
      playSuccess = true;
    } catch (error) {
      return Observable.throw(error);
    }
    return Observable.of(playSuccess);
  }
  /**
   * @param  {string} audio
   * @returns Observable
   */
  stop(audio: string): Observable<boolean> {
    try {
      if (this._audioElements[audio]) {
        this._audioElements[audio].stop();
      }
    } catch (error) {
      return Observable.throw(error);
    }
    return Observable.of(true);
  }
  /**
   * @returns Observable
   */
  stopAll(): Observable<boolean> {
    try {
      for (let el in this._audioElements) {
        if (this._audioElements[el]) {
          this._audioElements[el].stop();
        }

      }
    } catch (error) {
      return Observable.throw(error);
    }
    return Observable.of(true);
  }
  ngOnDestroy() {
    for (let el in this._audioElements) {
      if (this._audioElements[el]) {
        this._audioElements[el].stop();
        this._audioElements[el] = null;
      }
    }
    for (let el in this._audioBuffers) {
      if (this._audioBuffers[el]) {
        this._audioBuffers[el] = null;
      }
    }
    this._audioBuffers = null;
    this._audioElements = null;
    if (this._context) {
      this._context.close();
    }
    this._context = null;
  }
}
