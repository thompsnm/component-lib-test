import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'pgi-tool-bar',
  templateUrl: './tool-bar.component.html',
  encapsulation: ViewEncapsulation.None
})
export class PGiToolBar {}
