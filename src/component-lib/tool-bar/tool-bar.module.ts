import { NgModule } from '@angular/core';
import { PGiToolBar } from './tool-bar.component';

@NgModule({
  declarations: [
    PGiToolBar
  ],
  exports: [
    PGiToolBar
  ]
})
export class PGiToolBarModule {}
