import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PGiButtonsModule } from '../buttons';
import { PGiCollapse } from './collapse.component';

@NgModule({
  imports: [
    PGiButtonsModule,
    CommonModule
  ],
  declarations: [
    PGiCollapse
  ],
  exports: [
    PGiCollapse
  ]
})
export class PGiCollapseModule {}

