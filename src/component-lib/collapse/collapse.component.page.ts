import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { PGiCollapseModule } from './collapse.module';
import { PGiButtonsModule } from '../buttons';

@Component({
  selector: 'pgi-icon-button',
  template: '<button>x</button>'
})
class PGiIconButtonFake {}

export class PGiCollapsePage {
  root = null;

  selectors = {
    content: '[name=content]',
    toggle: '[name=collapseButton]'
  };

  static makeTestBed(container) {
    return TestBed
      // Import the module under test.
      .configureTestingModule( {
        imports: [ PGiCollapseModule ],
        declarations: [ container ]
      })
      // Remove sub-dependencies from the module under test.
      .overrideModule(PGiCollapseModule, {
        remove: {
          imports: [ PGiButtonsModule ]
        },
        add: {
          schemas: [ NO_ERRORS_SCHEMA ],
          declarations: [ PGiIconButtonFake ]
        }
      }
    );
  }

  constructor(debugElement) {
    this.root = debugElement;
  }

  contentElement() {
    return this.root.query(By.css( this.selectors.content ));
  }

  contentIsOpen() {
    const content = this.contentElement();
    return content.nativeElement.offsetHeight > 1;
  }

  contentIsClosed() {
    const content = this.contentElement();
    return content.nativeElement.offsetHeight === 0;
  }

  toggleElement() {
    return this.root.query(By.css( this.selectors.toggle ));
  }

  clickCollapseButton() {
    const button = this.toggleElement();
    button.triggerEventHandler('click', {});
  }
}

