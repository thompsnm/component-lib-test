import { Component, Input, Renderer, ElementRef, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'pgi-collapse',
  templateUrl: './collapse.component.html',
  encapsulation: ViewEncapsulation.None
})
export class PGiCollapse {
  @Input() collapsed;
  collapseEvent = new EventEmitter<Boolean>();
  renderer;
  rootElement;
  contentElement;
  contentElementSelector = '.content';
  contentHeight: any = 'auto';
  originalHeight = 0;

  constructor(renderer: Renderer, el: ElementRef) {
    this.renderer = renderer;
    this.rootElement = el;
    if ( this.collapsed ) {
      this.collapsed = this.collapsed === 'false' ? false : true;
    } else {
      // Default to false.
      this.collapsed = false;
    }
  }

  ngAfterViewInit() {
    this.contentElement = this.rootElement.nativeElement.querySelector(this.contentElementSelector);
    this.originalHeight = this.contentElement.offsetHeight;
    // Arrrg Angular...you kill me
    window.setTimeout(() => {
      this.updateHeight();
    });
  }

  // TODO testing
  updateHeight() {
    this.contentHeight = this.collapsed ? 0 : this.originalHeight;
  }

  toggleCollapse() {
    this.collapsed = !this.collapsed;
    this.updateHeight();
    this.collapseEvent.emit(this.collapsed);
  }
}
