import { Component } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';

import { PGiCollapse } from './collapse.component';
import { PGiCollapsePage } from './collapse.component.page';


const contentText = '<p id="foo">This is the content to hide.</p>';

@Component({
  template: `<pgi-collapse>${contentText}</pgi-collapse>`
})
class DefaultComponentWrapper {}

@Component({
  template: `<pgi-collapse collapsed="true">${contentText}</pgi-collapse>`
})
class ClosedComponentWrapper {}


describe('PGiCollapse', () => {
  let component;
  let collapse;
  let fixture;
  let contentEl;

  describe('without a collapse attribute', () => {
    beforeEach(async(() => {
      PGiCollapsePage.makeTestBed( DefaultComponentWrapper )
        .compileComponents()
        .then( () => {
          fixture = TestBed.createComponent(DefaultComponentWrapper);
          fixture.autoDetectChanges();
          collapse = new PGiCollapsePage( fixture.debugElement );
          contentEl = collapse.contentElement();
        });
    }));

    it('should contain the correct content.', () => {
      const content = contentEl.nativeElement.innerHTML.trim();
      expect(content).toEqual( contentText );
    });

    it('should be open by default.', () => {
      expect(collapse.contentIsOpen()).toBe(true);
    });

    describe('after clicking the collapse button', () => {
      beforeEach(() => {
        collapse.clickCollapseButton();
        fixture.detectChanges();
      });

      it('should close the content.', () => {
        expect(collapse.contentIsClosed()).toBe(true);
      });

      describe('twice', () => {
        beforeEach(() => {
          collapse.clickCollapseButton();
          fixture.detectChanges();
        });

        it('should open the content.', () => {
          expect(collapse.contentIsOpen()).toBe(true);
        });
      });
    });
  });

  describe('with collapse set to false', () => {
    beforeEach(async(() => {
      PGiCollapsePage.makeTestBed( ClosedComponentWrapper )
        .compileComponents();
    }));

    beforeEach(() => {
      jasmine.clock().install();

      fixture = TestBed.createComponent(ClosedComponentWrapper);
      fixture.detectChanges();
      collapse = new PGiCollapsePage( fixture.debugElement );

      jasmine.clock().tick(10);
      fixture.detectChanges();
    });

    afterEach(() => {
      jasmine.clock().uninstall();
    });

    it('should be closed.', () => {
      expect(collapse.contentIsClosed()).toBe(true);
    });
  });
});
