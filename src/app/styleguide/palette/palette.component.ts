import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'palette',
  templateUrl: './palette.component.html',
  styleUrls: ['./palette.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class Palette {
  @Input() color;
  names = [];

  constructor() {
    this.names.push(
      '50', '100', '200', '300', '400', '500', '600', '700', '800', '900',
      'A100', 'A200', 'A400', 'A700'
    );
  }
}
