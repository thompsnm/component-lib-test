import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Palette } from './palette.component';

@NgModule({
  declarations: [
    Palette
  ],
  imports: [
    CommonModule
  ],
  exports: [
    Palette
  ]
})
export class PaletteModule {}
