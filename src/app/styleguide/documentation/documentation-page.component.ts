import { Component, Input, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'documentation-page',
  templateUrl: './documentation-page.component.html',
  styleUrls: ['./documentation-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DocumentationPage {
  @Input() doc;

  constructor(private route: ActivatedRoute) {
    route.data.subscribe( (data) => {
      this.doc = data;
    });
  }
}
