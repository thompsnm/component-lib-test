import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { PGiSoundService } from '../../component-lib/sounds';
@Component({
  selector: 'ex-sound',
  templateUrl: './sound-example.html',
  encapsulation: ViewEncapsulation.None,
  providers: [PGiSoundService]
})
export class SoundServiceExample {
  constructor( @Inject(PGiSoundService) private soundService: PGiSoundService) { }

  ngOnInit() {
    this.soundService.fetchAndPlay('http://www.thesoundarchive.com/starwars/star-wars-theme-song.mp3');
  }
  playIncomingSound() {
    this.soundService.play('incomingSound').subscribe(
      (value) => { console.log('Audio Play success'); },
      (err) => { console.log(err); }
    );
  }
  playOutgoingSound() {
    this.soundService.play('outgoingSound').subscribe(
      (value) => { console.log('Audio Play success'); },
      (err) => { console.log(err); }
    );
  }
  playStarWars() {
    this.soundService.fetchAndPlay('http://www.thesoundarchive.com/starwars/star-wars-theme-song.mp3');
  }
  playAllSounds() {
    this.soundService.play('incomingSound');
    this.soundService.play('outgoingSound');
    this.soundService.fetchAndPlay('http://www.thesoundarchive.com/starwars/star-wars-theme-song.mp3');
  }
  stopincomingSounds() {
    this.soundService.stop('incomingSound');
  }
  stopoutgoingSounds() {
    this.soundService.stop('outgoingSound');
  }
  stopStarWars() {
    this.soundService.stop('http://www.thesoundarchive.com/starwars/star-wars-theme-song.mp3');
  }
  stopPlayingSounds() {
    this.soundService.stopAll();
  }
}
