import { Component, Injectable, Inject, ViewEncapsulation} from '@angular/core';
import { PGiToastService, PGiToastDuration } from '../../component-lib/toast';

@Injectable()
export class ToastOpenerService {
  constructor(
    private toastService: PGiToastService
  ) { }

  burnMyBread() {
    this.toastService.openToast('Burnt!', PGiToastDuration.LONG);
  }
}

@Component({
  selector: 'ex-toast',
  templateUrl: './toast-example.html',
  encapsulation: ViewEncapsulation.None,
  providers: [PGiToastService, ToastOpenerService]
})
export class ToastServiceExample {
  constructor(
    @Inject(PGiToastService) private toastService: PGiToastService,
    @Inject(ToastOpenerService) private toastOpenerService: ToastOpenerService
  ) { }

  showShortToast() {
    this.toastService.openToast('Lightly Toasted', PGiToastDuration.SHORT);
  }

  showMediumToast() {
    this.toastService.openToast('Just Right');
  }

  showLongToast() {
    this.toastOpenerService.burnMyBread();
  }
}
