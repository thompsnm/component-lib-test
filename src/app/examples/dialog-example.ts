import { Component, ViewEncapsulation, Input, Output } from '@angular/core';
import { PGiDialog } from '../../component-lib/dialogs/dialog.service';

@Component({
  template: `
    <pgi-base-dialog>
      <h1>Na na na na na na na</h1>
      <h1>Batman!</h1>
    </pgi-base-dialog>
  `
})
export class DialogExampleContent {

}

@Component({
  selector: 'ex-dialog',
  template: `<pgi-text-button (click)="openDialog()">Open Dialog</pgi-text-button>`,
  encapsulation: ViewEncapsulation.None
})
export class DialogExample {

  constructor(private dialog: PGiDialog) { }

  openDialog() {
    this.dialog.open(DialogExampleContent);
  }

  closeDialog() {
    this.dialog.closeAll();
  }
}

