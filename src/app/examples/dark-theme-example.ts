import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ex-dark-theme',
  templateUrl: './dark-theme-example.html',
  encapsulation: ViewEncapsulation.None
})
export class DarkThemeExample {
  toggleTheme() {
    document.body.classList.toggle('pgi-dark-theme');
  }
}
