import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ex-date-pipe',
  templateUrl: './date-pipe-example.html',
  styles: ['.dateSnippet { margin: 20px;}'],
  encapsulation: ViewEncapsulation.None
})
export class PGiDatePipeExample {
  locales: string[];
  today: Date;
  yesterday: Date;
  lastWeek: Date;
  greaterThanWeek: Date;
  calObj: any;

  constructor() {
    this.locales = ['en', 'hi', 'te', 'ml', 'fr', 'de', 'nl', 'sv'];
    this.today = new Date();
    this.yesterday = new Date(Date.now() - 86400000);
    this.lastWeek = new Date(Date.now() - (Math.floor(Math.random() * 4) + 2) * 86400000);
    this.greaterThanWeek = new Date(Date.now() - (Math.floor(Math.random() * 365) + 6) * 86400000);
    this.calObj = {
      calendar: {
        sameDay: 'h:mm A',
        nextDay: '[Tomorrow]',
        nextWeek: 'dddd',
        lastDay: '[Yesterday]',
        lastWeek: 'dddd',
        sameElse: 'L'
      }
    };
  }
}
