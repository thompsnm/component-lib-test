import { Component, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'ex-autocomplete',
  templateUrl: './autocomplete-example.html',
  encapsulation: ViewEncapsulation.None
})
export class AutocompleteExample {
  rappers = [
    {name: 'Ludicrous'},
    {name: 'PDiddy'},
    {name: '50 Cent'},
    {name: 'Blackalicious'},
    {name: 'Roots Manuva'},
    {name: 'Outkast'},
    {name: 'Biggie'},
    {name: 'Mos Def'},
    {name: 'Common'},
    {name: 'A Tribe Called Quest'},
    {name: 'The Roots'},
    {name: 'De La Soul'},
    {name: 'Eminem'},
    {name: '2Pac'}
  ];

  filteredRappers: Observable<any[]>;

  myControl: FormControl;

  constructor() {
    this.myControl = new FormControl();
  }

  ngOnInit() {
    this.filteredRappers = Observable.of( this.rappers )
      .merge( this.myControl.valueChanges )
      .scan( (list, value) => {
        return this.rappers.filter( (rapper) => {
          return rapper.name.toLowerCase().includes( value.toLowerCase() );
        });
      });
  }
}
