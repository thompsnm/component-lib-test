import { PGiCircularSpinner } from '../../component-lib/spinner/circular-spinner.component';
import {
    AfterViewInit,
    Component,
    ComponentFactoryResolver,
    ComponentRef,
    Input,
    Renderer,
    ViewChild,
    ViewContainerRef,
    ViewEncapsulation,
} from '@angular/core';
@Component({
    selector: 'ex-spinner',
    templateUrl: './spinner-example.html',
    encapsulation: ViewEncapsulation.None,
    styles: ['.spinner { width: 80px; height: 80px}']
})
export class SpinnerExample implements AfterViewInit {
    @Input() color;
    @ViewChild("spinner", { read: ViewContainerRef }) container;
    ref: ComponentRef<PGiCircularSpinner>;

    constructor(private componentFactoryResolver: ComponentFactoryResolver) {

    }

    ngAfterViewInit() {
        this.changeColor(null);
    }

    changeColor(value) {
        this.color = value;
        this.container.clear();
        const factory = this.componentFactoryResolver.resolveComponentFactory(PGiCircularSpinner);
        this.ref = this.container.createComponent(factory);
        this.ref.instance.color = value;
        this.ref.changeDetectorRef.detectChanges();
    }
}

