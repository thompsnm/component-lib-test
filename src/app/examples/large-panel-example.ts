// tslint:disable:max-line-length
import { Component, ViewEncapsulation, Input, Output } from '@angular/core';
import { PGiDialog } from '../../component-lib/dialogs/dialog.service';

@Component({
  template: `
    <pgi-large-panel #panel [title]='title'>
      <div body style="padding: 0 60px">
        <h1>Wild Style</h1>
        <p>Wild Style is an American 1983 hip hop film produced by Charlie Ahearn. Released theatrically in September 1982 by First Run Features and later re-released for home video by Rhino Home Video, it is regarded as the first hip hop motion picture. The film featured seminal figures within the given period, such as Fab Five Freddy, Lee Quiñones, Lady Pink, The Rock Steady Crew, The Cold Crush Brothers, Queen Lisa Lee of Zulu Nation, Grandmaster Flash and Zephyr.[1][2][3] The protagonist "Zoro" is played by New York graffiti artist "Lee" George Quiñones. 2012 marked the 30th anniversary of the film. Producers proposed a 2013 Blu-ray edition that would include additional interviews and features.</p>
        <h2>Background</h2>
        <p>An early version of the Wild Style logo appeared in 1981 when Charlie Ahearn hired graffiti writer Dondi to paint the 'window down' subway car piece that appears in the film.[4] The Dondi piece was the inspiration for the animated title sequence designed by the artist, Zephyr in 1982. [5] The Wild Style mural was painted by Zephyr, Revolt and Sharp in 1983. Charlie Ahearn and Fab 5 Freddy began working on the film on late 1981. The approach was a hybrid of a narrative musical and documentary, having the real hip hop pioneers play themselves in a loosely scripted story shot entirely in the South Bronx, the Lower East Side and MTA subway yards.</p>
        <h2>Plot</h2>
        <p>Wild Style takes place in 1981 in New York and centers around graffiti artists, Zoro (played by Lee Quiñones) and his encounters with an uptown journalist named, Virginia (played by Patti Astor).[6] More so than its story, however, the film is notable for featuring several prominent figures from early hip hop culture such as Busy Bee Starski, Fab Five Freddy, The Cold Crush Brothers and Grandmaster Flash.[7] Throughout the movie there are scenes depicting activities common in the early days of hip hop. These include MCing, turntablism, graffiti and b-boying. The film demonstrates the interconnections between music, dance and art in the development of hip hop culture.</p>
      </div>
      <pgi-text-button footer type="secondary" (click)="panel.closeDialog()">Close</pgi-text-button>
      <pgi-text-button footer type="primary">Save Changes</pgi-text-button>
    </pgi-large-panel>`
})
export class LargePanelExampleContent {
  title = 'Wild Style Information';
}

@Component({
  selector: 'ex-large-panel',
  template: `<pgi-text-button (click)="openPanel()">Open Large Panel</pgi-text-button>`,
  encapsulation: ViewEncapsulation.None
})
export class LargePanelExample {

  constructor(private dialog: PGiDialog) { }

  openPanel() {
    this.dialog.open(LargePanelExampleContent, true);
  }

  closeDialog() {
    this.dialog.closeAll();
  }
}
