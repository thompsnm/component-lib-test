import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteExample } from './autocomplete-example';
import { DarkThemeExample } from './dark-theme-example';
import { PGiComponentsModule } from '../../component-lib/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PGiDatePipeExample } from './date-pipe-example';
import { InputFormExample } from './input-form-example';
import { ToastServiceExample } from './toast-example';
import { DialogExample, DialogExampleContent } from './dialog-example';
import { LargePanelExample, LargePanelExampleContent } from './large-panel-example';
import { SoundServiceExample } from './sound-example';
import { PGI_SOUND_SET } from '../../component-lib/sounds';
import { SpinnerExample } from './spinner-example';
import { PGiCircularSpinner } from './../../component-lib/spinner/circular-spinner.component';
import { DropdownExample } from './drop-down-example';

let PGI_SOUND_SET_OBJ = {
  'outgoingSound': 'assets/OutgoingCallRingtone_Stock.wav',
  'incomingSound': 'assets/IncomingCallRingtone-Call5.wav'
};

@NgModule({
  declarations: [
    AutocompleteExample,
    DarkThemeExample,
    PGiDatePipeExample,
    InputFormExample,
    ToastServiceExample,
    SoundServiceExample,
    SpinnerExample,
    DialogExample,
    LargePanelExample,
    LargePanelExampleContent,
    DialogExampleContent,
    DropdownExample
  ],
  entryComponents: [
    LargePanelExampleContent,
    DialogExampleContent,
    PGiCircularSpinner
  ],
  imports: [
    CommonModule,
    PGiComponentsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: PGI_SOUND_SET, useValue: PGI_SOUND_SET_OBJ },
  ],
  exports: [
    AutocompleteExample,
    DarkThemeExample,
    PGiDatePipeExample,
    InputFormExample,
    ToastServiceExample,
    DialogExample,
    LargePanelExample,
    SoundServiceExample,
    SpinnerExample,
    DropdownExample
  ]
})
export class ExamplesModule { }
