import { Component, ViewEncapsulation } from '@angular/core';
import { NgForm} from '@angular/forms';

@Component({
  selector: 'ex-input-form',
  templateUrl: './input-form-example.html',
  encapsulation: ViewEncapsulation.None
})
export class InputFormExample {
  formData: string;
  formValid = true;

  onSubmit(form: NgForm) {
    this.formData = JSON.stringify(form.value);
    this.formValid = form.valid;
  }
}
