import { Component, ViewEncapsulation, Input, ViewChild } from '@angular/core';
import { DropdownOptions } from '../../component-lib/drop-down/drop-down-options';

@Component({
  selector: 'ex-drop-down',
  templateUrl: './drop-down-example.html',
  encapsulation: ViewEncapsulation.None
})

export class DropdownExample {

  @ViewChild('dropDown') dropDown;

  @Input() disabled: String;
  @Input() selectedIndex: number;
  @Input() id: String;
  @Input() titleText: String;
  @Input() type: String = 'single';
  @Input() itemsCount = 3;
  @Input() errorMessage: String;

  constructor() { }

  ngAfterViewInit() {
    this.dropDown.options = [
      new DropdownOptions('Item1', 'desc1'),
      new DropdownOptions('Item2', 'desc2'),
      new DropdownOptions('Item3', 'desc3', true),
      new DropdownOptions('Item4', 'desc4'),
      new DropdownOptions('Item5', 'desc5'),
      new DropdownOptions('Item5', 'desc6')
    ];
  }
}
