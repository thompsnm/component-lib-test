import { Component, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  navigation = [];

  constructor(private router: Router) {
    router.config.forEach( (route) => {
      if ( route.path !== '' && route.path !== '**' ) {
        this.navigation.push(route.data);
      }
    });
  }
}
