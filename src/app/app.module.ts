// 3rd party dependencies
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicComponentModule } from 'ng-dynamic';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import 'hammerjs';

// Components defined by the style guide website (not exportable).
import { AppComponent } from './app.component';
import { PaletteModule } from './styleguide/palette/palette.module';
import { DocumentationPage } from './styleguide/documentation/documentation-page.component';
import { ExamplesModule } from './examples';

// Component libary config.
import { PGiComponentsModule } from '../component-lib/components.module';
import { PGI_ICON_SET_URL } from '../component-lib/icons';

let routesConfig = require('app/routes.json');

let routes: Routes = [];

routesConfig.forEach((config) => {
  routes.push({
    path: config.path,
    component: DocumentationPage,
    data: config
  });
});

let defaultPath = `/${routes[0].path}`;

routes.push({ path: '', redirectTo: defaultPath, pathMatch: 'full' });
routes.push({ path: '**', redirectTo: defaultPath, pathMatch: 'full' });

@NgModule({
  declarations: [
    AppComponent,
    DocumentationPage
  ],
  imports: [
    DynamicComponentModule.forRoot({
      imports: [
        // Any components that are referenced in the SASS documentation
        // need their modules imported here.
        PGiComponentsModule,
        ExamplesModule,
        PaletteModule
      ]
    }),
    BrowserModule,
    RouterModule.forRoot(routes),
    CommonModule,
    HttpModule,
    // Dynamic modules from the SASS docs also need to be declared here.
    PGiComponentsModule,
    PaletteModule,
    ExamplesModule
  ],
  providers: [
    { provide: PGI_ICON_SET_URL, useValue: 'assets/iconSet.svg' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
