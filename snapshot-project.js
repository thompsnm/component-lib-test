var fs = require('fs');
var package = require('./package.json');
var semver = require('semver');

if (process.argv.length !== 3) {
  throw new Error('This script requires the build number to be passed as the one and only argument.');
}

var buildNumber = process.argv[2];
var newVersion = semver.inc(package.version, 'patch') + '-snapshot.' + buildNumber;
package.version = newVersion;

fs.writeFile('package.json', JSON.stringify(package, null, 2), 'utf8', function(err) {
  if (err) {
    throw err;
  }

  console.log('Updated version in package.json to:', package.version);
});
