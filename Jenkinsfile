node () {

  /*
   * The Docker plugin appears to cache images, so we're cleaning before runs to
   * ensure the build always pulls the latest image.
   */
  stage('Clean Up Workspace') {
    step([$class: 'WsCleanup'])
  }

  stage('Prepare Environment') {

    checkout scm

    withNPM(npmrcConfig: 'npmrc') {
      PACKAGE_VERSION = sh(
        script: 'node -pe "require(\'./package.json\').version"',
        returnStdout: true
      ).trim()

      PUBLISHED_VERSION = sh(
        script: 'npm show @pgi/ng-component-lib version',
        returnStdout: true
      ).trim()

      PUBLISHING_NEW_RELEASE = PACKAGE_VERSION != PUBLISHED_VERSION
    }

    /*
      * -v /etc/passwd:/etc/passwd
      *   The Docker plugin passes the UID of the user on the host machine as the
      *   user in the container automatically. See `-u 1005:1005` in the logs.
      *   Unfortunately, the two have different /etc/passwd files, so while 1005
      *   is the jenkins user on the host, 1005 is unrecognized in the container.
      *   Mounting the host /etc/passwd to the container fixes this.
      */
    docker.image('node:7.5.0').inside('-v /etc/passwd:/etc/passwd') {

      withEnv([
        /* Override the npm cache directory to avoid: EACCES: permission denied, mkdir '/.npm' */
        'npm_config_cache=npm-cache'
      ]) {

        stage('Install Dependencies') {
          sh 'npm install'
        }

        stage('Lint Project') {
          sh 'npm run lint'
        }

        stage('Build Dist') {
          sh 'npm run build:dist'
        }

        stage('Run Unit Tests') {
          sh 'npm run test -- --single-run --reporters mocha'
        }

        stage('Verify Theme Builds') {
          sh 'npm run validate:all-themes'
        }

        if (BRANCH_NAME == 'master') {

          stage('Publish Dist to Artifactory') {
            withNPM(npmrcConfig: 'npmrc') {
              if (PUBLISHING_NEW_RELEASE) {
                println 'Versions are different, publishing new release'
                sh 'npm publish dist'
              } else {
                println 'Versions are the same, publishing snapshot'
                sh "node snapshot-project.js ${env.BUILD_NUMBER}"
                sh 'npm run package:config'
                sh 'npm publish dist --tag snapshot'
              }
            }
          }

          stage('Trigger Ludicrous Electron Build') {
            if (PUBLISHING_NEW_RELEASE) {
              println "A new release of this project has been published.\n" +
                      "This job will not kick off a new build of the Ludicrous Electron project as no snapshots have changed.\n" +
                      "To build a new release of Ludicrous Electron that uses the new release of this project, increment the version number in Ludicrous Electron and push the change to the remote repository."
            } else {
              println "A new snapshot of this project has been published.\n" +
                      "Kicking off a new build of the Ludicrous Electron project to use this snapshot."
              build job: 'Ludicrous Electron Build/master', wait: false
            }
          }

          stage('Build Hosted Style Guide') {
            sh 'npm run build:hosted-sg'
          }

        }

      }

    }


    if (BRANCH_NAME == 'master') {
      docker.image('cgswong/aws').inside() {
        stage('Deploy Style Guide to S3') {
          withAWS(credentials:'s3-nokia', region:'us-east-2') {
            println 'Publishing style guide to styleguide-alpha.pgi-tools.com'
            sh 'aws s3 cp style-guide/ s3://styleguide-alpha.pgi-tools.com/ --region us-east-2 --acl public-read --recursive'

            if(PUBLISHING_NEW_RELEASE) {
              println 'Publishing style guide to styleguide.pgi-tools.com'
              sh 'aws s3 cp style-guide/ s3://styleguide.pgi-tools.com/ --region us-east-2 --acl public-read --recursive'
            }
          }
        }
      }
    }

  }

}
