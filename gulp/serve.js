var gulp = require('gulp');

// Kick off the development webserver.
var server = null;
function serve(done){
  if( server ){
    server.kill();
  }

  var command = [
    `./${paths.node.webpackServer}`,
    `--no-info --colors --historyApiFallback`,
    `--content-base ${paths.src.app.rootDir}`,
    `--port ${paths.server.port}`,
    `--env.theme ${paths.theme.current}`,
  ].join(' ');

  server = utils.shell(command, function(){ done(); });
}

gulp.task( 'sg-serve', function(done){
  serve(done);
});

// Kick off a web server using the Style Guide bundle that will be shipped to S3
gulp.task('hosted-sg-serve', function() {
  gulp.src('style-guide')
    .pipe(webserver({
      livereload: true,
      directoryListing: false,
      open: false,
      port: 8091,
      fallback: 'index.html'
    }));
});

