var gulp = require('gulp');

var allDocFormats = ['html', 'css', 'js', 'ts', 'scss', 'xml', 'json', 'bash', 'shell'];

// To build the documentation from KSS comments in the stylesheets
// we perform the following steps:
// 1. Parse the stylesheets using the KSS tool.
// 2. Read in the page-hierarchy.json file which defines page ordering.
//    This makes it easier to re-order styleguide pages without having to update
//    all of the doc comments.
// 3. Sort the KSS JSON output based on the page-hierarchy.
// 4. Output the final docs.json which is used by the site to generate routes and pages.
gulp.task( 'build-kss', function(done){
  var options = {
    markdown: true
  };
  // TODO Only reparse if one of the SCSS files is newer than
  // the output docs.json.
  kss.traverse(paths.src.componentLib.rootDir, options).then(function(styleguide){
    gutil.log( gutil.colors.cyan('kss parsing complete') );
    generateDocsFromKSS(styleguide.toJSON());
    done();
  });
});

gulp.task( 'kss', function(done){
  sequence('build-kss', done);
});

gulp.task( 'docs-watch', function(){
  return gulp.watch(paths.src.css.all, ['kss']);
});


var generateDocsFromKSS = function(kss){
  var hierarchy = require(`../${paths.src.json.hierarchy}`);
  var docs = setupPageConfig( hierarchy );
  if( kss.sections ){
    highlightCode(kss.sections);
    // TODO Handle the case where two top level docs have the same name.
    getPagesFromDocs(kss.sections, docs);
    addChildPages(kss.sections, docs);
    sortChildPages(docs, hierarchy);
  }
  writeRouteConfig(docs, paths.build.json.docs);
  gutil.log( gutil.colors.cyan('docs regenerated:'), gutil.colors.green(paths.build.json.docs) );
}

var writeRouteConfig = function(docs, path){
  var routes = JSON.stringify(docs, null, 2);
  fs.writeFileSync( path, routes );
}

/**
 * Create the basic docs configuration object which lists
 * the top level pages of the app.
 */
var setupPageConfig = function(docsConfig){
  var docs = [];
  for( var i = 0; i < docsConfig.length; i++ ){
    var current = docsConfig[i];
    docs.push( makePage(current.name, null, null, null, current.children) );
  }
  return docs;
}

var unescapeText = function(text){
  return text.replace(/&#39;/g, "'")
    .replace(/&gt;/g, '>')
    .replace(/&lt;/g, '<')
    .replace(/&quot;/g, '"')
    .replace(/&amp;/g, '&');
}

var highlightCodeBlock = function(text){
  var matcher = /<pre><code.*?>([\S\s]*?)<\/code><\/pre>/g;
  var endTag = '</code></pre>';
  var endTagLength = endTag.length;

  let updated = text.replace(matcher, function(full, inner, index, input){
    let contentStart = full.length - endTagLength - inner.length;
    let startTag = input.substr(index, contentStart);

    // Determine the language of this code block.
    let blockLang = startTag.match(/lang-(.*?)"/);
    let lang = blockLang ? [ blockLang[1] ] : allDocFormats;

    // Highlight any code blocks in the given text.
    let highlighted = highlight.highlightAuto( unescapeText(inner), lang);
    return startTag + highlighted.value + '</code></pre>';
  });

  return updated;
}

var highlightCode = function(kss){
  kss.forEach( function(doc){
    doc.description = highlightCodeBlock( doc.description );
    var markup = highlight.highlightAuto(doc.markup, ['html']);
    doc.highlightedMarkup = markup.value;
  });
}

/**
 * Make a single documentation page config object.
 */
var makePage = function( name, title = null, hierarchy = null, config = null, children = [] ) {
  title = title ? title : name;
  hierarchy = hierarchy ? hierarchy : name;
  var path = hierarchy.replace('.', '#');

  return {
    name: name,
    title: title,
    hierarchy: hierarchy,
    path: path,
    config: config,
    children: children
  };
}

var getPagesFromDocs = function(kss, docs) {
  for ( var i = 0, total = kss.length; i < total; i++ ) {
    var doc = kss[i];
    if ( doc.depth === 1 ) {
      addTopLevelDoc(doc, docs);
    }
  }
}

var addTopLevelDoc = function(doc, docs) {
  for ( var i = 0, total = docs.length; i < total; i++ ) {
    var page = docs[i];
    if ( page.name === doc.reference ) {
      page.title = doc.header;
      page.config = doc;
      // We're done here.
      return true;
    }
  }

  // Handle unknown top level docs.
  addUnknownTopLevelDoc(doc, docs);
}

var addUnknownTopLevelDoc = function(doc, docs) {
  docs.push( makePage(
    doc.reference,
    doc.header,
    doc.reference,
    doc
  ));
}

var addChildPages = function(kss, docs) {
  for( var i = 0, total = kss.length; i < total; i++ ){
    var doc = kss[i];
    if( doc.depth > 1 ){
      addChildLevelDoc(doc, docs);
    }
  }
}

var addChildLevelDoc = function(doc, docs) {
  var hierarchy = doc.reference.split('.');
  for ( var i = 0, total = docs.length; i < total; i++ ) {
    var page = docs[i];
    if ( page.name === hierarchy[0] ) {
      page.children.push( makePage(
        hierarchy[1],
        doc.header,
        doc.reference,
        doc
      ));
      // We're done here.
      return true;
    }
  }

  // If we couldn't find it something was funky. We'll just add it on the end.
  console.warn('Could not find a home for child doc:', doc.reference, doc);
  addUnknownTopLevelDoc(doc, docs);
}

var getChildOrder = function(doc, hierarchy){
  for( var j = 0, tj = hierarchy.length; j < tj; j++){
    var override = hierarchy[j];
    if( override.name === doc.name ){
      return override.childOrder;
    }
  }

  return false;
}

var sortChildPages = function(docs, hierarchy) {
  for( var i = 0, total = docs.length; i < total; i++){
    var doc = docs[i];
    var childSortConfig = getChildOrder(doc, hierarchy);

    if ( doc.children.length > 0 ) {
      if( childSortConfig ){
        configDocChildrenSort(doc, childSortConfig);
      }
      else {
        standardDocChildrenSort(doc);
      }
    }
  }
}

var configDocChildrenSort = function(doc, order){
  var i, j, it, ij, name, child = 0;
  var matched = [];
  var unmatched = [];
  // Look for all of the documents listed in the config.
  for( i = 0, it = order.length; i < it; i++ ){
    name = order[i];

    for( j = 0, jt = doc.children.length; j < jt; j++ ){
      child = doc.children[j];
      if( child.name === name ){
        matched.push(child);
        break;
      }
    }
  }

  // Look for all of the docs that weren't listed in the config.
  for( i = 0, it = doc.children.length; i < it; i++ ){
    child = doc.children[i];
    var found = false;

    for( j = 0, jt = order.length; j < jt; j++ ){
      name = order[j];
      if( child.name === name ){
        found = true;
        break;
      }
    }

    if( !found ){
      unmatched.push(child);
    }
  }

  // Add all of the children that were found to the front of the
  // list and anything else at the back.
  doc.children = matched.concat( unmatched );
}

var standardDocChildrenSort = function(doc) {
  doc.children.sort((a, b) => {
    if ( a.config.weight && b.config.weight ) {
      return sortDocChildAlpha(a.config.weight, b.config.weight);
    }
    else if ( a.config.weight ) {
      return 1;
    }
    else if ( b.config.weight ) {
      return -1;
    }
    else {
      return 0;
    }
  });
}

var sortDocChildAlpha = function(a, b) {
  if ( a > b ) {
    return 1;
  }
  else if ( b > a ) {
    return -1;
  }
  else {
    return 0;
  }
}

