var gulp = require('gulp');

var docStart = `
/*
Full PGi Icon Set

Here is a full list of the icons available in this library.

> NOTE: Any icons not colored red should be updated to use "currentColor" as their fill value.

Markup:
<div class="sg-icon-list">
`;

var docEnd = `
</div>

Weight: 2

Styleguide icons.all
*/
`;

var iconTemplate = function(name){
  return `<div class="sg-icon-example"><pgi-icon size="2" aria-label="${name}" icon="pgi:${name}"></pgi-icon><p>${name}<p></div>`;
};

// Make the icon documentation for an icon file.
var makeIconDoc = function(filePath){
  var name = path.basename(filePath).split('.')[0];
  return iconTemplate(name);
}

// Make the <svg> node that will be placed inside the iconSet svg file.
var makeIconSVG = function(file){
  var cont = file.contents.toString();
  var name = path.basename(file.path).split('.')[0];
  return cont.replace('<svg', '<svg id="' + name + '"')
    // Fix any icons that are using black or white as their fill or line colors.
    .replace('stroke="#000000"', 'stroke="currentColor"')
    .replace('stroke="#FFFFFF"', 'stroke="currentColor"')
    .replace('stroke="#ffffff"', 'stroke="currentColor"')
    .replace('fill="#000000"', 'fill="currentColor"')
    .replace('fill="#FFFFFF"', 'fill="currentColor"')
    .replace('fill="#ffffff"', 'fill="currentColor"');
}

gulp.task('icons-docs',
  'Build the docs so all the icons appear as samples in the styleguide',
  function() {
    var icons = paths.src.svg.icons;
    if( !paths.hasFiles(paths.src.svg.iconsDir) ){
      icons = paths.theme.getFile(icons);
    }

    return gulp.src(icons)
      .pipe(newer(paths.build.svg.iconDoc))
      .pipe(debug({title: gutil.colors.cyan('icon-docs'), showFiles:paths.verbose}))
      .pipe(tap( function(file) {
        file.contents = new Buffer( makeIconDoc(file.path) );
      }))
      .pipe(concat(paths.build.svg.iconDocFilename, {newLine:''}))
      .pipe(insert.wrap(docStart, docEnd))
      .pipe(gulp.dest(paths.build.svg.iconDocDir, { mode: 0664, overwrite: true}));
  }
);

gulp.task( 'icons-svg',
  'Build the icon set svg for angular material',
  function(done) {
    var icons = paths.src.svg.icons;
    if( !paths.hasFiles(paths.src.svg.iconsDir) ){
      icons = paths.theme.getFile(icons);
    }

    return gulp.src(icons)
      .pipe(newer(paths.build.svg.iconSet))
      .pipe(debug({title: gutil.colors.cyan('icons-svg'), showFiles:paths.verbose}))
      .pipe(tap(function(file) {
        file.contents = new Buffer( makeIconSVG(file) )
      }))
      .pipe(concat(paths.build.svg.iconSetFilename))
      .pipe(insert.wrap('<svg><defs>','</defs></svg>'))
      .pipe(debug({title: gutil.colors.cyan('icons-svg output'), showFiles:paths.verbose}))
      .pipe(gulp.dest(paths.build.svg.iconSetDir, { mode: 0664, overwrite: true}));
  }
);

gulp.task('icons', false, ['icons-svg', 'icons-docs']);

gulp.task( 'icons-watch', function(){
  return gulp.watch(paths.src.svg.icons, ['icons']);
});

