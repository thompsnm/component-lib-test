var gulp = require('gulp');
var inlineResources = require('./inline-resources');

gulp.task('clean-and-package', function(done){
  sequence('clean-dist', 'package', done);
});

gulp.task('package', function(done){
  sequence(['copy-package-json', 'package-components'], done);
});

gulp.task('package-watch', function(done){
  gulp.watch(paths.src.componentLib.all, ['package']);
});

gulp.task('package-components', function(){
  return gulp.src(paths.src.componentLib.all)
    .pipe(newer(paths.dist.rootDir))
    .pipe(debug({title: gutil.colors.cyan( 'package-components'), showFiles:paths.verbose}))
    .pipe(gulp.dest(paths.dist.rootDir));
});

gulp.task('copy-package-json', function(){
  return gulp.src(paths.src.packageJSON)
    .pipe(newer(paths.dist.rootDir))
    .pipe(debug({title: gutil.colors.cyan('copy-package-json'), showFiles:paths.verbose}))
    .pipe(gulp.dest(paths.dist.rootDir));
});

//Tasks for bundling the npm module
gulp.task('copy-and-inline-resource', function(done) {
  sequence(['copy-html', 'copy-assets', 'copy-scss'], 'inline-resources');
});

gulp.task('copy-html', function copyHtml() {
  return gulp.src(paths.src.html.components)
    .pipe(debug({title: gutil.colors.cyan('copy-html'), showFiles:paths.verbose}))
    .pipe(gulp.dest(paths.dist.rootDir));
});

gulp.task('copy-assets', function copyAssets () {
  return gulp.src(paths.src.assets.all)
    .pipe(debug({title: gutil.colors.cyan('copy-assets'), showFiles:paths.verbose}))
    .pipe(gulp.dest(paths.dist.rootDir));
});

gulp.task('copy-scss', function copyScss () {
  return gulp.src(paths.src.css.components)
    .pipe(debug({title: gutil.colors.cyan('copy-scss'), showFiles:paths.verbose}))
    .pipe(gulp.dest(paths.dist.rootDir));
});

gulp.task('inline-resources', function inlineResource() {
  return inlineResources('./dist');
});

gulp.task('package-hosted-sg', function packageStyleGuide (done) {
  sequence('webpack-bundle-hosted-sg', 'copy-assets-to-hosted-sg', done);
});

gulp.task('webpack-bundle-hosted-sg', function webpackBundleHostedStyleGuide (done) {
  var command = `webpack --env=prod`;
  utils.shell(command, function(){ done(); });
});

gulp.task('copy-assets-to-hosted-sg', function copyAssetsToStyleGuide () {
  return gulp.src(paths.src.assets.all)
    .pipe(debug({title:'copy-assets-to-hosted-sg', showFiles:false}))
    .pipe(gulp.dest(paths.styleGuide.assets.rootDir));
});

