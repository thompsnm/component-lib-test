var gulp = require('gulp');

gulp.task( 'clean-documentation', function(done){
  del([paths.build.json.docs]).then( function(){ done(); } );
});

gulp.task('clean-dist', function(done){
  del([paths.dist.rootDir]).then( function(){ done(); } );
});

gulp.task('clean-icons', function(done){
  del([paths.build.svg.iconDoc, paths.build.svg.iconSet]).then( function(){ done(); });
});

gulp.task('clean-fonts', function(done){
  del([paths.build.fonts.all]).then( function(){ done(); } );
});

gulp.task('clean-shared-vars', function(done){
  del([paths.build.css.sharedVars, paths.build.json.sharedVars]).then( function(){ done(); });
});

gulp.task('clean-themed-app-assets', function(done){
  del([paths.build.appAssets.rootDir]).then( function(){ done(); } );
});

gulp.task('clean-hosted-sg', function(done){
  del(['style-guide']).then( function(){ done(); });
});

gulp.task('clean', function(done){
  sequence(
    [
      'clean-documentation',
      'clean-dist',
      'clean-icons',
      'clean-fonts',
      'clean-shared-vars',
      'clean-themed-app-assets',
      'clean-hosted-sg'
    ],
    done
  );
});

