var gulp = require('gulp');

gulp.task('shared-variables', ['scss-shared-variables', 'js-shared-variables']);

gulp.task('scss-shared-variables', function(){
  var input = paths.theme.getFile(paths.src.json.sharedVars);

  return fs.createReadStream(input)
    .pipe(jsonSass({
      prefix: '$pgi-shared-variables:'
    }))
    .pipe(debug({title: gutil.colors.cyan('scss-shared-variables'), showFiles:false}))
    .pipe(fs.createWriteStream(paths.build.css.sharedVars))
});

gulp.task('js-shared-variables', function(){
  var sharedVars = paths.theme.getFile( paths.src.json.sharedVars );

  return gulp.src(sharedVars)
    .pipe(debug({title: gutil.colors.cyan('js-shared-variables'), showFiles:paths.verbose}))
    .pipe(gulp.dest(paths.build.json.sharedVarsDir));
});

gulp.task('fonts', function(){
  var config = paths.theme.getFile(paths.src.theme.fontsConfig);

  return gulp.src(config)
    .pipe(newer(paths.build.fonts.googleFontsMain))
    .pipe(ggf())
    .pipe(debug({title: gutil.colors.cyan('fonts'), showFiles:paths.verbose}))
    .pipe(gulp.dest(paths.build.fonts.root));
});

gulp.task('themed-app-assets', function(){
  var assetsToCopy = [
    paths.theme.getFile( paths.build.json.sharedVars ),
    paths.theme.getFile( paths.build.svg.iconSet ),
    paths.theme.getFile( paths.build.sounds.incoming ),
    paths.theme.getFile( paths.build.sounds.outgoing )
  ]
  return gulp.src(assetsToCopy)
    .pipe(newer(paths.build.appAssets.rootDir))
    .pipe(debug({title: gutil.colors.cyan('themed-app-assets'), showFiles: paths.verbose}))
    .pipe(gulp.dest(paths.build.appAssets.rootDir));
});
