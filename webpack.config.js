const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ProgressPlugin = require('webpack/lib/ProgressPlugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const { NoEmitOnErrorsPlugin, LoaderOptionsPlugin } = require('webpack');
const { BaseHrefWebpackPlugin, GlobCopyWebpackPlugin } = require('@angular/cli/plugins/webpack');
const { CommonsChunkPlugin } = require('webpack').optimize;
const { AotPlugin } = require('@ngtools/webpack');

function localPath(){
  var folders = Array.prototype.slice.call(arguments);
  folders.unshift(process.cwd());
  return path.join.apply(null, folders);
}

function themePath(theme){
  return localPath('src/component-lib/themes', theme);
}

const nodeModules = localPath( 'node_modules' );
const entryPoints = ["inline","polyfills","sw-register","scripts","styles","vendor","main"];

module.exports = function(env){
  const defaultTheme = 'pgi';
  const themeFolder = env && env.theme ? themePath(env.theme) : themePath(defaultTheme);

  const sassLoaderOptions = {
    includePaths: [
      themeFolder
    ],
    outputStyle: "compressed"
  };

  return {
    "devtool": "source-map",
    "resolve": {
      "extensions": [
        ".ts",
        ".js"
      ],
      "modules": [
        "./node_modules"
      ]
    },
    "resolveLoader": {
      "modules": [
        "./node_modules"
      ]
    },
    "entry": {
      "main": [
        "./src/main.ts"
      ],
      "polyfills": [
        "./src/polyfills.ts"
      ],
      "styles": [
        "./src/styles.scss"
      ]
    },
    "output": {
      "path": localPath("style-guide"),
      "filename": "[name].bundle.js",
      "chunkFilename": "[id].chunk.js"
    },
    "module": {
      "rules": [
        {
          "enforce": "pre",
          "test": /\.js$/,
          "loader": "source-map-loader",
          "exclude": [
            /\/node_modules\//
          ]
        },
        {
          "test": /\.json$/,
          "loader": "json-loader"
        },
        {
          "test": /\.html$/,
          "loader": "raw-loader"
        },
        {
          "test": /\.(eot|svg)$/,
          "loader": "file-loader?name=[name].[ext]"
        },
        {
          "test": /\.(jpg|png|gif|otf|ttf|woff|woff2|cur|ani)$/,
          "loader": "url-loader?name=[name].[ext]&limit=10000"
        },
        {
          "test": /\.css$/,
          "exclude": [
            localPath( "src/styles.scss" )
          ],
          "loaders": [
            "exports-loader?module.exports.toString()",
            "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
            "postcss-loader"
          ]
        },
        {
          "test": /\.scss$|\.sass$/,
          "exclude": [
            localPath( "src/styles.scss" )
          ],
          "loaders": [
            "exports-loader?module.exports.toString()",
            "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
            "postcss-loader",
            {
              loader: "sass-loader",
              options: sassLoaderOptions
            }
          ]
        },
        {
          "test": /\.less$/,
          "exclude": [
            localPath( "src/styles.scss" )
          ],
          "loaders": [
            "exports-loader?module.exports.toString()",
            "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
            "postcss-loader",
            "less-loader"
          ]
        },
        {
          "test": /\.styl$/,
          "exclude": [
            localPath( "src/styles.scss" )
          ],
          "loaders": [
            "exports-loader?module.exports.toString()",
            "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
            "postcss-loader",
            "stylus-loader?{\"sourceMap\":false,\"paths\":[]}"
          ]
        },
        {
          "test": /\.css$/,
          "include": [
            localPath( "src/styles.scss" )
          ],
          "loaders": ExtractTextPlugin.extract({
            "use": [
              "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
              "postcss-loader"
            ],
            "fallback": "style-loader",
            "publicPath": ""
          })
        },
        {
          "include": [
            localPath( "src/styles.scss" )
          ],
          "test": /\.scss$|\.sass$/,
          "loaders": ExtractTextPlugin.extract({
            "use": [
              "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
              "postcss-loader",
              {
                loader: "sass-loader",
                options: sassLoaderOptions
              }
            ],
            "fallback": "style-loader",
            "publicPath": ""
          })
        },
        {
          "test": /\.less$/,
          "include": [
            localPath( "src/styles.scss" )
          ],
          "loaders": ExtractTextPlugin.extract({
            "use": [
              "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
              "postcss-loader",
              "less-loader"
            ],
            "fallback": "style-loader",
            "publicPath": ""
          })
        },
        {
          "test": /\.styl$/,
          "include": [
            localPath( "src/styles.scss" )
          ],
          "loaders": ExtractTextPlugin.extract({
            "use": [
              "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
              "postcss-loader",
              "stylus-loader?{\"sourceMap\":false,\"paths\":[]}"
            ],
            "fallback": "style-loader",
            "publicPath": ""
          })
        },
        {
          "test": /\.ts$/,
          "loader": "@ngtools/webpack",
        }
      ]
    },
    "plugins": [
      new NoEmitOnErrorsPlugin(),
      new HtmlWebpackPlugin({
        "template": "./src/index.html",
        "filename": "./index.html",
        "hash": false,
        "inject": true,
        "compile": true,
        "favicon": false,
        "minify": false,
        "cache": true,
        "showErrors": true,
        "chunks": "all",
        "excludeChunks": [],
        "title": "Webpack App",
        "xhtml": true,
        "chunksSortMode": function sort(left, right) {
          let leftIndex = entryPoints.indexOf(left.names[0]);
          let rightindex = entryPoints.indexOf(right.names[0]);
          if (leftIndex > rightindex) {
            return 1;
          }
          else if (leftIndex < rightindex) {
            return -1;
          }
          else {
            return 0;
          }
        }
      }),
      new BaseHrefWebpackPlugin({}),
      new CommonsChunkPlugin({
        "name": "inline",
        "minChunks": null
      }),
      new CommonsChunkPlugin({
        "name": "vendor",
        "minChunks": (module) => module.resource && module.resource.startsWith(nodeModules),
        "chunks": [
          "main"
        ]
      }),
      new GlobCopyWebpackPlugin({
        "patterns": [
          "assets",
          "favicon.ico"
        ],
        "globOptions": {
          "cwd": localPath("src"),
          "dot": true,
          "ignore": "**/.gitkeep"
        }
      }),
      new ProgressPlugin(),
      new ExtractTextPlugin({
        "filename": "[name].bundle.css",
        "disable": true
      }),
      new LoaderOptionsPlugin({
        "sourceMap": false,
        "options": {
          "postcss": [
            autoprefixer()
          ],
          "sassLoader": {
            "sourceMap": false,
            "includePaths": []
          },
          "lessLoader": {
            "sourceMap": false
          },
          "context": ""
        }
      }),
      new AotPlugin({
        "tsConfigPath": "src/tsconfig.json",
        "mainPath": "main.ts",
        "hostReplacementPaths": {
          "environments/environments.ts": "environments/environment.ts"
        },
        "exclude": [
        ],
        "skipCodeGeneration": true
      })
    ],
    "node": {
      "fs": "empty",
      "global": true,
      "crypto": "empty",
      "tls": "empty",
      "net": "empty",
      "process": true,
      "module": false,
      "clearImmediate": false,
      "setImmediate": false
    }
  };
};
