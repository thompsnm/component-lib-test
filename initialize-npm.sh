#!/bin/bash

function _check-num-params {
# 3 parameters:
#   _check-num-params ${#} -gt 2
#   Reads nicely as "The number of parameters of this function should be greater than 2"
# 2 parameters:
#   _check-num-params ${#} 2
#    Reads nicely as "The number of parameters of this function should be equal to 2"

  if [[ ${#} -eq 3 ]]; then
    local actual_num_params="${1}"
    local operator="${2}"
    local expected_num_params="${3}"
  else
    local operator="-eq"
    local actual_num_params="${1}"
    local expected_num_params="${2}"
  fi

  if ! ( test "${actual_num_params}" "${operator}" "${expected_num_params}" ); then
    log-error "${FUNCNAME[1]} needs ${operator} ${expected_num_params} parameters! (you provided: ${actual_num_params})"
    return 1
  fi
}

function prompt {
  # Usage: prompt displayed-prompt-string default-answer actual-answer-var
  # The value of actual-answer-var is changed in the calling function.
  # This enables testing which is thwarted by "var=$(prompt ...)" usage.
  _check-num-params ${#} -ge 2

  local question="${1}"
  local default_answer="${2}"
  local actual_answer=\$"$3"  # Name of variable (not value!).

  if [[ -n ${default_answer} ]]; then
    question="${question} [${default_answer}]"
  fi

  question="${question}: "

  read -p "${question}" actual_answer
  if [[ -z "${actual_answer}" ]]; then
    actual_answer="${default_answer}"
  fi

  if [[ ${#} -eq 2 ]]; then
    echo "${actual_answer}"
  elif [[ ${#} -eq 3 ]]; then
    # return value in place only if 3rd parameter 'actual-answer-var' present.
    # magically set the caller's parameter to this value.
    # printf is safer than eval, which can execute commands with impunity.
    printf -v "$3" "${actual_answer}"
  fi
}

#Retreive password from lastpass with fallback to manual entry
function get-password {
  local password_name="${1:-sudo}"
  local prompt="${2:-enter $password_name password}"

  if [[ -t 1 ]]; then
    log-error "Cowardly refusing to print password to terminal output"
    return ${FAIL}
  fi

  if [[ -c /dev/tty ]]; then
    # Read pass
    local passcode
    echo -n "${prompt}: " >&2
    IFS= read -rs passcode < /dev/tty
    echo >&2
    if [[ -z ${passcode} ]]; then
      log-error "No password entered."
      return ${FAIL}
    fi
    echo ${passcode}
    return 0
  fi
}

#Check if .npmrc exists
if [[ -f ./.npmrc ]]; then
  echo "The .npmrc file exists";
  #get user and pw values to use as defaults
fi

echo "Setting up NPM Repository..."
prompt "Enter your Artifactory username" ${USER} untrimmed_username
username=$(echo ${untrimmed_username} | xargs)
password=$(get-password "${credentials}" "Enter your Artifactory password")

echo ${username}
echo ${password}

#get auth values
authresponse=$(curl -u${username}:${password} "https://cddev.jfrog.io/cddev/api/npm/npm/auth/pgi")

#check if error was returned
echo ${authresponse}

#update .npmrc
touch ./.npmrc
echo "${authresponse}" > ./.npmrc
chmod 0600 ./.npmrc
