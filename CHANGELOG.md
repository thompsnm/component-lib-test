## 1.0.0

This version adds multiple theme support to the library. Multiple theme support
is fully documented in the README.scss. In order to enable this functionality
there are some breaking changes.

**Breaking API Changes**:

- Moved all global variables to pgi-* mixins. For example, the `$m-nudge` variable
is now `pgi-nudge()` and `$m-fs-s` is now `pgi-font-size(s)`. Look at the
`src/component-lib/themes/_*-utils.sccss` files to find the new mixins to use.
- It is no longer necessary to import multiple SASS files to get access to the
different theme functions/mixins. Now simply import "theme" as described in the docs.
- The imports for initializing the component library styles have also changed.
See the getting started docs for more information about how to import the SASS styles
into your projects.

